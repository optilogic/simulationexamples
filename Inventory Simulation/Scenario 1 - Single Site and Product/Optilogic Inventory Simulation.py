"""This module simulates a supply chain network
and tracks the inventory level for each siteproduct present
in the orders file.

It is assumed that any unfulfilled order is backordered
and is fulfilled whenever the material is available in the
inventory.  The service level is estimated based on how
late the order was fulfilled

Sequence of daily events as implemented in the model
1. Receive any replenishment.
2. Implement any new inventory policy if any.
3. Process any unfilled orders FIFO.
4. Process new orders FIFO.
5. Review inventory levels.
6. Write out statistics.

"""

__author__ = 'Roar Simulation'

import os
import tempfile
os.environ['MPLCONFIGDIR'] = tempfile.mkdtemp()
import simpy
import math
import datetime as dt
import calendar as cl
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import sys

# noinspection PyCompatibility
from pathlib import Path

MODEL_VERSION = "1.0.0"

EVENT_TIME_OFFSET_ORDERS = 0.00006
EVENT_TIME_OFFSET_REPLEN_IN = 0.00002
EVENT_TIME_OFFSET_REVIEW = 0.00008
EVENT_TIME_OFFSET_COLLECT = 0.00009
EVENT_TIME_OFFSET_WARM_UP = 0.00000

EVENT_PRIORITY_WARM_UP = 0
EVENT_PRIORITY_PERIOD = 1
EVENT_PRIORITY_REPLEN = 2
EVENT_PRIORITY_ORDERS = 3
EVENT_PRIORITY_REVIEW = 4
EVENT_PRIORITY_STATS = 5
EVENT_PRIORITY_END_RUN = 10

REPORT_NONE = 'none'
REPORT_SUMMARY = 'summary'
REPORT_SNAPSHOT = 'timeSeries'
REPORT_LOG = 'logEvents'
REPORT_PLOT = 'plot'

oneParameterDistributions = ['constant', 'poisson', 'exponential', 'geometric', 'weibull']

warmUpComplete = False
warmUpTime = 0


class Network:
    """
        Supply Chain Network
    """

    def __init__(self):
        self.sites_dc = {}
        self.lanes_dc = {}
        self.lanesSites_dc = {}
        self.lanesOrigin_dc = {}
        self.lanesDestination_dc = {}
        self.transport_dc = {}
        self.initLanesOrigin_dc = []
        self.initLanesDestination_dc = []
        self.initLanes_dc = []

    def site_exists(self, site_name):
        if site_name in self.sites_dc.keys():
            return True
        return False

    def transport_exists(self, transport_name):
        if transport_name in self.transport_dc.keys():
            return True
        return False

    def add_site(self, site):
        if not self.site_exists(site.name):
            self.sites_dc[site.name] = site

    def add_transport(self, transport):
        if not self.transport_exists(transport.name):
            self.transport_dc[transport.name] = transport

    def get_transport(self, transport_name, line_number):
        try:
            return self.transport_dc[transport_name]

        except KeyError:
            transport = create_transport(transport_name, line_number)
            try:
                if math.isnan(transport):
                    return transport

            except TypeError:
                self.add_transport(transport)
                return transport

    def create_blank_lanes_sites(self, origin, destination):
        if (origin.name, destination.name) not in self.lanesSites_dc.keys():
            self.lanesSites_dc[origin.name, destination.name] = []

    def create_transport_lane(self, origin, destination, transport_name, distance, line_number):
        # check if transport lane has already been defined
        if (origin.name, destination.name) in self.lanesSites_dc.keys():
            for item in self.lanesSites_dc[origin.name, destination.name]:
                if item.lineNumber == line_number:
                    return False
        else:
            self.lanesSites_dc[origin.name, destination.name] = []

        transport = self.get_transport(transport_name, line_number)

        try:
            if math.isnan(float(transport)):
                return False

        except TypeError:
            lane = TransportLane(origin, destination, transport, distance, line_number)

            self.lanesSites_dc[(origin.name, destination.name)].append(lane)

            if origin.name not in self.lanesOrigin_dc.keys():
                self.lanesOrigin_dc[origin.name] = []
            self.lanesOrigin_dc[origin.name].append(lane)

            if destination.name not in self.lanesDestination_dc.keys():
                self.lanesDestination_dc[destination.name] = []
            self.lanesDestination_dc[destination.name].append(lane)

            return True

    def get_origin_lanes(self, origin):
        if origin.name not in self.initLanesOrigin_dc:
            self.initLanesOrigin_dc.append(origin.name)
            set_origin_lanes(origin)

        if origin.name in self.lanesOrigin_dc.keys():
            return self.lanesOrigin_dc[origin.name]

        return []

    def get_destination_lanes(self, destination):
        if destination.name not in self.initLanesDestination_dc:
            self.initLanesDestination_dc.append(destination.name)
            set_destination_lanes(destination)

        if destination.name in self.lanesDestination_dc.keys():
            return self.lanesDestination_dc[destination.name]

        return []

    def get_sites_lanes(self, origin, destination):

        if (origin.name, destination.name) not in self.initLanes_dc:
            self.initLanes_dc.append((origin.name, destination.name))
            set_sites_lanes(origin, destination)

        return self.lanesSites_dc[(origin.name, destination.name)]


class Site:
    """
        Site
    """

    def __init__(self, name, groups_list, report_level):
        self.name = name
        self.type = get_site_type(name)
        self.products = {}
        self.inventoryPolicies = {}
        self.address = {}
        self.reportLevel = report_level
        self.groups = groups_list

    def add_product(self, name, product):

        if name in self.products:
            return
        self.products[name] = product

    def add_inventory_policy(self, name, inventory_policy):

        if name in self.inventoryPolicies:
            return
        self.inventoryPolicies[name] = inventory_policy

    def get_groups(self):
        return self.groups


class Supplier(Site):
    # subclass of Site

    def __init__(self, name, report_level):
        super(Supplier, self).__init__(name, report_level)

    attribute = 0

    def set_attribute(self, value):
        self.attribute = value


class Customer(Site):
    # subclass of Site

    def __init__(self, name, groups_list):
        super(Customer, self).__init__(name, groups_list, 0)


class Product:

    def __init__(self, name, groups_list):
        self.name = name
        self.inventoryPolicies = {}
        self.sites = {}
        self.weight = 0
        self.unitCost = 0
        self.groups = groups_list

        self.reportLevel = REPORT_NONE

    def add_site(self, name, site):
        if name in self.sites:
            return
        self.sites[name] = site

    def add_inventory_policy(self, name, inventory_policy):
        if name in self.inventoryPolicies:
            return
        self.inventoryPolicies[name] = inventory_policy

    def get_groups(self):
        return self.groups


class Transport:

    def __init__(self, name, fixed_cost, unit_cost, speed):
        self.name = name
        self.fixedCost = float(fixed_cost)
        self.unitCost = float(unit_cost)
        self.speed = float(speed)


class TransportLane:

    def __init__(self, from_site, to_site, transport, distance, line_number):
        self.name = from_site.name + '---' + to_site.name + '---' + transport.name
        self.transport = transport
        self.originSite = from_site
        self.destinationSite = to_site
        self.distance = float(distance)
        if transport.speed > 0:
            self.time = self.distance / transport.speed
        else:
            self.time = 0
        self.lineNumber = line_number


class InventoryPolicy:
    # combination of site and product makes up an Inventory Policy

    POLICY_s = 1
    POLICY_T = 2

    def __init__(self, env_, name, product, site, report_level):

        self.env = env_
        self.product = product
        self.site = site
        self.name = name
        self.orderTotal = 0
        self.orderTotal_ = 0
        self.orderFilled = 0
        self.orderFilled_ = 0
        self.qtyFilled = 0
        self.qtyFilled_ = 0
        self.totalOrdered = 0
        self.totalOrdered_ = 0
        self.totalShipped = 0
        self.totalShipped_ = 0
        self.inventoryPosition = 0
        self.onHandInventory = 0
        self.cumLate = 0
        self.cumLate_ = 0
        self.orderLate = 0
        self.orderLate_ = 0
        self.orderOnTime = 0
        self.orderOnTime_ = 0
        self.coq = 0
        self.boq = 0
        self.transportationCost = 0
        self.replenId = 0
        self.policy = 0
        self.policySettings = {}
        self.reviewYield = 0
        self.reviewThreshold = 0
        self.storeBackorder = simpy.Store(env_)
        self.storeOrderFIFO = simpy.Store(env_)
        self.storeInventory = simpy.Store(env_, 1)
        self.reviewPeriodChange = False
        self.obsTime = []
        self.obsInventoryOnHand = []
        self.obsInventoryPosition = []
        self.obsReorderAt = []
        self.obsReorderTo = []
        self.obsCoq = []
        self.obsBoq = []
        self.updatePeriod = 0
        self.plotXaxisList1 = []
        self.plotXaxisList2 = []
        self.queue = []
        self.reviewProcess = env_.process(self.review_inventory(env_))
        self.reportMenu = Report([report_level, self.site.reportLevel, globalReportingLevel])

        # Statistics plot

        if snapInterval > 0 and not site_product_report(self, REPORT_NONE):
            env_.process(self.collect_stats(env_))

    def set_policies(self, policies_list):
        self.onHandInventory = float(policies_list['inventory_initial'])
        self.inventoryPosition = self.onHandInventory
        self.policySettings = policies_list
        self.update_policies()

    def update_policies(self):

        global updateCompleted_ls

        while True:
            if review_policy_set(self):
                break

        if self not in updateCompleted_ls:
            updateCompleted_ls.append(self)

        if len(self.storeInventory.get_queue) > 0 and self.policySettings['inventory_review_period'] > 0:
            self.storeInventory.put(1)

        if self.reviewPeriodChange:
            if self.reviewYield > 0:
                self.reviewProcess.interrupt()
            self.reviewPeriodChange = False

    def set_policy_property(self, alias_, value_):

        if alias_ == 'inventory_review_period' and self.policySettings[alias_] != int(value_):
            self.reviewPeriodChange = True

        try:
            self.policySettings[alias_] = float(value_)

        except TypeError:
            self.policySettings[alias_] = value_

        self.update_policies()

    def collect_stats(self, env_):

        # yield env_.timeout(EVENT_TIME_OFFSET_COLLECT)
        yield schedule_event(0, EVENT_PRIORITY_STATS)
        while True:

            # Update plot statistics
            if site_product_report(self, REPORT_PLOT) and plotCreate:
                self.obsTime.append(env_.now)
                self.obsInventoryOnHand.append(self.onHandInventory)
                self.obsInventoryPosition.append(self.inventoryPosition)
                self.obsReorderAt.append(self.reviewThreshold)
                self.obsReorderTo.append(self.policySettings['inventory_order_up_to'])
                self.obsCoq.append(self.coq)
                self.obsBoq.append(self.boq)

            if site_product_report(self, REPORT_SNAPSHOT):
                # Update snapshot reports
                str1 = '{}\t{}\t{}'.format(format_time(runBeginDate + get_time_to_add(env.now)),
                                           self.site.name, self.product.name)
                global keepTimeSeriesMemory
                if keepTimeSeriesMemory:
                    global timeSeriesMemoryInv_ls, timeSeriesMemoryCoq_ls, timeSeriesMemoryBoq_ls

                    timeSeriesMemoryInv_ls.append('{}\t{:.0f}\n'.format(str1, self.onHandInventory))
                    timeSeriesMemoryCoq_ls.append('{}\t{:.0f}\n'.format(str1, self.coq))
                    timeSeriesMemoryBoq_ls.append('{}\t{:.0f}\n'.format(str1, self.boq))

                else:
                    fileInv.write('{}\t{:.0f}\n'.format(str1, self.onHandInventory))
                    fileCoq.write('{}\t{:.0f}\n'.format(str1, self.coq))
                    fileBoq.write('{}\t{:.0f}\n'.format(str1, self.boq))

            if site_product_report(self, REPORT_LOG):
                fileLog.write(
                    'Day {:.0f} ({}): Collect statistics for Site = {}, Product = {}, OH = {:.0f}, COQ = {:.0f}, '
                    'BOQ = {:.0f}\n'.format(env_.now + 1, format_time(runBeginDate + get_time_to_add(env.now)),
                                            self.site.name, self.product.name, self.onHandInventory, self.coq,
                                            self.boq))
                if consoleEvents:
                    print(
                        'Day {:.0f} ({}): Collect statistics for Site = {}, Product = {}, OH = {:.0f}, COQ = {:.0f}, '
                        'BOQ = {:.0f}'.format(env_.now + 1, format_time(runBeginDate + get_time_to_add(env.now)),
                                              self.site.name, self.product.name, self.onHandInventory, self.coq,
                                              self.boq))

            # yield env_.timeout(snapInterval)
            yield schedule_event(snapInterval, EVENT_PRIORITY_STATS)

    def review_inventory(self, env_):

        # yield env_.timeout(max(0, EVENT_TIME_OFFSET_REVIEW - divmod(env_.now, 1)[1]))
        yield schedule_event(0, EVENT_PRIORITY_REVIEW)

        if self.policySettings['inventory_review_period'] > 0:
            try:
                self.reviewYield += 1
                # yield env_.timeout(max(0, self.policySettings['inventory_review_period'] - 1 +
                #                       EVENT_TIME_OFFSET_REVIEW - divmod(env_.now, 1)[1]))
                yield schedule_event(self.policySettings['inventory_review_period'] - 1, EVENT_PRIORITY_REVIEW)
                self.reviewYield += 1

                self.print_review_msg(env_)
                self.check_inventory(env_)

            except simpy.Interrupt:
                pass

        while True:

            if self.policySettings['inventory_review_period'] > 0:

                try:
                    self.reviewYield += 1
                    # yield env_.timeout(self.policySettings['inventory_review_period'] +
                    #                   EVENT_TIME_OFFSET_REVIEW - divmod(env_.now, 1)[1])
                    yield schedule_event(self.policySettings['inventory_review_period'], EVENT_PRIORITY_REVIEW)
                    self.reviewYield += 1

                except simpy.Interrupt:
                    continue

                self.print_review_msg(env_)
                self.check_inventory(env_)

            else:
                yield self.storeInventory.get()
                self.check_inventory(env_)

    def update_complete(self):

        if site_product_report(self, REPORT_LOG):
            fileLog.write(
                "Day {:.0f} ({}): Update policy for Site = {}, Product = {}, ({}), RP = {:.0f}, RL = {:.0f}, "
                "RT = {:.0f}\n".format(self.env.now + 1, format_time(runBeginDate + get_time_to_add(self.env.now)),
                                       self.site.name, self.product.name, self.policySettings['inventory_policy'],
                                       self.reviewThreshold, self.policySettings['inventory_order_up_to'],
                                       self.policySettings['inventory_review_period']))
            if consoleEvents:
                print(
                    'Day {:.0f} ({}): Update policy for Site = {}, Product = {}, ({}), RP = {:.0f}, RL = {:.0f}, '
                    'RT = {:.0f}'.format(self.env.now + 1, format_time(runBeginDate + get_time_to_add(self.env.now)),
                                         self.site.name, self.product.name, self.policySettings['inventory_policy'],
                                         self.reviewThreshold, self.policySettings['inventory_order_up_to'],
                                         self.policySettings['inventory_review_period']))

        if reviewAtReset:
            self.print_review_msg(self.env)
            self.check_inventory(self.env)

    def print_review_msg(self, env_):

        if site_product_report(self, REPORT_LOG):
            fileLog.write(
                'Day {:.0f} ({}): Review inventory for Site = {}, Product = {}, OH = {:.0f}, POS = {:.0f}, '
                'RP = {:.0f}\n'.format(env_.now + 1, format_time(runBeginDate + get_time_to_add(env_.now)),
                                       self.site.name, self.product.name, self.onHandInventory,
                                       self.inventoryPosition,
                                       self.reviewThreshold))
            if consoleEvents:
                print(
                    'Day {:.0f} ({}): Review inventory for Site = {}, Product = {}, OH = {:.0f}, POS = {:.0f}, '
                    'RP = {:.0f}'.format(env_.now + 1, format_time(runBeginDate + get_time_to_add(env_.now)),
                                         self.site.name, self.product.name, self.onHandInventory,
                                         self.inventoryPosition,
                                         self.reviewThreshold))

    def check_inventory(self, env_):

        if self.policy == InventoryPolicy.POLICY_s:
            if self.inventoryPosition > self.policySettings['inventory_reorder_point']:
                return False

            qty = self.policySettings['inventory_order_up_to'] - self.inventoryPosition

        elif self.policy == InventoryPolicy.POLICY_T:
            if self.inventoryPosition >= self.policySettings['inventory_order_up_to']:
                return False

            qty = self.policySettings['inventory_order_up_to'] - self.inventoryPosition

        elif self.policySettings['inventory_reorder_qty'] > 0:

            qty = self.policySettings['inventory_reorder_qty']

        else:
            return False

        self.replenId += 1

        reorderName = 'SRC-{}_{}_{}'.format(self.site.name, self.product.name, self.replenId)

        self.coq += qty
        self.inventoryPosition = self.policySettings['inventory_order_up_to']

        sourceSiteName = get_sourcing_site_name(self)

        if sourceSiteName == '':
            # virtual source...product is not ordered from another site
            if site_product_report(self, REPORT_LOG):
                fileLog.write("Day {:.0f} ({}): Create replen order {} Qty = {:.0f} for Site = {}, "
                              "Product = {}, OH = {:.0f}, POS = {:.0f}, RP = {:.0f}\n".
                              format(env_.now + 1,
                                     format_time(runBeginDate + get_time_to_add(env_.now)), reorderName, qty,
                                     self.site.name,
                                     self.product.name, self.onHandInventory, self.inventoryPosition,
                                     self.reviewThreshold))
                if consoleEvents:
                    print('Day {:.0f} ({}): Create replen order {} Qty = {:.0f} for Site = {}, '
                          'Product = {}, OH = {:.0f}, POS = {:.0f}, RP = {:.0f}'.
                          format(env_.now + 1,
                                 format_time(runBeginDate + get_time_to_add(env_.now)), reorderName, qty,
                                 self.site.name,
                                 self.product.name, self.onHandInventory, self.inventoryPosition,
                                 self.reviewThreshold))

            env_.process(self.receive_replenishment(env_, qty, reorderName))

        else:
            # product must be ordered from another site

            if site_product_report(self, REPORT_LOG):
                fileLog.write('Day {:.0f} ({}): Send replen order {} Qty = {:.0f} to {} for Site = {}, '
                              'Product = {}, OH = {:.0f},  POS = {:.0f},  RP = {:.0f}\n'.
                              format(env_.now + 1, format_time(runBeginDate + get_time_to_add(env_.now)),
                                     reorderName,
                                     qty,
                                     sourceSiteName, self.site.name, self.product.name,
                                     self.onHandInventory, self.inventoryPosition, self.reviewThreshold))
                if consoleEvents:
                    print('Day {:.0f} ({}): Send replen order {} Qty = {:.0f} to {} for Site = {}, '
                          'Product = {}, OH = {:.0f},  POS = {:.0f},  RP = {:.0f}'.
                          format(env_.now + 1, format_time(runBeginDate + get_time_to_add(env_.now)),
                                 reorderName, qty, sourceSiteName, self.site.name, self.product.name,
                                 self.onHandInventory, self.inventoryPosition, self.reviewThreshold))

            date = format_time(runBeginDate + get_time_to_add(env_.now +
                                                              self.policySettings['sourcing_order_time']))

            create_order_item(env_, Order.TYPE_SUPPLIER, reorderName, "1", self.site.name,
                              get_inventory_policy(sourceSiteName, self.product.name),
                              qty, date, date, self.policySettings['sourcing_order_time'])

            check_updates_completed()

        return True

    def receive_replenishment(self, env_, qty, order_id):
        # replenishment event waits for leadtime

        # yield env_.timeout(self.policySettings['sourcing_ship_time'] + EVENT_TIME_OFFSET_REPLEN_IN -
        #                   divmod(env_.now, 1)[1])

        yield schedule_event(self.policySettings['sourcing_ship_time'], EVENT_PRIORITY_REPLEN)

        if len(self.storeBackorder.get_queue) > 0:
            yield self.storeBackorder.put(1)

        self.onHandInventory += qty
        self.coq -= qty

        if site_product_report(self, REPORT_LOG):
            fileLog.write('Day {:.0f} ({}): Receive replen order {} Qty {:.0f} at Site = {}, Product = {}, '
                          'OH = {:.0f},  POS = {:.0f}\n'.
                          format(env_.now + 1, format_time(runBeginDate + get_time_to_add(env_.now)), order_id, qty,
                                 self.site.name,
                                 self.product.name, self.onHandInventory, self.inventoryPosition))
            if consoleEvents:
                print('Day {:.0f} ({}): Receive replen order {} Qty {:.0f} at Site = {}, Product = {}, '
                      'OH = {:.0f},  POS = {:.0f}'.
                      format(env_.now + 1, format_time(runBeginDate + get_time_to_add(env_.now)), order_id, qty,
                             self.site.name, self.product.name, self.onHandInventory, self.inventoryPosition))


class Order:
    TYPE_CUSTOMER = 0
    TYPE_SUPPLIER = 1

    def __init__(self, order_type, order_number, customer, order_date, order_due_date):
        self.type = order_type
        self.orderNumber = order_number
        self.customer = customer
        self.date = order_date
        self.dueDate = order_due_date
        self.orderItems = []

    def add_order_item(self, order_item):
        self.orderItems.append(order_item)
        return len(self.orderItems)


class OrderItem:

    def __init__(self, env_, order, order_line_number, inventory_policy, qty, delay_time, order_date, due_date):
        self.env = env_
        self.inventoryPolicy = inventory_policy
        self.qty = qty
        self.order = order
        self.orderLineNumber = order_line_number
        self.orderItemNumber = order.add_order_item(self)
        self.orderDate = order_date
        self.orderDueDate = due_date
        self.transportLane = 0

        env_.process(self.process_order_item(delay_time))

    def process_order_item(self, receive_time):

        global totalOrdersShipped, totalOrdersShipped_
        global totalOrdersReceived, totalOrdersReceived_
        global keepOrdersMemory, keepShipmentsMemory
        global ordersMemory

        orderNumber = self.order.orderNumber
        customer = self.order.customer
        # orderDate = self.order.orderDate
        # orderDueDate = self.order.orderDueDate
        orderDate = self.orderDate
        orderDueDate = self.orderDueDate

        # yield self.env.timeout(max(0, receive_time + EVENT_TIME_OFFSET_ORDERS - d))
        yield schedule_event(receive_time, EVENT_PRIORITY_ORDERS)

        if warmUpComplete:
            self.inventoryPolicy.orderTotal_ += 1
            self.inventoryPolicy.totalOrdered_ += self.qty
            totalOrdersReceived_ += 1

        totalOrdersReceived += 1
        self.inventoryPolicy.orderTotal += 1
        self.inventoryPolicy.totalOrdered += self.qty
        self.inventoryPolicy.inventoryPosition -= self.qty

        if site_product_report(self.inventoryPolicy, REPORT_LOG):
            fileLog.write('Day {:.0f} ({}): Receive order {} - qty {:.0f} at Site = {}, Product = {}\n'.
                          format(self.env.now + 1,
                                 format_time(runBeginDate + get_time_to_add(self.env.now)), orderNumber,
                                 self.qty, self.inventoryPolicy.site.name, self.inventoryPolicy.product.name))
            if consoleEvents:
                print('Day {:.0f} ({}): Receive order {} - qty {:.0f} at Site = {}, Product = {}'.
                      format(self.env.now + 1, format_time(runBeginDate + get_time_to_add(self.env.now)),
                             orderNumber, self.qty, self.inventoryPolicy.site.name, self.inventoryPolicy.product.name))

        if keepOrdersMemory:
            ordersMemory.append('{}\t{}\t{}\t{}\t{}\t{}\t{:.0f}\t{}\n'.
                                format(orderDate, self.order.orderNumber, self.orderLineNumber,
                                       self.inventoryPolicy.site.name, customer, self.inventoryPolicy.product.name,
                                       self.qty, orderDueDate))
        else:
            fileOrder.write('{}\t{}\t{}\t{}\t{}\t{}\t{:.0f}\t{}\n'.
                            format(orderDate, self.order.orderNumber, self.orderLineNumber,
                                   self.inventoryPolicy.site.name, customer, self.inventoryPolicy.product.name,
                                   self.qty, orderDueDate))

        if not fulfillOrders:
            order_item_removed(self.env.now)
            return

        if self.inventoryPolicy.policySettings['inventory_review_period'] == 0:
            self.inventoryPolicy.check_inventory(self.env)

        # OrderItems are processed FIFO
        self.inventoryPolicy.queue.append(self)
        if len(self.inventoryPolicy.storeBackorder.get_queue) > 0 or \
                len(self.inventoryPolicy.storeOrderFIFO.get_queue) > 0:
            # There are orders that are already queued up or on backorder

            if site_product_report(self.inventoryPolicy, REPORT_LOG):
                fileLog.write('Day {:.0f} ({}): Queue order {} - qty {:.0f} at Site = {}, Product = {}\n'.
                              format(self.env.now + 1, format_time(runBeginDate + get_time_to_add(self.env.now)),
                                     orderNumber, self.qty, self.inventoryPolicy.site.name,
                                     self.inventoryPolicy.product.name))
                if consoleEvents:
                    print('Day {:.0f} ({}): Queue order {} - qty {:.0f} at Site = {}, Product = {}'.
                          format(self.env.now + 1,
                                 format_time(runBeginDate + get_time_to_add(self.env.now)), orderNumber,
                                 self.qty, self.inventoryPolicy.site.name, self.inventoryPolicy.product.name))

            qtyBackOrder = self.qty
            self.inventoryPolicy.boq += qtyBackOrder

            # following orders queue up behind current order on backorder
            yield self.inventoryPolicy.storeOrderFIFO.get()

            if site_product_report(self.inventoryPolicy, REPORT_LOG):
                fileLog.write('Day {:.0f} ({}): Unqueue order {} - qty {:.0f} at Site = {}, Product = {}\n'.
                              format(self.env.now + 1, format_time(runBeginDate + get_time_to_add(self.env.now)),
                                     orderNumber, self.qty, self.inventoryPolicy.site.name,
                                     self.inventoryPolicy.product.name))
                if consoleEvents:
                    print('Day {:.0f} ({}): Unqueue order {} - qty {:.0f} at Site = {}, Product = {}'.
                          format(self.env.now + 1,
                                 format_time(runBeginDate + get_time_to_add(self.env.now)), orderNumber,
                                 self.qty, self.inventoryPolicy.site.name, self.inventoryPolicy.product.name))

        else:
            if self.inventoryPolicy.onHandInventory >= self.qty:
                qtyBackOrder = 0
            else:
                qtyBackOrder = self.qty - self.inventoryPolicy.onHandInventory
                self.inventoryPolicy.boq += qtyBackOrder

        if self.inventoryPolicy.onHandInventory < self.qty:
            # Track the number of orders backordered

            if site_product_report(self.inventoryPolicy, REPORT_LOG):
                fileLog.write('Day {:.0f} ({}): Backorder order {} - qty {:.0f} at Site = {}, '
                              'Product = {}\n'.format(self.env.now + 1,
                                                      format_time(runBeginDate + get_time_to_add(self.env.now)),
                                                      orderNumber, self.qty, self.inventoryPolicy.site.name,
                                                      self.inventoryPolicy.product.name))

                if consoleEvents:
                    print('Day {:.0f} ({}): Backorder order {} - qty {:.0f} at Site = {}, Product = {}'.
                          format(self.env.now + 1,
                                 format_time(runBeginDate + get_time_to_add(self.env.now)), orderNumber,
                                 self.qty, self.inventoryPolicy.site.name, self.inventoryPolicy.product.name))

            while self.inventoryPolicy.onHandInventory < self.qty:
                # Hold until sufficient replenishment
                yield self.inventoryPolicy.storeBackorder.get()

        if len(self.inventoryPolicy.storeOrderFIFO.get_queue) > 0:
            yield self.inventoryPolicy.storeOrderFIFO.put('')

        self.inventoryPolicy.queue.remove(self)

        # ship order

        self.inventoryPolicy.onHandInventory -= self.qty
        self.inventoryPolicy.boq -= qtyBackOrder
        self.inventoryPolicy.totalShipped += self.qty
        self.inventoryPolicy.orderFilled += 1

        if warmUpComplete:
            self.inventoryPolicy.totalShipped_ += self.qty
            self.inventoryPolicy.orderFilled_ += 1

        shipDate = (get_strp_time(orderDueDate) - runBeginDate).days

        if shipDate + EVENT_TIME_OFFSET_ORDERS >= self.env.now:
            if warmUpComplete:
                self.inventoryPolicy.orderOnTime_ += 1
                self.inventoryPolicy.qtyFilled_ += self.qty

            self.inventoryPolicy.orderOnTime += 1
            self.inventoryPolicy.qtyFilled += self.qty

            if site_product_report(self.inventoryPolicy, REPORT_LOG):
                fileLog.write('Day {:.0f} ({}): Ship order {} - qty {:.0f} on time at Site = {}, Product = {}\n'.
                              format(self.env.now + 1, format_time(runBeginDate + get_time_to_add(self.env.now)),
                                     orderNumber, self.qty, self.inventoryPolicy.site.name,
                                     self.inventoryPolicy.product.name))
                if consoleEvents:
                    print('Day {:.0f} ({}): Ship order {} - qty {:.0f} on time at Site = {}, Product = {}'.
                          format(self.env.now + 1,
                                 format_time(runBeginDate + get_time_to_add(self.env.now)), orderNumber,
                                 self.qty, self.inventoryPolicy.site.name, self.inventoryPolicy.product.name))
        else:
            if warmUpComplete:
                self.inventoryPolicy.cumLate_ += self.env.now - shipDate
                self.inventoryPolicy.orderLate_ += 1

            self.inventoryPolicy.cumLate += self.env.now - shipDate
            self.inventoryPolicy.orderLate += 1

            if site_product_report(self.inventoryPolicy, REPORT_LOG):
                fileLog.write('Day {:.0f} ({}): Ship order {} - qty {:.0f} late at Site = {}, Product = {}\n'.
                              format(self.env.now + 1, format_time(runBeginDate + get_time_to_add(self.env.now)),
                                     orderNumber, self.qty, self.inventoryPolicy.site.name,
                                     self.inventoryPolicy.product.name))

                if consoleEvents:
                    print('Day {:.0f} ({}): Ship order {} - qty {:.0f} late at Site = {}, Product = {}'.
                          format(self.env.now + 1,
                                 format_time(runBeginDate + get_time_to_add(self.env.now)), orderNumber,
                                 self.qty, self.inventoryPolicy.site.name, self.inventoryPolicy.product.name))

        if keepShipmentsMemory:
            global shipmentsMemory_ls
            shipmentsMemory_ls.append('{}\t{}\t{}\t{}\t{}\t{}\t{:.0f}\t{}\n '.
                                      format(format_time(runBeginDate + get_time_to_add(self.env.now)),
                                             self.order.orderNumber, self.orderLineNumber,
                                             self.inventoryPolicy.site.name, customer,
                                             self.inventoryPolicy.product.name, self.qty, orderDueDate))
        else:
            fileShip.write('{}\t{}\t{}\t{}\t{}\t{}\t{:.0f}\t{}\n '.
                           format(format_time(runBeginDate + get_time_to_add(self.env.now)), self.order.orderNumber,
                                  self.orderLineNumber, self.inventoryPolicy.site.name, customer,
                                  self.inventoryPolicy.product.name, self.qty, orderDueDate))

        totalOrdersShipped += 1

        if warmUpComplete:
            totalOrdersShipped_ += 1

        if self.order.type == Order.TYPE_SUPPLIER:
            # orderitem needs to be sent to destination site

            # d = divmod(self.env.now, 1)[1]
            # yield self.env.timeout(self.inventoryPolicy.policySettings['sourcing_ship_time'] +
            # EVENT_TIME_OFFSET_REPLEN_IN - d)

            self.transportLane = assign_transport_lane(self, self.inventoryPolicy.site, get_site(self.order.customer))
            yield schedule_event(self.inventoryPolicy.policySettings['sourcing_ship_time'], EVENT_PRIORITY_REPLEN)

            self.order_item_replenished()
        else:
            order_item_removed(self.env.now)

    def order_item_replenished(self):
        inventoryPolicy = get_inventory_policy(self.order.customer, self.inventoryPolicy.product.name)

        if len(inventoryPolicy.storeBackorder.get_queue) > 0:
            inventoryPolicy.storeBackorder.put('')

        inventoryPolicy.onHandInventory += self.qty
        inventoryPolicy.coq -= self.qty

        inventoryPolicy.transportationCost += (inventoryPolicy.policySettings['sourcing_order_cost'] +
                                               inventoryPolicy.policySettings['sourcing_unit_cost'] * self.qty)

        if site_product_report(self.inventoryPolicy, REPORT_LOG):
            fileLog.write('Day {:.0f} ({}): Receive replen order {} Qty = {:.0f} at Site = {}, '
                          'Product = {}, OH = {:.0f},  POS = {:.0f}\n'.
                          format(self.env.now + 1, format_time(runBeginDate + get_time_to_add(self.env.now)),
                                 self.order.orderNumber,
                                 self.qty, inventoryPolicy.site.name, inventoryPolicy.product.name,
                                 inventoryPolicy.onHandInventory,
                                 inventoryPolicy.inventoryPosition))
            if consoleEvents:
                print('Day {:.0f} ({}): Receive replen order {} Qty = {:.0f} at Site = {}, Product = {}, '
                      'OH = {:.0f},  POS = {:.0f}'.
                      format(self.env.now + 1,
                             format_time(runBeginDate + get_time_to_add(self.env.now)),
                             self.order.orderNumber,
                             self.qty, inventoryPolicy.site.name, inventoryPolicy.product.name,
                             inventoryPolicy.onHandInventory,
                             inventoryPolicy.inventoryPosition))


class Shipment:

    def __init__(self, name):
        self.name = name
        self.orderItems = {}


# noinspection PyTypeChecker
class Demand:
    def __init__(self, env_, line_number, day, period_time, product_name, customer_name, source_site, demand_qty,
                 demand_period, order_qty_avg, order_qty_cv, order_qty_dist, delivery_due_date, first_order_number,
                 force_sum_demand, random_each_order):
        global demandGenerated
        try:
            if math.isnan(float(customer_name)):
                pass
        except ValueError:
            if customer_name in customersExclude_ls:
                demandGenerated -= 1
                simulation_terminate(env_.now)
                return

        try:
            if math.isnan(float(source_site)):
                pass
        except ValueError:
            if source_site in sitesExclude_ls:
                demandGenerated -= 1
                simulation_terminate(env_.now)
                return

        self.env = env_
        self.lineNumber = line_number
        self.day = day
        self.periodTime = period_time
        self.productName = product_name
        self.customerName = customer_name
        self.sourceSite = source_site
        self.demandQty = demand_qty
        self.demandPeriod = demand_period
        self.orderQtyAvg = order_qty_avg
        self.orderQtyCV = order_qty_cv
        self.orderQtyDist = order_qty_dist
        self.deliveryDueDate = delivery_due_date
        self.firstOrderNumber = first_order_number
        self.forceSumDemand = force_sum_demand
        self.randomizeEachOrder = random_each_order
        self.reduceMean = 0
        self.addOne = 0
        self.ReportError = True

        env_.process(self.generate_demand())

    # noinspection PyUnboundLocalVariable
    def generate_demand(self):
        global orderNumberDefault, orderItemsRemain, demandGenerated

        customerName = ''
        siteName = ''
        orderNumber = 'NaN'
        productName = demand_get_product(self.productName, self.lineNumber)

        try:
            if math.isnan(float(self.randomizeEachOrder)):
                self.randomizeEachOrder = to_bool(demandDefaults['random_each_order'])
        except ValueError:
            if not self.randomizeEachOrder:
                self.randomizeEachOrder = to_bool(demandDefaults['random_each_order'])
            else:
                self.randomizeEachOrder = to_bool(self.randomizeEachOrder)

        if not self.randomizeEachOrder:
            customerName = demand_get_customer(self.customerName)
            siteName = demand_get_site(self.sourceSite, customerName, productName)

        valueList = demand_quantity_stochastic(self.orderQtyAvg, self.orderQtyCV, self.orderQtyDist, self.lineNumber)

        if not valueList[0]:
            demandQty = float(self.demandQty)
            try:
                if math.isnan(demandQty):
                    demandQty = float(demandDefaults['order_qty'])
                    write_exception_missing(demandAliasHeader_dc['demand_qty'], 'Set to default value.', demandQty,
                                            'Demand.txt', self.lineNumber)
                else:
                    if demandQty < 0:
                        write_exception_invalid(demandQty, demandAliasHeader_dc['demand_qty'],
                                                'Set to positive value:',
                                                -demandQty, 'Demand.txt', self.lineNumber)
                        demandQty = -demandQty

                    if int(round(demandQty)) == 0:
                        write_exception_invalid(demandQty, demandAliasHeader_dc['demand_qty'],
                                                'Set to new value:',
                                                '1', 'Demand.txt', self.lineNumber)
                        demandQty = 1

            except ValueError:
                write_exception_invalid(demandQty, demandAliasHeader_dc['demand_qty'], 'Set to default value.',
                                        demandDefaults['order_qty'], 'Demand.txt', self.lineNumber)
                demandQty = float(demandDefaults['order_qty'])

        else:
            self.orderQtyDist = valueList[1]
            self.orderQtyAvg = valueList[2]
            self.orderQtyCV = valueList[3]
            demandQty = 0

        try:
            if math.isnan(float(self.demandPeriod)):
                self.demandPeriod = demandDefaults['period']
        except ValueError:
            if not self.demandPeriod:
                self.demandPeriod = demandDefaults['period']
            else:
                self.demandPeriod = self.demandPeriod.lower()

        try:
            if not math.isnan(float(self.firstOrderNumber)):
                orderNumber = float(self.firstOrderNumber)
        except ValueError:
            pass

        try:
            if math.isnan(float(self.deliveryDueDate)):
                self.deliveryDueDate = float(demandDefaults['delivery_due_date'])
            else:
                self.deliveryDueDate = float(self.deliveryDueDate)

        except ValueError:
            self.deliveryDueDate = float(demandDefaults['delivery_due_date'])

        try:
            if math.isnan(float(self.forceSumDemand)):
                self.forceSumDemand = to_bool(demandDefaults['force_sum'])
        except ValueError:
            if not self.forceSumDemand:
                self.forceSumDemand = to_bool(demandDefaults['force_sum'])
            else:
                self.forceSumDemand = to_bool(self.forceSumDemand)

        orders_dict = []

        if self.demandPeriod == 'day' or self.demandPeriod == 'second' or self.demandPeriod == 'minute' or \
                self.demandPeriod == 'hour':
            if demandQty > 0:
                numOrders = min(self.periodTime, demandQty)
                orderSize = demandQty // numOrders
                orderResidual = demandQty - numOrders * orderSize
            else:
                numOrders = self.periodTime

            j = 0
            for i in range(int(numOrders)):
                # noinspection PyUnboundLocalVariable
                if demandQty == 0:
                    orders_dict.append([(i - j), self.sample_order_qty()])
                elif self.forceSumDemand and i < orderResidual:
                    # noinspection PyUnboundLocalVariable,PyUnboundLocalVariable
                    orders_dict.append([(i - j), orderSize + 1])
                else:
                    orders_dict.append([(i - j), orderSize])

                j = i

        elif self.demandPeriod == 'week' or self.demandPeriod == 'biweekly':
            if self.demandPeriod == 'week':
                dayCount = 7
            else:
                dayCount = 14

            numOrders = ((self.periodTime - 1) // dayCount) + 1

            if demandQty > 0:
                numOrders = min(numOrders, demandQty)
                orderSize = demandQty // numOrders
                orderResidual = demandQty - numOrders * orderSize

            j = 0
            for i in range(int(numOrders)):
                # noinspection PyUnboundLocalVariable
                if demandQty == 0:
                    orders_dict.append([j, self.sample_order_qty()])
                elif self.forceSumDemand and i < orderResidual:
                    orders_dict.append([j, orderSize + 1])
                else:
                    orders_dict.append([j, orderSize])

                j = dayCount

        elif self.demandPeriod == 'month':
            numOrders = max(1, monthInPeriod[self.day])

            if demandQty > 0:
                numOrders = min(numOrders, demandQty)
                orderSize = demandQty // numOrders
                orderResidual = demandQty - numOrders * orderSize

            dayInMonth = 0

            theDate = get_strp_time(self.day)

            for i in range(int(numOrders)):
                # noinspection PyUnboundLocalVariable
                if demandQty == 0:
                    orders_dict.append([dayInMonth, self.sample_order_qty()])
                elif self.forceSumDemand and i < orderResidual:
                    orders_dict.append([dayInMonth, orderSize + 1])
                else:
                    orders_dict.append([dayInMonth, orderSize])

                if i + 1 == numOrders:
                    break

                dayInMonth = cl.monthrange(theDate.year, theDate.month)[1]
                theDate += dayInMonth

        elif self.demandPeriod == 'year':
            numOrders = max(1, monthInPeriod[self.day] // 12)

            if demandQty > 0:
                numOrders = min(numOrders, demandQty)
                orderSize = demandQty // numOrders
                orderResidual = demandQty - numOrders * orderSize

            dayInYear = 0
            theDate = get_strp_time(self.day)

            for i in range(int(numOrders)):
                if demandQty == 0:
                    orders_dict.append([dayInYear, self.sample_order_qty()])
                elif self.forceSumDemand and i < orderResidual:
                    orders_dict.append([dayInYear, orderSize + 1])
                else:
                    orders_dict.append([dayInYear, orderSize])

                if i + 1 == numOrders:
                    break

                dayInYear = 0
                for _ in range(12):
                    dayInMonth = cl.monthrange(theDate.year, theDate.month)[1]
                    theDate += dayInMonth
                    dayInYear += dayInMonth

        else:
            pass

        orderItemsRemain += len(orders_dict)

        for item in orders_dict:
            yield schedule_event(item[0], EVENT_PRIORITY_ORDERS)

            if int(item[1]) == 0:
                if np.random.random_sample() < 0.5:
                    write_exception_amend('0', 'Order quantity sampled:', 'Order discarded:', qty, 'Demand.txt',
                                          self.lineNumber)
                    continue
                qty = 1
                write_exception_amend('0', 'Order quantity sampled', 'Order quantity reset:', qty, 'Demand.txt',
                                      self.lineNumber)
            else:
                qty = int(item[1])

            if self.randomizeEachOrder:
                customerName = demand_get_customer(self.customerName)
                siteName = demand_get_site(self.sourceSite, customerName, productName)

            try:
                if math.isnan(float(orderNumber)):
                    orderNum = orderNumberDefault
                    orderNumberDefault += 1
                else:
                    orderNum = orderNumber
                    orderNumber += 1
            except ValueError:
                orderNum = orderNumberDefault
                orderNumberDefault += 1

            create_order(self.env, 'TY2-{}-{:.0f}'.format(self.lineNumber, orderNum), '1', productName, qty,
                         customerName, format_time(runBeginDate + get_time_to_add(self.env.now)), siteName,
                         format_time(runBeginDate + get_time_to_add(self.env.now + float(self.deliveryDueDate))), 0)

        demandGenerated -= 1
        simulation_terminate(self.env.now)

    def sample_order_qty(self):

        while True:
            qty = int(round(sample_from_distribution(self.orderQtyDist, self.orderQtyAvg,
                                                     self.orderQtyCV))) + self.addOne

            if qty < 0:
                write_exception_amend('Negative value', 'Order quantity sampled:', 'Order discarded:', qty,
                                      'Demand.txt', self.lineNumber)
                if self.ReportError:
                    print('-- Day {:.0f} ({}): Negative order quantity sampled in Demand.txt line {:.0f}. Resampling.'.
                          format(self.env.now + 1, format_time(runBeginDate + get_time_to_add(self.env.now)),
                                 self.lineNumber))
                continue

            if qty == 0:
                self.orderQtyAvg -= 0.5
                self.addOne = 0.5
                qty = 1

            return qty


class DemandArrival:

    def __init__(self, env_, data_series):
        global demandArrivalGenerated

        self.env = env_
        self.productName = data_series['product_name']
        self.customerName = data_series['customer_name']
        self.sourceSite = data_series['source_site']

        try:
            if math.isnan(float(self.customerName)):
                pass
        except ValueError:
            if self.customerName in customersExclude_ls:
                demandArrivalGenerated -= 1
                simulation_terminate(env_.now)
                return

        try:
            if math.isnan(float(self.sourceSite)):
                pass
        except ValueError:
            if self.sourceSite in sitesExclude_ls:
                demandArrivalGenerated -= 1
                simulation_terminate(env_.now)
                return

        self.demandQtyAvg = data_series['demand_qty_avg']
        self.demandQtyCV = data_series['demand_qty_cv']
        self.demandQtyDist = data_series['demand_qty_dist']
        self.orderQtyAvg = data_series['order_qty_avg']
        self.orderQtyCV = data_series['order_qty_cv']
        self.orderQtyDist = data_series['order_qty_dist']
        self.firstOrderTimeAvg = data_series['first_order_time_avg']
        self.firstOrderTimeCV = data_series['first_order_time_cv']
        self.firstOrderTimeDist = data_series['first_order_time_dist']
        self.timeBetweenOrdersAvg = data_series['time_bet_orders_avg']
        self.timeBetweenOrdersCV = data_series['time_bet_orders_cv']
        self.timeBetweenOrdersDist = data_series['time_bet_orders_dist']
        self.numberOfOrders = data_series['number_of_orders']
        self.deliveryDueDate = float(data_series['delivery_due_date'])
        self.shippingMethod = data_series['shipping_method']
        self.firstOrderNumber = data_series['first_order_number']
        self.forceSumDemand = data_series['force_sum_demand']
        self.randomizeEachOrder = data_series['random_each_order']
        self.lineNumber = data_series['line_number']
        self.addOne = 0
        self.ReportError = True

        env_.process(self.generate_demand_arrival())

    def generate_demand_arrival(self):
        global orderNumberDefault, orderItemsRemain, demandArrivalGenerated
        customerName = ''
        siteName = ''
        orderNumber = 'NaN'

        try:
            if math.isnan(float(self.numberOfOrders)):
                self.numberOfOrders = float(demandArrivalsDefaults['number_of_orders'])
            else:
                self.numberOfOrders = float(self.numberOfOrders)
        except ValueError:
            self.numberOfOrders = float(self.numberOfOrders)

        if math.isnan(self.numberOfOrders) and runLength == 0:
            print('\n\nDATA ERROR: Must specify either a simulation run end date or NumberOfOrders on line {} in '
                  'DemandArrivals.txt\n'.format(self.lineNumber))
            terminate_simulation(True)

        if self.numberOfOrders == 0:
            demandArrivalGenerated -= 1
            simulation_terminate(self.env.now)
            return

        productName = demand_get_product(self.productName, self.lineNumber)

        try:
            if math.isnan(float(self.randomizeEachOrder)):
                self.randomizeEachOrder = demandArrivalsDefaults['random_each_order']
            else:
                self.randomizeEachOrder = to_bool(self.randomizeEachOrder)
        except ValueError:
            self.randomizeEachOrder = to_bool(self.randomizeEachOrder)

        if not self.randomizeEachOrder:
            customerName = demand_get_customer(self.customerName)
            siteName = demand_get_site(self.sourceSite, customerName, productName)

        try:
            if not math.isnan(float(self.firstOrderNumber)):
                orderNumber = float(self.firstOrderNumber)
        except ValueError:
            pass

        try:
            if math.isnan(self.deliveryDueDate):
                self.deliveryDueDate = float(demandArrivalsDefaults['delivery_due_date'])
            else:
                self.deliveryDueDate = float(self.deliveryDueDate)

        except ValueError:
            self.deliveryDueDate = float(demandArrivalsDefaults['delivery_due_date'])

        arrivals = []
        totalTime = 0

        time = first_time_arrival_stochastic(self.firstOrderTimeAvg, self.firstOrderTimeCV,
                                             self.firstOrderTimeDist, self.lineNumber)

        try:
            if not math.isnan(float(time)):
                arrivals.append(time)
                totalTime = time
        except ValueError:
            pass

        firstTime = True
        while len(arrivals) < self.numberOfOrders and (totalTime < runLength or runLength == 0):

            if firstTime:
                valueList = time_between_arrival_stochastic(self.timeBetweenOrdersAvg, self.timeBetweenOrdersCV,
                                                            self.timeBetweenOrdersDist, self.lineNumber)
                time = valueList[0]
                self.timeBetweenOrdersDist = valueList[1]
                self.timeBetweenOrdersAvg = valueList[2]
                self.timeBetweenOrdersCV = valueList[3]
                firstTime = False
            else:
                time = sample_from_distribution(self.timeBetweenOrdersDist, self.timeBetweenOrdersAvg,
                                                self.timeBetweenOrdersCV)

            totalTime += time
            if totalTime < runLength or runLength == 0:
                arrivals.append(time)

        if len(arrivals) < self.numberOfOrders:
            write_exception_invalid(int(self.numberOfOrders), demandArrivalsAliasHeader_dc['number_of_orders'],
                                    'Fewer orders than specified created:', len(arrivals),
                                    'DemandArrivals.txt', self.lineNumber)

        orders_dict = []

        firstTime = True
        while True:

            if firstTime:
                valueList = self.quantity_stochastic(self.orderQtyAvg, self.orderQtyCV, self.orderQtyDist, 1)

                qty = valueList[0]
                try:
                    if math.isnan(float(qty)):
                        break
                except ValueError:
                    pass

                self.orderQtyDist = valueList[1]
                self.orderQtyAvg = valueList[2]
                self.orderQtyCV = valueList[3]

                firstTime = False

                if qty <= 0:
                    qty = self.sample_order_qty()
            else:
                qty = self.sample_order_qty()

            orders_dict.append([arrivals[0], qty])
            arrivals.pop(0)
            if len(arrivals) == 0:
                break

        numOrders = len(arrivals)
        if numOrders > 0:

            try:
                if math.isnan(float(self.forceSumDemand)):
                    forceSum = to_bool(demandArrivalsDefaults['force_sum_demand'])
                else:
                    forceSum = to_bool(self.forceSumDemand)

            except ValueError:
                forceSum = to_bool(self.forceSumDemand)

            qty = self.quantity_stochastic(self.demandQtyAvg, self.demandQtyCV, self.demandQtyDist, 2)[0]

            if qty == 0:
                """
                if np.random.random_sample() < 0.5:
                    demandArrivalGenerated -= 1
                    simulation_terminate(self.env.now)
                    return
                """
                numOrders = 1
                orderSize = 1
                orderResidual = 0
            else:
                while qty < 0:
                    write_exception_amend('Negative value', 'Order quantity sampled:', 'Order discarded:', qty,
                                          'DemandArrivals.txt', self.lineNumber)

                    if self.ReportError:
                        print(
                            '-- Day {:.0f} ({}): Negative order quantity sampled in DemandArrivals.txt line {:.0f}. '
                            'Resampling.'.format(self.env.now + 1,
                                                 format_time(runBeginDate + get_time_to_add(self.env.now)),
                                                 self.lineNumber))

                    qty = self.quantity_stochastic(self.demandQtyAvg, self.demandQtyCV, self.demandQtyDist, 2)[0]

                numOrders = min(numOrders, int(qty))
                orderSize = qty // numOrders
                orderResidual = qty - numOrders * orderSize

            for i in range(numOrders):
                if forceSum and i < orderResidual:
                    orders_dict.append([arrivals[i], orderSize + 1])
                else:
                    orders_dict.append([arrivals[i], orderSize])

        orderItemsRemain += len(orders_dict)

        for item in orders_dict:
            yield schedule_event(item[0], EVENT_PRIORITY_ORDERS)

            qty = item[1]
            if qty == 0:
                if np.random.random_sample() < 0.5:
                    write_exception_amend('0', 'Order quantity sampled:', 'Order discarded:', qty, 'DemandArrivals.txt',
                                          self.lineNumber)
                    continue
                qty = 1
                write_exception_amend('0', 'Order quantity sampled:', 'Order quantity reset:', qty,
                                      'DemandArrivals.txt', self.lineNumber)

            if self.randomizeEachOrder:
                customerName = demand_get_customer(self.customerName)
                siteName = demand_get_site(self.sourceSite, customerName, productName)

            try:
                if math.isnan(float(orderNumber)):
                    orderNum = orderNumberDefault
                    orderNumberDefault += 1
                else:
                    orderNum = orderNumber
                    orderNumber += 1
            except ValueError:
                orderNum = orderNumberDefault
                orderNumberDefault += 1

            create_order(self.env, 'TY4-{}-{:.0f}'.format(self.lineNumber, orderNum), '1', productName, qty,
                         customerName, format_time(runBeginDate + get_time_to_add(self.env.now)), siteName,
                         format_time(runBeginDate + get_time_to_add(self.env.now + self.deliveryDueDate)), 0)

        demandArrivalGenerated -= 1
        simulation_terminate(self.env.now)

    def sample_order_qty(self):

        while True:
            qty = int(round(sample_from_distribution(self.orderQtyDist, self.orderQtyAvg,
                                                     self.orderQtyCV))) + self.addOne

            if qty < 0:
                write_exception_amend('Negative value', 'Order quantity sampled:', 'Order discarded:', qty,
                                      'DemandArrivals.txt', self.lineNumber)
                if self.ReportError:
                    print(
                        '-- Day {:.0f} ({}): Negative order quantity sampled in DemandArrivals.txt line {:.0f}. '
                        'Resampling.'.format(self.env.now + 1,
                                             format_time(runBeginDate + get_time_to_add(self.env.now)),
                                             self.lineNumber))
                continue

            if qty == 0:
                self.orderQtyAvg -= 0.5
                self.addOne = 0.5
                qty = 1

            return qty

    def quantity_stochastic(self, qty_avg, qty_cv, qty_dist, qty_type):
        missingValues = []
        stochasticQty = 0
        try:
            if math.isnan(float(qty_avg)):
                missingValues.append(True)
                stochasticQty += 1
            else:
                missingValues.append(False)
        except ValueError:
            missingValues.append(False)
        try:
            if math.isnan(float(qty_cv)):
                missingValues.append(True)
                stochasticQty += 1
            else:
                missingValues.append(False)
        except ValueError:
            missingValues.append(False)
        try:
            if math.isnan(float(qty_dist)):
                missingValues.append(True)
                stochasticQty += 1
            else:
                missingValues.append(False)
        except ValueError:
            missingValues.append(False)

        if stochasticQty == 3 and qty_type == 1:
            return [float('NaN')]

        if missingValues[0]:
            if qty_type == 1:
                qtyAverage = float(demandArrivalsDefaults['order_qty_avg'])
                write_exception_missing(demandArrivalsAliasHeader_dc['order_qty_avg'], 'Set to default value.',
                                        qtyAverage, 'DemandArrivals.txt', self.lineNumber)
            else:
                qtyAverage = float(demandArrivalsDefaults['demand_qty_avg'])
                write_exception_missing(demandArrivalsAliasHeader_dc['demand_qty_avg'], 'Set to default value.',
                                        qtyAverage, 'DemandArrivals.txt', self.lineNumber)
        else:
            qtyAverage = float(qty_avg)

        if missingValues[2]:
            if qty_type == 1:
                qtyDist = demandArrivalsDefaults['order_qty_dist']
                write_exception_missing(demandArrivalsAliasHeader_dc['order_qty_dist'], 'Set to default value.',
                                        qtyDist, 'DemandArrivals.txt', self.lineNumber)
            else:
                qtyDist = demandArrivalsDefaults['demand_qty_dist']
                write_exception_missing(demandArrivalsAliasHeader_dc['demand_qty_dist'], 'Set to default value.',
                                        qtyDist, 'DemandArrivals.txt', self.lineNumber)
        else:
            qtyDist = qty_dist

        qtyCv = float(qty_cv)
        if missingValues[1] and qtyDist.lower() not in oneParameterDistributions:
            if qty_type == 1:
                qtyCv = float(demandArrivalsDefaults['order_qty_cv'])
                write_exception_missing(demandArrivalsAliasHeader_dc['order_qty_cv'], 'Set to default value.', qtyCv,
                                        'DemandArrivals.txt', self.lineNumber)
            else:
                qtyCv = float(demandArrivalsDefaults['demand_qty_cv'])
                write_exception_missing(demandArrivalsAliasHeader_dc['demand_qty_cv'], 'Set to default value.',
                                        qtyCv, 'DemandArrivals.txt', self.lineNumber)

        qty = sample_from_distribution(qtyDist, qtyAverage, qtyCv)

        try:
            if math.isnan(qty):
                if qty_type == 1:
                    write_exception_invalid(qty_dist, demandArrivalsAliasHeader_dc['order_qty_dist'],
                                            'Unsupported distribution changed to:',
                                            demandArrivalsDefaults['order_qty_dist'], 'DemandArrivals.txt',
                                            self.lineNumber)
                    qtyDist = demandArrivalsDefaults['order_qty_dist']
                    if qtyDist.lower() not in oneParameterDistributions:
                        try:
                            if math.isnan(float(qtyCv)):
                                qtyCv = float(demandArrivalsDefaults['order_qty_cv'])
                                write_exception_missing(demandArrivalsAliasHeader_dc['order_qty_cv'],
                                                        'Set to default value.', qtyCv, 'DemandArrivals.txt',
                                                        self.lineNumber)
                        except ValueError:
                            pass

                else:
                    write_exception_invalid(qty_dist, demandArrivalsAliasHeader_dc['demand_qty_dist'],
                                            'Unsupported distribution changed to:',
                                            demandArrivalsDefaults['demand_qty_dist'], 'DemandArrivals.txt',
                                            line_number)
                    qtyDist = demandArrivalsDefaults['demand_qty_dist']
                    if qtyDist.lower() not in oneParameterDistributions:
                        try:
                            if math.isnan(float(qtyCv)):
                                qtyCv = float(demandArrivalsDefaults['demand_qty_cv'])
                                write_exception_missing(demandArrivalsAliasHeader_dc['demand_qty_cv'],
                                                        'Set to default value.', qtyCv, 'DemandArrivals.txt',
                                                        self.lineNumber)
                        except ValueError:
                            pass

                qty = sample_from_distribution(qtyDist, qtyAverage, qtyCv)
        except ValueError:
            pass

        return [int(round(qty)), qtyDist, qtyAverage, qtyCv]


class DemandSeasonal:

    def __init__(self, env_, data_series):
        global demandSeasonalGenerated

        self.env = env_
        self.productName = data_series['product_name']
        self.customerName = data_series['customer_name']
        self.sourceSite = data_series['source_site']
        try:
            if math.isnan(float(self.customerName)):
                pass
        except ValueError:
            if self.customerName in customersExclude_ls:
                demandSeasonalGenerated -= 1
                simulation_terminate(env_.now)
                return

        try:
            if math.isnan(float(self.sourceSite)):
                pass
        except ValueError:
            if self.sourceSite in sitesExclude_ls:
                demandSeasonalGenerated -= 1
                simulation_terminate(env_.now)
                return

        self.demandQtyAvg = data_series['demand_qty_avg']
        self.demandQtyCV = data_series['demand_qty_cv']
        self.demandQtyDist = data_series['demand_qty_dist']
        self.demandQtyPeriod = data_series['demand_qty_period']
        self.demandSeasonalityIndex = data_series['demand_seasonality_index']
        self.demandSeasonalityIndexPeriod = data_series['demand_seasonality_index_period']
        self.demandGrowth = data_series['demand_growth']
        self.demandGrowthPeriod = data_series['demand_growth_period']
        self.orderQtyAvg = data_series['order_qty_avg']
        self.orderQtyCV = data_series['order_qty_cv']
        self.orderQtyDist = data_series['order_qty_dist']
        self.orderQtyPeriod = data_series['order_qty_period']
        self.numberOfOrders = data_series['number_of_orders']
        self.deliveryDueDate = float(data_series['delivery_due_date'])
        self.shippingMethod = data_series['shipping_method']
        self.firstOrderNumber = data_series['first_order_number']
        self.forceSumDemand = data_series['force_sum_demand']
        self.randomizeEachOrder = data_series['random_each_order']
        self.lineNumber = data_series['line_number']
        self.addOne = 0
        self.ReportError = True

        env_.process(self.generate_demand_seasonal())

    def generate_demand_seasonal(self):

        global orderNumberDefault, orderItemsRemain, demandSeasonalGenerated
        customerName = ''
        siteName = ''
        orderNumber = 'NaN'

        try:
            if math.isnan(float(self.numberOfOrders)):
                if runLength == 0:
                    self.numberOfOrders = float(demandSeasonalDefaults['number_of_orders'])
            else:
                self.numberOfOrders = float(self.numberOfOrders)
        except ValueError:
            self.numberOfOrders = float(self.numberOfOrders)

        if math.isnan(self.numberOfOrders) and runLength == 0:
            print('\n\nDATA ERROR: Must specify either a simulation run end date or NumberOfOrders on '
                  'line {} in DemandSeasonal.txt\n'.format(self.lineNumber))
            terminate_simulation(True)

        if self.numberOfOrders == 0:
            demandSeasonalGenerated -= 1
            simulation_terminate(self.env.now)
            return

        productName = demand_get_product(self.productName, self.lineNumber)

        try:
            if math.isnan(float(self.randomizeEachOrder)):
                self.randomizeEachOrder = demandSeasonalDefaults['random_each_order']
            else:
                self.randomizeEachOrder = to_bool(self.randomizeEachOrder)
        except ValueError:
            self.randomizeEachOrder = to_bool(self.randomizeEachOrder)

        if not self.randomizeEachOrder:
            customerName = demand_get_customer(self.customerName)
            siteName = demand_get_site(self.sourceSite, customerName, productName)

        try:
            if not math.isnan(float(self.firstOrderNumber)):
                orderNumber = float(self.firstOrderNumber)
        except ValueError:
            pass

        try:
            if math.isnan(self.deliveryDueDate):
                self.deliveryDueDate = float(demandSeasonalDefaults['delivery_due_date'])
            else:
                self.deliveryDueDate = float(self.deliveryDueDate)
        except ValueError:
            self.deliveryDueDate = float(demandSeasonalDefaults['delivery_due_date'])

        try:
            if math.isnan(float(self.demandGrowth)):
                self.demandGrowth = float(demandSeasonalDefaults['demand_growth'])
            else:
                self.demandGrowth = float(self.demandGrowth)
        except ValueError:
            self.demandGrowth = float(self.demandGrowth)

        self.demandGrowth = 1 + self.demandGrowth * 0.01

        try:
            if math.isnan(float(self.demandGrowthPeriod)):
                self.demandGrowthPeriod = demandSeasonalDefaults['demand_growth_period'].lower()

        except ValueError:
            self.demandGrowthPeriod = self.demandGrowthPeriod.lower()

        try:
            if math.isnan(float(self.demandSeasonalityIndexPeriod)):
                self.demandSeasonalityIndexPeriod = demandSeasonalDefaults['demand_seasonality_index_period'].lower()

        except ValueError:
            self.demandSeasonalityIndexPeriod = self.demandSeasonalityIndexPeriod.lower()

        series = self.demandSeasonalityIndex
        try:
            if math.isnan(float(series)):
                series = demandSeasonalDefaults['demand_seasonality_index']
        except ValueError:
            pass
        series = series.replace('[', '').replace(']', '')
        self.demandSeasonalityIndex = [float(i) for i in series.split(',')]

        orderPeriod = ''
        valueList = self.quantity_stochastic(self.orderQtyAvg, self.orderQtyCV, self.orderQtyDist, 1)
        qty = valueList[0]
        addDay = 0
        try:
            if math.isnan(float(qty)):
                qty = 0
                try:
                    if math.isnan(float(self.demandQtyPeriod)):
                        orderPeriod = demandSeasonalDefaults['demand_qty_period'].lower()
                except ValueError:
                    orderPeriod = self.demandQtyPeriod.lower()

            else:
                self.orderQtyDist = valueList[1]
                self.orderQtyAvg = valueList[2]
                self.orderQtyCV = valueList[3]
                while qty <= 0:
                    qty = self.sample_order_qty()

                try:
                    if math.isnan(float(self.orderQtyPeriod)):
                        orderPeriod = demandSeasonalDefaults['order_qty_period'].lower()
                except ValueError:
                    orderPeriod = self.orderQtyPeriod.lower()

        except ValueError:
            pass

        arrivals = []
        totalTime = 0

        arrivals.append(datetimeStart)

        if orderPeriod == 'month' or orderPeriod == 'quarter' or orderPeriod == 'year':

            if orderPeriod == 'month':
                addMonth = 1
            elif orderPeriod == 'quarter':
                addMonth = 3
            else:
                addMonth = 12

            theDay = runBeginDate.day
            theMonth = runBeginDate.month
            theYear = runBeginDate.year

            while True:

                if not math.isnan(float(self.numberOfOrders)) and len(arrivals) == self.numberOfOrders:
                    break

                theMonth += addMonth
                if theMonth > 12:
                    theMonth -= 1
                    theYear += theMonth // 12
                    theMonth = (theMonth % 12) + 1

                nextDate = dt.date(theYear, theMonth, min(theDay, cl.monthrange(theYear, theMonth)[1]))
                timeDelta = get_time_interval(arrivals[-1], nextDate)
                totalTime += timeDelta

                if runLength > 0 and (totalTime >= runLength):
                    break

                arrivals.append(nextDate)
        else:
            if orderPeriod == 'day' or orderPeriod == 'hour' or orderPeriod == 'minute' or orderPeriod == 'second':
                addDay = 1

            elif orderPeriod == 'week':
                addDay = 7

            elif orderPeriod == 'biweekly':
                addDay = 14

            while True:

                if not math.isnan(float(self.numberOfOrders)) and len(arrivals) == self.numberOfOrders:
                    break

                totalTime += addDay

                if runLength > 0 and (totalTime >= runLength):
                    break

                arrivals.append(dt.date(arrivals[-1].year, arrivals[-1].month, arrivals[-1].day) +
                                get_time_to_add(addDay))

        orders_dict = []

        if qty > 0:
            orders_dict.append([get_time_interval(datetimeStart, arrivals[0]),
                                int(qty * self.get_seasonality_index(arrivals[0]) *
                                    self.get_growth_factor(arrivals[0]))])
            for i in range(len(arrivals) - 1):
                orders_dict.append([get_time_interval(arrivals[i], arrivals[i + 1]),
                                    int(self.sample_order_qty() * self.get_seasonality_index(arrivals[i + 1]) *
                                        self.get_growth_factor(arrivals[i + 1]))])
        else:
            try:
                if math.isnan(float(self.forceSumDemand)):
                    forceSum = to_bool(demandArrivalsDefaults['force_sum_demand'])
                else:
                    forceSum = to_bool(self.forceSumDemand)

            except ValueError:
                forceSum = to_bool(self.forceSumDemand)

            qty = self.quantity_stochastic(self.demandQtyAvg, self.demandQtyCV, self.demandQtyDist, 2)[0]

            if qty == 0:
                orders_dict.append([get_time_interval(datetimeStart, arrivals[0]),
                                    int(self.get_seasonality_index(arrivals[0]) * self.get_growth_factor(arrivals[0]))])
            else:

                while qty < 0:
                    write_exception_amend('Negative value', 'Order quantity sampled:', 'Order discarded:', qty,
                                          'DemandSeasonal.txt', self.lineNumber)

                    if self.ReportError:
                        print(
                            '-- Day {:.0f} ({}): Negative order quantity sampled in DemandSeasonal.txt line {:.0f}. '
                            'Resampling.'.format(self.env.now + 1,
                                                 format_time(runBeginDate + get_time_to_add(self.env.now)),
                                                 self.lineNumber))

                    qty = self.quantity_stochastic(self.demandQtyAvg, self.demandQtyCV, self.demandQtyDist, 2)[0]

                seasonalityIndex = {}
                growthDemand = {}
                indexTotal = 0
                for date in arrivals:
                    index = self.get_seasonality_index(date)
                    indexTotal += index
                    seasonalityIndex[date] = self.get_seasonality_index(date)
                    growthDemand[date] = self.get_growth_factor(date)

                orderResidual = int(qty)
                orderDemand = {}

                for date in arrivals:
                    orderSize = int(qty * seasonalityIndex[date] / indexTotal)
                    if orderResidual > orderSize:
                        orderResidual -= orderSize
                    else:
                        orderSize = orderResidual
                        orderResidual = 0

                    orderDemand[date] = orderSize
                    if orderResidual == 0:
                        break

                if forceSum:
                    while orderResidual > 0:
                        key = list(orderDemand.keys())[0]
                        qty = orderDemand.pop(key)
                        orderDemand[key] = qty + 1
                        orderResidual -= 1

                orders_dict.append([get_time_interval(datetimeStart, arrivals[0]),
                                    int(orderDemand[arrivals[0]] * self.get_growth_factor(arrivals[0]))])

                for i in range(len(orderDemand) - 1):
                    orders_dict.append([get_time_interval(arrivals[i], arrivals[i + 1]),
                                        int(orderDemand[arrivals[i + 1]] * self.get_growth_factor(arrivals[i + 1]))])

        orderItemsRemain += len(orders_dict)

        for item in orders_dict:
            yield schedule_event(item[0], EVENT_PRIORITY_ORDERS)

            qty = item[1]
            if qty == 0:
                if np.random.random_sample() < 0.5:
                    write_exception_amend('0', 'Order quantity sampled:', 'Order discarded:', qty, 'DemandSeasonal.txt',
                                          self.lineNumber)
                    continue
                qty = 1
                write_exception_amend('0', 'Order quantity sampled:', 'Order quantity reset:', qty,
                                      'DemandSeasonal.txt', self.lineNumber)

            if self.randomizeEachOrder:
                customerName = demand_get_customer(self.customerName)
                siteName = demand_get_site(self.sourceSite, customerName, productName)

            try:
                if math.isnan(float(orderNumber)):
                    orderNum = orderNumberDefault
                    orderNumberDefault += 1
                else:
                    orderNum = orderNumber
                    orderNumber += 1
            except ValueError:
                orderNum = orderNumberDefault
                orderNumberDefault += 1

            create_order(self.env, 'TY3-{}-{:.0f}'.format(self.lineNumber, orderNum), '1', productName, qty,
                         customerName, format_time(runBeginDate + get_time_to_add(self.env.now)), siteName,
                         format_time(runBeginDate + get_time_to_add(self.env.now + self.deliveryDueDate)), 0)

        demandSeasonalGenerated -= 1
        simulation_terminate(self.env.now)
        pass

    def sample_order_qty(self):

        while True:
            qty = int(round(sample_from_distribution(self.orderQtyDist, self.orderQtyAvg,
                                                     self.orderQtyCV))) + self.addOne

            if qty < 0:
                write_exception_amend('Negative value', 'Order quantity sampled:', 'Order discarded:', qty,
                                      'DemandSeasonal.txt', self.lineNumber)
                if self.ReportError:
                    print(
                        '-- Day {:.0f} ({}): Negative order quantity sampled in DemandSeasonal.txt line {:.0f}. '
                        'Resampling.'.format(self.env.now + 1,
                                             format_time(runBeginDate + get_time_to_add(self.env.now)),
                                             self.lineNumber))
                continue

            if qty == 0:
                self.orderQtyAvg -= 0.5
                self.addOne = 0.5
                qty = 1

            return qty

    def quantity_stochastic(self, qty_avg, qty_cv, qty_dist, qty_type):
        missingValues = []
        stochasticQty = 0
        try:
            if math.isnan(float(qty_avg)):
                missingValues.append(True)
                stochasticQty += 1
            else:
                missingValues.append(False)
        except ValueError:
            missingValues.append(False)
        try:
            if math.isnan(float(qty_cv)):
                missingValues.append(True)
                stochasticQty += 1
            else:
                missingValues.append(False)
        except ValueError:
            missingValues.append(False)
        try:
            if math.isnan(float(qty_dist)):
                missingValues.append(True)
                stochasticQty += 1
            else:
                missingValues.append(False)
        except ValueError:
            missingValues.append(False)

        if stochasticQty == 3 and qty_type == 1:
            return [float('NaN')]

        if missingValues[0]:
            if qty_type == 1:
                qtyAverage = float(demandSeasonalDefaults['order_qty_avg'])
                write_exception_missing(demandSeasonalAliasHeader_dc['order_qty_avg'], 'Set to default value.',
                                        qtyAverage, 'DemandSeasonal.txt', self.lineNumber)
            else:
                qtyAverage = float(demandSeasonalDefaults['demand_qty_avg'])
                write_exception_missing(demandSeasonalAliasHeader_dc['demand_qty_avg'], 'Set to default value.',
                                        qtyAverage, 'DemandSeasonal.txt', self.lineNumber)
        else:
            qtyAverage = float(qty_avg)

        if missingValues[2]:
            if qty_type == 1:
                qtyDist = demandSeasonalDefaults['order_qty_dist']
                write_exception_missing(demandSeasonalAliasHeader_dc['order_qty_dist'], 'Set to default value.',
                                        qtyDist, 'DemandSeasonal.txt', self.lineNumber)
            else:
                qtyDist = demandSeasonalDefaults['demand_qty_dist']
                write_exception_missing(demandSeasonalAliasHeader_dc['demand_qty_dist'], 'Set to default value.',
                                        qtyDist, 'DemandSeasonal.txt', self.lineNumber)
        else:
            qtyDist = qty_dist

        qtyCv = float(qty_cv)
        if missingValues[1] and qtyDist.lower() not in oneParameterDistributions:
            if qty_type == 1:
                qtyCv = float(demandSeasonalDefaults['order_qty_cv'])
                write_exception_missing(demandSeasonalAliasHeader_dc['order_qty_cv'], 'Set to default value.', qtyCv,
                                        'DemandSeasonal.txt', self.lineNumber)
            else:
                qtyCv = float(demandSeasonalDefaults['demand_qty_cv'])
                write_exception_missing(demandSeasonalAliasHeader_dc['demand_qty_cv'], 'Set to default value.',
                                        qtyCv, 'DemandSeasonal.txt', self.lineNumber)

        qty = sample_from_distribution(qtyDist, qtyAverage, qtyCv)

        try:
            if math.isnan(qty):
                if qty_type == 1:
                    write_exception_invalid(qty_dist, demandSeasonalAliasHeader_dc['order_qty_dist'],
                                            'Unsupported distribution changed to:',
                                            demandSeasonalDefaults['order_qty_dist'], 'DemandSeasonal.txt',
                                            self.lineNumber)
                    qtyDist = demandSeasonalDefaults['order_qty_dist']
                    if qtyDist.lower() not in oneParameterDistributions:
                        try:
                            if math.isnan(float(qtyCv)):
                                qtyCv = float(demandSeasonalDefaults['order_qty_cv'])
                                write_exception_missing(demandSeasonalAliasHeader_dc['order_qty_cv'],
                                                        'Set to default value.', qtyCv, 'DemandSeasonal.txt',
                                                        self.lineNumber)
                        except ValueError:
                            pass

                else:
                    write_exception_invalid(qty_dist, demandSeasonalAliasHeader_dc['demand_qty_dist'],
                                            'Unsupported distribution changed to:',
                                            demandSeasonalDefaults['demand_qty_dist'], 'DemandSeasonal.txt',
                                            self.lineNumber)
                    qtyDist = demandSeasonalDefaults['demand_qty_dist']
                    if qtyDist.lower() not in oneParameterDistributions:
                        try:
                            if math.isnan(float(qtyCv)):
                                qtyCv = float(demandSeasonalDefaults['demand_qty_cv'])
                                write_exception_missing(demandSeasonalAliasHeader_dc['demand_qty_cv'],
                                                        'Set to default value.', qtyCv, 'DemandSeasonal.txt',
                                                        self.lineNumber)
                        except ValueError:
                            pass

                qty = sample_from_distribution(qtyDist, qtyAverage, qtyCv)
        except ValueError:
            pass

        return [int(round(qty)), qtyDist, qtyAverage, qtyCv]

    def get_seasonality_index(self, datetime):
        if self.demandSeasonalityIndexPeriod == 'month':
            if datetime.month <= len(self.demandSeasonalityIndex):
                return self.demandSeasonalityIndex[datetime.month - 1]

            return self.demandSeasonalityIndex[-1]

        if self.demandSeasonalityIndexPeriod == 'week':
            delta = (datetime.day - 1) // 7 + 1

        elif self.demandSeasonalityIndexPeriod == 'day':
            delta = datetime.weekday()

        elif self.demandSeasonalityIndexPeriod == 'biweekly':
            delta = (datetime.day - 1) // 14 + 1

        elif self.demandSeasonalityIndexPeriod == 'quarter':
            delta = (datetime.month - 1) // 3

        elif self.demandSeasonalityIndexPeriod == 'year':
            delta = datetime.year - datetimeStart.year

        else:
            return 1

        if delta <= len(self.demandSeasonalityIndex):
            return self.demandSeasonalityIndex[delta]

        return self.demandSeasonalityIndex[-1]

    def get_growth_factor(self, datetime):
        if self.demandGrowthPeriod == 'year':
            return self.demandGrowth ** ((datetime - datetimeStart).days // 365.25 + 1)

        if self.demandGrowthPeriod == 'month':
            return self.demandGrowth ** (delta_month(datetimeStart, datetime) + 1)

        if self.demandGrowthPeriod == 'week':
            return self.demandGrowth ** ((datetime - datetimeStart).days // 7 + 1)

        if self.demandGrowthPeriod == 'day':
            return self.demandGrowth ** ((datetime - datetimeStart).days + 1)

        if self.demandGrowthPeriod == 'quarter':
            return self.demandGrowth ** (delta_month(datetimeStart, datetime) // 3 + 1)

        if self.demandGrowthPeriod == 'biweekly':
            return self.demandGrowth ** ((datetime - datetimeStart).days // 14 + 1)

        return 1


class Report:

    def __init__(self, levels):
        global reportMenus_df

        self.list = []
        for item in levels:
            try:
                if math.isnan(float(item)):
                    continue

            except ValueError:
                pass

            i = 1
            while True:
                alias = ''
                try:
                    alias = reportMenus_df.loc[item, i]
                    if math.isnan(float(alias)):
                        break

                except ValueError:
                    self.list.append(alias)
                    i += 1
                except KeyError:
                    break

            if len(self.list) > 0:
                break


global dateRunBegin, dateStart, dateEnd, runBeginDate, warmedDate, endDate, warmLength
global reviewAtReset, snapInterval, plotCreate, logEvents, consoleEvents
global scenario, replicationId, defaultSourcingSite, inputFilesList
global masterDataDynamic, loadOrdersMemory, loadPeriodsMemory, loadDemandMemory
global policyLists_df, dataDictionary_df, ordersDaily_df, sitesConsider_df
global inventoryPoliciesProducts_df
global customers_df, customersExclude_df, customersConsider_df, inventoryPolicyPeriods_df
global sourcingPoliciesAliases_ls, inventoryPoliciesSites_df, inventoryPoliciesAliases_ls
global demand_df, demandArrivals_df, periodDates, reportMenus_df
global fileOrder, fileShip, fileInv, fileCoq
global fileBoq, fileExceptions, fileLog, stripFileName
global globalReportingLevel, reportLevelDefaults, transportDefaults
global anySite_dc, anyProduct_dc, byGroups_dc, siteProduct_dc, orderQtyDefault
global inventoryPolicyDefaults, inventoryPolicyHeaderAlias_dc, demandPeriodDefault, orderNumberDefault
global inventoryPoliciesAliasHeader_dc, ordersAliasHeader_dc, transportsAliasHeader_dc, transportLanesAliasHeader_dc
global inventoryPoliciesPeriodAliasHeader_dc, headerDictionary_df, headerIndexed_df, alias_dc, header_dc, hdrFileName
global demandAliasHeader_dc, demandArrivalsAliasHeader_dc, monthInPeriod, randomSeed
global demandDefaults, demandArrivalsDefaults, numOrderTxt, numDemand, fulfillOrders
global demandSeasonal_df, demandSeasonalDefaults, datetimeStart, demandSeasonalAliasHeader_dc
global sourcingPoliciesAliasHeader_dc, customerSourcingPoliciesAliasHeader_dc, sourcingPolicyDefaults
global customerSourcingCustomer_ls, customerSourcingProduct_ls, groupSites_df, groupProducts_df
global customerSourcingCustomer_df, customerSourcingProduct_df
global customerSourcingPolicyDefaults, sourcingPoliciesSites_df, sourcingPoliciesProducts_df

keepOrdersMemory = False
keepShipmentsMemory = False
keepTimeSeriesMemory = False
runAborted = False
productGroups_ls = []
productGroups_df = {}
products_df = {}
sites_df = {}
sites_ls = []

siteGroups_ls = []
siteGroups_df = {}
sitesExclude_ls = []
sitesExclude_df = {}

customerGroups_df = {}
customers_ls = []
customersExclude_ls = []
customerSourcingPolicies2_df = {}

transports_df = {}
transportLanesDest_df = {}
transportLanesSites_df = {}
transportLanesOrigin_df = {}
transportLanesOriginUnique_df = {}
transportLanesDestinationUnique_df = {}

customerSourcingPolicies_df = {}
sourcingPolicies_df = {}
inventoryPolicies_df = {}
periods_df = {}

demandGenerated = 1
orderGenerated = False
demandArrivalGenerated = 1
demandSeasonalGenerated = 1

headerPresent_dc = {}
updateCompleted_ls = []
totalOrdersShipped = 0
totalOrdersShipped_ = 0
totalOrdersReceived = 0
totalOrdersReceived_ = 0
totalOrdersGenerated = 0
totalOrdersGenerated_ = 0
orderItemsRemain = 0
runLength = 0
pathInputs = Path("Inputs")
pathOutputs = Path("Outputs")
pathConfig = Path("Config")
ordersMemory = []
shipmentsMemory_ls = []
timeSeriesMemoryInv_ls = []
timeSeriesMemoryCoq_ls = []
timeSeriesMemoryBoq_ls = []
orderDates = []


def schedule_event(delay_, priority_):
    event_ = env.event()

    event_._ok = True
    env.schedule(event_, priority=priority_, delay=delay_)

    return event_


def run_simulation(run_time):
    yield schedule_event(run_time, EVENT_PRIORITY_END_RUN)
    endSimulation.succeed()


def order_item_removed(time):
    global orderItemsRemain

    orderItemsRemain -= 1
    simulation_terminate(time)


def run_warm_up(env_):
    global warmUpComplete
    global warmUpTime

    yield schedule_event(warmLength, EVENT_PRIORITY_WARM_UP)

    warmUpTime = ((dt.datetime.now() - timeAtStart).total_seconds())
    date = format_time((runBeginDate + get_time_to_add(env_.now)))

    if warmLength > 0:
        print('-- Day {:.0f} ({}): Complete warm up in {:.3f} seconds'.
              format(env.now + 1, date, warmUpTime))

        if logEvents:
            fileLog.write('Day {:.0f} ({}): Complete warm up in {:.3f} seconds\n'.
                          format(env_.now + 1, date, warmUpTime))
    warmUpComplete = True

    if runLength == 0:
        simulation_terminate(env_.now)


def order_generate(env_):
    global orderDates, orderGenerated, orderItemsRemain, orderNumberDefault

    orderCreated = 0

    if loadOrdersMemory:
        if len(orderDates) == 0:
            orderDates = sorted([key for key, _ in ordersDaily_df], key=get_time_from_begin)

        for day in orderDates:
            elapsedTime = get_time_from_begin(day)

            if elapsedTime < env_.now:
                todayOrders_df = ordersDaily_df.get_group(day)
                numOrder = len(todayOrders_df.index)
                orderCreated += numOrder
                orderItemsRemain -= numOrder
                for index, row in todayOrders_df.iterrows():
                    write_exception_invalid(row['order_date'], ordersAliasHeader_dc['order_date'], 'Order is ignored.',
                                            '', 'Orders.txt', row['line_number'])
                continue

            # yield env_.timeout(elapsedTime - env_.now)
            yield schedule_event(elapsedTime - env_.now, EVENT_PRIORITY_ORDERS)
            todayOrders_df = ordersDaily_df.get_group(day)

            for index, row in todayOrders_df.iterrows():
                orderCreated += 1
                customerName = demand_get_customer(row['customer_name'])
                try:
                    if math.isnan(float(customerName)):
                        orderItemsRemain -= 1
                        simulation_terminate(env_.now)
                        continue
                except ValueError:
                    pass

                siteName = demand_get_site(row['source_site'], customerName, row['product_name'])
                try:
                    if math.isnan(float(siteName)):
                        orderItemsRemain -= 1
                        simulation_terminate(env_.now)
                        continue
                except ValueError:
                    pass

                try:
                    if math.isnan(float(row['order_number'])):
                        orderNumber = 'ORD-{}-{:.0f}'.format(row['line_number'], orderNumberDefault)
                        orderNumberDefault += 1
                    else:
                        orderNumber = row['order_number']
                except ValueError:
                    orderNumber = row['order_number']

                try:
                    if math.isnan(float(row['order_line_number'])):
                        orderLineNumber = ''
                    else:
                        orderLineNumber = row['order_line_number']
                except ValueError:
                    orderLineNumber = row['order_line_number']

                create_order(env_, orderNumber, orderLineNumber, row['product_name'], row['quantity'],
                             customerName, day, siteName, row['delivery_due_date'],
                             row['line_number'])

            if orderCreated == numOrderTxt:
                break

        orderGenerated = True
        simulation_terminate(env_.now)
        return

    inputFileName = get_os_file_name('Orders.txt', False)
    with open(inputFileName) as fp:
        line = fp.readline()
        lineNumber = 1
        if line:
            aliasColumn = read_header('Orders', [strip_string(x) for x in line.strip().split("\t")])
            if headerPresent_dc['Orders']:
                # read next line after header
                line = fp.readline()
                lineNumber += 1

            while line:
                row = line.strip().split("\t")

                if len(row) > 0 and row[0] != '':
                    elapsedTime = get_time_from_begin(row[aliasColumn['order_date'] - 1])

                    if elapsedTime < env_.now:
                        write_exception_invalid(row[aliasColumn['order_date'] - 1], ordersAliasHeader_dc['order_date'],
                                                'Order is ignored.', '', 'Orders.txt', lineNumber)
                    else:
                        # yield env_.timeout(elapsedTime - env_.now)
                        yield schedule_event(elapsedTime - env_.now, EVENT_PRIORITY_ORDERS)

                        orderValues = {}

                        for alias in aliasColumn.keys():
                            if aliasColumn[alias] <= len(row):
                                orderValues[alias] = row[aliasColumn[alias] - 1]
                            else:
                                orderValues[alias] = 'NaN'

                        customerName = demand_get_customer(orderValues['customer_name'])
                        try:
                            if math.isnan(float(customerName)):
                                continue
                        except ValueError:
                            pass

                        siteName = demand_get_site(orderValues['source_site'], customerName,
                                                   orderValues['product_name'])
                        try:
                            if math.isnan(float(siteName)):
                                continue
                        except ValueError:
                            pass

                        try:
                            if math.isnan(float(orderValues['order_number'])):
                                orderNumber = 'ORD-{}-{:.0f}'.format(lineNumber, orderNumberDefault)
                                orderNumberDefault += 1
                            else:
                                orderNumber = orderValues['order_number']
                        except ValueError:
                            orderNumber = orderValues['order_number']

                        try:
                            if math.isnan(float(orderValues['order_line_number'])):
                                orderLineNumber = ''
                            else:
                                orderLineNumber = orderValues['order_line_number']
                        except ValueError:
                            orderLineNumber = orderValues['order_line_number']

                        orderItemsRemain += 1
                        create_order(env_, orderNumber, orderLineNumber,
                                     orderValues['product_name'], orderValues['quantity'], customerName,
                                     orderValues['order_date'], siteName,
                                     orderValues['delivery_due_date'], lineNumber)

                line = fp.readline()
                lineNumber += 1

    fp.close()
    orderGenerated = True
    simulation_terminate(env_.now)


def create_order(env_, order_number, order_line_number, product_name, qty, customer, order_date, site_name, due_date,
                 line_number):
    # check if site defined in master
    global orderItemsRemain

    if len(sites_df) == 0 or site_name not in sites_df.index:
        try:
            if math.isnan(float(site_name)):
                write_exception_missing(ordersAliasHeader_dc['source_site'], 'Order is ignored.', '',
                                        'Orders.txt', line_number)
                return

        except ValueError:
            pass

        if masterDataDynamic:
            # if site_name in sites_dc.keys():
            if not network.site_exists(site_name):
                write_exception_undefined(site_name, ordersAliasHeader_dc['source_site'], 'New site is defined.',
                                          site_name, 'Orders.txt', line_number)

        else:
            write_exception_undefined(site_name, ordersAliasHeader_dc['source_site'], 'Order is ignored.', '',
                                      'Orders.txt', line_number)
            return

    # check if product is defined in master
    if len(products_df) == 0 or product_name not in products_df.index:
        try:
            if math.isnan(float(product_name)):
                write_exception_missing(ordersAliasHeader_dc['product_name'], 'Order is ignored.', '',
                                        'Orders.txt', line_number)
                return

        except ValueError:
            pass

        if masterDataDynamic:
            if product_name not in products_dc.keys():
                write_exception_undefined(product_name, ordersAliasHeader_dc['product_name'], 'New product is defined.',
                                          product_name, 'Orders.txt', line_number)

        else:
            write_exception_undefined(product_name, ordersAliasHeader_dc['product_name'], 'Order is ignored.', '',
                                      'Orders.txt', line_number)
            return

    if not validate_date(due_date) or not validate_interval(order_date, due_date):
        write_exception_invalid(due_date, ordersAliasHeader_dc['delivery_due_date'], 'Set to order date.', order_date,
                                'Orders.txt', line_number)
        due_date = order_date

    try:
        qty = float(qty)
        if qty <= 0:
            write_exception_invalid(qty, ordersAliasHeader_dc['Quantity'], 'Set to positive value:', -qty,
                                    'Orders.txt', line_number)
            qty = -qty

    except ValueError:
        write_exception_invalid(qty, ordersAliasHeader_dc['Quantity'], 'Set to default value:', orderQtyDefault,
                                'Orders.txt', line_number)
        qty = orderQtyDefault

    except TypeError:
        write_exception_invalid(qty, ordersAliasHeader_dc['Quantity'], 'Set to default value:', orderQtyDefault,
                                'Orders.txt', line_number)
        qty = orderQtyDefault

    inventoryPolicy = get_inventory_policy(site_name, product_name)

    create_order_item(env_, Order.TYPE_CUSTOMER, order_number, order_line_number, customer, inventoryPolicy, qty,
                      order_date, due_date, get_time_from_begin(order_date) - env_.now)

    global totalOrdersGenerated, totalOrdersGenerated_
    if warmUpComplete:
        totalOrdersGenerated_ += 1

    totalOrdersGenerated += 1

    check_updates_completed()


def create_order_item(env_, order_type, order_number, order_line_number, customer, inventory_policy, qty,
                      order_date, due_date, delay_time):
    if order_number in orders_dc.keys():
        theOrder = orders_dc[order_number]
    else:
        # create order
        theOrder = Order(order_type, order_number, customer, order_date, due_date)
        orders_dc[order_number] = theOrder

    OrderItem(env_, theOrder, order_line_number, inventory_policy, qty, delay_time, order_date, due_date)


def demand_generate(env_):
    global monthInPeriod, demandGenerated, numDemand
    periodLength = {}
    dateDemand = []
    lastDate = ''
    firstDate = ''
    demandCreated = 0
    demandGenerated = 0
    monthInPeriod = {}

    for day in periodDates:
        if dateEnd != '' and get_interval_time(day, dateEnd) < 0:
            monthInPeriod[dateDemand[-1]] = month_delta(dateDemand[-1], day)
            break

        dateDemand.append(day)
        time = get_time_from_begin(day)

        if time <= 0:
            if len(dateDemand) > 1:
                dateDemand.remove(dateDemand[0])
            firstDate = day

    if len(dateDemand) > 1:
        periodLength[dateDemand[0]] = get_interval_time(dateRunBegin, dateDemand[1])
        monthInPeriod[dateDemand[0]] = month_delta(dateDemand[0], dateDemand[1])

        if dateEnd != '':
            periodLength[dateDemand[-1]] = get_interval_time(dateDemand[-1], dateEnd) + 1
        else:
            theDate = get_strp_time(dateDemand[-1])
            periodLength[dateDemand[-1]] = cl.monthrange(theDate.year, theDate.month)[1]
    else:
        if dateEnd != '':
            periodLength[dateDemand[0]] = get_interval_time(dateRunBegin, dateEnd) + 1
            monthInPeriod[dateDemand[0]] = month_delta(dateDemand[0], dateEnd)
        else:
            theDate = get_strp_time(dateDemand[0])
            periodLength[dateDemand[0]] = cl.monthrange(theDate.year, theDate.month)[1]
            monthInPeriod[dateDemand[0]] = 1

    numMonth = 0
    for day in dateDemand:
        if day == firstDate:
            continue
        if lastDate == '':
            lastDate = day
            continue
        periodLength[lastDate] = get_interval_time(lastDate, day)
        numMonth = month_delta(lastDate, day)
        monthInPeriod[lastDate] = numMonth
        lastDate = day

    if lastDate not in monthInPeriod.keys():
        monthInPeriod[lastDate] = numMonth

    if loadDemandMemory:
        if numDemand > 0:
            for day in periodDates:
                if day not in dateDemand:
                    try:
                        periodSettings_df = demand_df.get_group(periods_df.loc[day, 'period_number'])
                        numDemand -= len(periodSettings_df.index)

                    except KeyError:
                        continue

                    except AttributeError:
                        continue

            for day in dateDemand:

                time = get_time_from_begin(day) - env_.now
                demandGenerated += 1
                yield env_.timeout(max(0, time))
                demandGenerated -= 1

                # find all records for current period
                if len(periods_df) > 0:
                    try:
                        periodSettings_df = demand_df.get_group(periods_df.loc[day, 'period_number'])
                    except KeyError:
                        continue

                    for i, row in periodSettings_df.iterrows():
                        demandCreated += 1
                        demandGenerated += 1
                        Demand(env_, row['line_number'], day, periodLength[day], row['product_name'],
                               row['customer_name'], row['source_site'], row['demand_qty'], row['demand_period'],
                               row['order_qty_avg'], row['order_qty_cv'], row['order_qty_dist'],
                               row['delivery_due_date'], row['first_order_number'], row['force_sum_demand'],
                               row['random_each_order'])

                    if demandCreated == numDemand:
                        break

                else:
                    currentPeriod = 0

                    while demandCreated < numDemand:
                        currentPeriod += 1

                        try:
                            periodSettings_df = demand_df.get_group(str(currentPeriod))
                        except KeyError:
                            continue

                        for i, row in periodSettings_df.iterrows():
                            demandCreated += 1
                            demandGenerated += 1
                            Demand(env_, row['line_number'], day, periodLength[day], row['product_name'],
                                   row['customer_name'], row['source_site'], row['demand_qty'], row['demand_period'],
                                   row['order_qty_avg'], row['order_qty_cv'], row['order_qty_dist'],
                                   row['delivery_due_date'], row['first_order_number'], row['force_sum_demand'],
                                   row['random_each_order'])

                    break

        simulation_terminate(env_.now)
        return

    inputFileName = get_os_file_name('Demand.txt', False)
    with open(inputFileName) as fp:
        line = fp.readline()
        lineNumber = 1
        if line:
            aliasColumn = read_header('Demand', [strip_string(x) for x in line.strip().split("\t")])
            if headerPresent_dc['Demand']:
                # read next line after header
                line = fp.readline()

                if not line:
                    fp.close()
                    simulation_terminate(env_.now)
                    return

                lineNumber += 1

        row = line.strip().split("\t")

        for day in dateDemand:

            time = get_time_from_begin(day) - env_.now
            demandGenerated += 1
            yield env_.timeout(max(0, time))
            demandGenerated -= 1
            currentPeriod = 0
            while line:
                if len(row) > 0 and row[0] != '':
                    if len(periods_df) > 0:
                        currentPeriod = periods_df.loc[day, 'period_number']
                        demandPeriod = row[aliasColumn['period_number'] - 1]

                    if len(periods_df) == 0 or demandPeriod == currentPeriod:
                        while len(row) < len(demandAliasHeader_dc):
                            row.append('NaN')

                        Demand(env_, lineNumber, day, periodLength[day], row[aliasColumn['product_name'] - 1],
                               row[aliasColumn['customer_name'] - 1], row[aliasColumn['source_site'] - 1],
                               row[aliasColumn['demand_qty'] - 1], row[aliasColumn['demand_period'] - 1],
                               row[aliasColumn['order_qty_avg'] - 1], row[aliasColumn['order_qty_cv'] - 1],
                               row[aliasColumn['order_qty_dist'] - 1], row[aliasColumn['delivery_due_date'] - 1],
                               row[aliasColumn['first_order_number'] - 1], row[aliasColumn['force_sum_demand'] - 1],
                               row[aliasColumn['random_each_order'] - 1])

                        demandGenerated += 1
                    elif demandPeriod > currentPeriod:
                        break

                    line = fp.readline()

                    if not line:
                        fp.close()
                        simulation_terminate(env_.now)
                        return

                row = line.strip().split("\t")
                lineNumber += 1

    fp.close()
    simulation_terminate(env_.now)


def demand_arrivals_generate(env_):
    global demandArrivalGenerated
    demandArrivalGenerated = 0

    yield env_.timeout(0)

    for i, row in demandArrivals_df.iterrows():
        DemandArrival(env_, row)
        demandArrivalGenerated += 1

    simulation_terminate(env_.now)


def demand_seasonal_generate(env_):
    global demandSeasonalGenerated

    demandSeasonalGenerated = 0

    yield env_.timeout(0)

    for i, row in demandSeasonal_df.iterrows():
        DemandSeasonal(env_, row)
        demandSeasonalGenerated += 1

    simulation_terminate(env_.now)


def demand_get_product(product_name, line_number):
    try:
        if math.isnan(float(product_name)):
            if len(products_df) == 0:
                write_exception_missing(demandAliasHeader_dc['product_name'], 'Set to blank.', '',
                                        'Demand.txt', line_number)
                return ''
            productName = np.random.choice(products_df.index.values.tolist())
            write_exception_missing(demandAliasHeader_dc['product_name'], 'Set to random product.', productName,
                                    'Demand.txt', line_number)
            return productName
    except ValueError:
        if not product_name:
            if len(products_df) == 0:
                write_exception_missing(demandAliasHeader_dc['product_name'], 'Set to blank.', '',
                                        'Demand.txt', line_number)
                return ''

            productName = np.random.choice(products_df.index.values.tolist())
            write_exception_missing(demandAliasHeader_dc['product_name'], 'Set to random product.', productName,
                                    'Demand.txt', line_number)
            return productName

        return product_name


def demand_get_customer(customer_name):
    customerName = customer_name
    try:
        if math.isnan(float(customer_name)):
            if len(customers_ls) > 0:
                customerName = np.random.choice(customers_ls)
            else:
                customerName = ''
    except ValueError:
        if customer_name in customersExclude_ls:
            return 'Excluded'
        elif not customer_name and len(customers_ls) > 0:
            customerName = np.random.choice(customers_ls)

    return create_customer(customerName)


def demand_get_site(site_name, customer_name, product_name):
    try:
        if math.isnan(float(site_name)):
            pass
    except ValueError:
        if not site_name:
            pass
        else:
            if site_name in sitesExclude_ls:
                return float('NaN')
            return get_customer_source(customer_name, product_name, [site_name])

    try:

        if customerSourcingPolicies_df.index.isin([(customer_name, product_name)]).any():
            siteList = customerSourcingPolicies_df.loc[[(customer_name, product_name)], 'source_site'].tolist()
            return get_customer_source(customer_name, product_name, siteList)
        customerGroups = []
        productGroups = []

        if customer_name in customerSourcingCustomer_ls:

            if product_name in products_dc:
                productGroups = set(products_dc[product_name].get_groups()).intersection(
                    np.unique(customerSourcingCustomer_df.loc[[customer_name], 'product_name'].tolist()))
            else:
                productGroups = set(get_product_group_list(product_name)).intersection(
                    np.unique(customerSourcingCustomer_df.loc[[customer_name], 'product_name'].tolist()))

            siteList = []
            for product in productGroups:
                addList = customerSourcingPolicies_df.loc[[(customer_name, product)], 'source_site'].tolist()
                siteList = siteList + addList

            if len(siteList) > 0:
                return get_customer_source(customer_name, product_name, siteList)

        if product_name in customerSourcingProduct_ls:

            if customer_name in customers_dc:
                customerGroups = set(customers_dc[customer_name].get_groups()).intersection(
                    np.unique(customerSourcingProduct_df.loc[[product_name], 'customer_name'].tolist()))
            else:
                customerGroups = set(get_customer_group_list(customer_name)).intersection(
                    np.unique(customerSourcingProduct_df.loc[[product_name], 'customer_name'].tolist()))

            siteList = []
            for customer in customerGroups:
                siteList = siteList + customerSourcingPolicies_df.loc[[(customer, product_name)],
                                                                      'source_site'].tolist()

            if len(siteList) > 0:
                return get_customer_source(customer_name, product_name, siteList)

        if len(customerGroups) == 0:
            if customer_name in customers_dc:
                customerGroups = customers_dc[customer_name].get_groups()
            else:
                customerGroups = get_customer_group_list(customer_name)

        if len(productGroups) == 0:
            if product_name in products_dc:
                productGroups = products_dc[product_name].get_groups()
            else:
                productGroups = get_product_group_list(product_name)

        siteList = []
        for customer in customerGroups:
            for product in productGroups:
                try:
                    siteList = siteList + customerSourcingPolicies_df.loc[[(customer, product)], 'source_site'].tolist()
                except KeyError:
                    pass

        if len(siteList) > 0:
            return get_customer_source(customer_name, product_name, siteList)

    except AttributeError:
        pass

    if len(sites_ls) == 0:
        return ''

    return np.random.choice(sites_ls)


def get_customer_source(customer_name, product_name, sites_list):
    if len(sites_ls) == 0:
        if len(sites_list) == 0:
            return ''
        else:
            return sites_list[0]

    sites = set(sites_ls).intersection(sites_list)

    if len(sites) == 1:
        return list(sites)[0]
    elif len(sites) > 1:
        return np.random.choice(list(sites))

    sites = set(siteGroups_ls).intersection(sites_list)

    if len(sites) == 0:
        return float('NaN')

    group = []
    if len(sites) == 1:
        group = list(sites)[0]
    elif len(sites) > 1:
        group = np.random.choice(list(sites))

    sites = groupSites_df.get_group(group)['site_name'].values.tolist()

    try:
        rule = customerSourcingPolicies2_df.loc[[(customer_name, product_name, group)], 'sourcing_rule']

    except KeyError:
        rule = customerSourcingPolicyDefaults['sourcing_rule']

    except AttributeError:
        rule = customerSourcingPolicyDefaults['sourcing_rule']

    return select_item_by_rule(sites, rule)


def select_item_by_rule(list, rule):
    if 'probability' in rule:
        return np.random.choice(list)

    if 'preference' in rule:
        return list[0]

    if 'priority' in rule:
        pass

    if 'closest' in rule:
        pass

    if 'custom' in rule:
        pass

    return list[0]


def demand_quantity_stochastic(order_qty_avg, order_qty_cv, order_qty_dist, line_number):
    missingValues = []
    stochasticDemand = 0
    try:
        if math.isnan(float(order_qty_avg)):
            missingValues.append(True)
            stochasticDemand += 1
        else:
            missingValues.append(False)
    except ValueError:
        if not order_qty_avg:
            missingValues.append(True)
            stochasticDemand += 1
        else:
            missingValues.append(False)
    try:
        if math.isnan(float(order_qty_cv)):
            missingValues.append(True)
            stochasticDemand += 1
        else:
            missingValues.append(False)
    except ValueError:
        if not order_qty_cv:
            missingValues.append(True)
            stochasticDemand += 1
        else:
            missingValues.append(False)

    try:
        if math.isnan(float(order_qty_dist)):
            missingValues.append(True)
            stochasticDemand += 1
        else:
            missingValues.append(False)
    except ValueError:
        if not order_qty_dist:
            missingValues.append(True)
            stochasticDemand += 1
        else:
            missingValues.append(False)

    if stochasticDemand == 3:
        return [False]

    if missingValues[0]:
        qtyAverage = float(demandDefaults['order_qty_average'])
        write_exception_missing(demandAliasHeader_dc['order_qty_avg'], 'Set to default value.', qtyAverage,
                                'Demand.txt', line_number)
    else:
        qtyAverage = float(order_qty_avg)

    if missingValues[2]:
        qtyDist = demandDefaults['order_qty_dist']
        write_exception_missing(demandAliasHeader_dc['order_qty_dist'], 'Set to default value.', qtyDist,
                                'Demand.txt', line_number)
    else:
        qtyDist = order_qty_dist

    qtyCv = float(order_qty_cv)
    if missingValues[1] and qtyDist.lower() not in oneParameterDistributions:
        qtyCv = float(demandDefaults['order_qty_cv'])
        write_exception_missing(demandAliasHeader_dc['order_qty_cv'], 'Set to default value.', qtyCv,
                                'Demand.txt', line_number)

    qty = sample_from_distribution(qtyDist, qtyAverage, qtyCv)

    try:
        if math.isnan(qty):
            write_exception_invalid(order_qty_dist, demandAliasHeader_dc['order_qty_dist'],
                                    'Unsupported distribution changed to:', demandDefaults['order_qty_dist'],
                                    'Demand.txt', line_number)

            qtyDist = demandDefaults['order_qty_dist']

            if qtyDist.lower() not in oneParameterDistributions:
                try:
                    if math.isnan(qtyCv):
                        qtyCv = float(demandDefaults['order_qty_cv'])
                        write_exception_missing(demandAliasHeader_dc['order_qty_cv'], 'Set to default value.', qtyCv,
                                                'Demand.txt', line_number)

                except ValueError:
                    pass

    except ValueError:
        pass

    return [True, qtyDist, qtyAverage, qtyCv]


def first_time_arrival_stochastic(time_avg, time_cv, time_dist, line_number):
    try:
        if math.isnan(float(time_avg)):
            return float('NaN')
        else:
            timeAverage = float(time_avg)
    except ValueError:
        timeAverage = float(time_avg)

    try:
        if math.isnan(float(time_dist)):
            timeDist = demandArrivalsDefaults['first_order_time_dist']
            write_exception_missing(demandArrivalsAliasHeader_dc['first_order_time_dist'],
                                    'Set to default value.', timeDist, 'DemandArrivals.txt',
                                    line_number)
        else:
            timeDist = time_dist
    except ValueError:
        timeDist = time_dist

    timeCv = float(time_cv)
    if timeDist.lower() not in oneParameterDistributions:
        try:
            if math.isnan(timeCv):
                timeCv = float(demandArrivalsDefaults['first_order_time_cv'])
                write_exception_missing(demandArrivalsAliasHeader_dc['first_order_time_cv'],
                                        'Set to default value.', timeCv, 'DemandArrivals.txt',
                                        line_number)
        except ValueError:
            pass

    time = sample_from_distribution(timeDist, timeAverage, timeCv)

    try:
        if math.isnan(time):
            write_exception_invalid(timeDist, demandAliasHeader_dc['first_order_time_dist'],
                                    'Unsupported distribution changed to:',
                                    demandArrivalsDefaults['first_order_time_dist'], 'DemandArrivals.txt',
                                    line_number)

            timeDist = demandArrivalsDefaults['first_order_time_dist']

            if timeDist.lower() not in oneParameterDistributions:
                try:
                    if math.isnan(timeCv):
                        timeCv = float(demandArrivalsDefaults['first_order_time_cv'])
                        write_exception_missing(demandArrivalsAliasHeader_dc['first_order_time_cv'],
                                                'Set to default value.', timeCv, 'DemandArrivals.txt',
                                                line_number)

                except ValueError:
                    pass

            time = sample_from_distribution(timeDist, timeAverage, timeCv)

    except ValueError:
        pass

    return time


def time_between_arrival_stochastic(time_avg, time_cv, time_dist, line_number):
    try:
        if math.isnan(float(time_avg)):
            timeAverage = float(demandArrivalsDefaults['time_bet_orders_avg'])
            write_exception_missing(demandArrivalsAliasHeader_dc['time_bet_orders_avg'], 'Set to default value.',
                                    timeAverage, 'DemandArrivals.txt', line_number)
        else:
            timeAverage = float(time_avg)
    except ValueError:
        timeAverage = float(demandArrivalsDefaults['time_bet_orders_avg'])
        write_exception_invalid(time_avg, demandArrivalsAliasHeader_dc['time_bet_orders_avg'], 'Set to default value.',
                                timeAverage, 'DemandArrivals.txt', line_number)

    try:
        if math.isnan(float(time_dist)):
            timeDist = demandArrivalsDefaults['time_bet_orders_dist']
            write_exception_missing(demandArrivalsAliasHeader_dc['time_bet_orders_dist'], 'Set to default value.',
                                    timeDist, 'DemandArrivals.txt', line_number)
        else:
            timeDist = time_dist
    except ValueError:
        timeDist = time_dist

    timeCv = float(time_cv)
    if timeDist.lower() not in oneParameterDistributions:
        try:
            if math.isnan(timeCv):
                timeCv = float(demandArrivalsDefaults['time_bet_orders_cv'])
                write_exception_missing(demandArrivalsAliasHeader_dc['time_bet_orders_cv'], 'Set to default value.',
                                        timeCv, 'DemandArrivals.txt', line_number)
        except ValueError:
            timeCv = float(demandArrivalsDefaults['time_bet_orders_cv'])
            write_exception_invalid(time_cv, demandArrivalsAliasHeader_dc['time_bet_orders_cv'],
                                    'Set to default value.', timeCv, 'DemandArrivals.txt', line_number)

    time = sample_from_distribution(timeDist, timeAverage, timeCv)

    try:
        if math.isnan(time):
            write_exception_invalid(time_dist, demandAliasHeader_dc['time_bet_orders_dist'],
                                    'Unsupported distribution changed to default:',
                                    demandArrivalsDefaults['time_bet_orders_dist'], 'DemandArrivals.txt',
                                    line_number)
            timeDist = demandArrivalsDefaults['time_bet_orders_dist']
            if timeDist.lower() not in oneParameterDistributions:
                try:
                    if math.isnan(timeCv):
                        timeCv = float(demandArrivalsDefaults['time_bet_orders_cv'])
                        write_exception_missing(demandArrivalsAliasHeader_dc['time_bet_orders_cv'],
                                                'Set to default value.',
                                                timeCv, 'DemandArrivals.txt', line_number)
                except ValueError:
                    timeCv = float(demandArrivalsDefaults['time_bet_orders_cv'])
                    write_exception_invalid(time_cv, demandArrivalsAliasHeader_dc['time_bet_orders_cv'],
                                            'Set to default value.', timeCv, 'DemandArrivals.txt', line_number)

            time = sample_from_distribution(timeDist, timeAverage, timeCv)
    except ValueError:
        pass

    return [time, timeDist, timeAverage, timeCv]


def period_print_generate(env_):
    for day in periodDates:
        time = get_time_from_begin(day) - env_.now
        if time <= 0:
            continue
        yield env_.timeout(time)
        if not consoleEvents and (env_.now < runLength or runLength == 0):
            print('-- Day {:.0f} ({})'.format(env_.now + 1, format_time(runBeginDate + get_time_to_add(env_.now))))

    theDate = runBeginDate + get_time_to_add(env.now)
    while True:
        time = cl.monthrange(theDate.year, theDate.month)[1] - theDate.day + 1
        yield env_.timeout(time)
        theDate = runBeginDate + get_time_to_add(env.now)
        if not consoleEvents and (env_.now < runLength or runLength == 0):
            print('-- Day {:.0f} ({})'.format(env_.now + 1, format_time(theDate)))


def period_policy_generate(env_):
    if loadPeriodsMemory:
        for day in periodDates:
            time = get_time_from_begin(day) - env_.now
            yield env_.timeout(max(0, time))
            if not prep_site_product_period_lists(env_, time):
                continue

            # find all records for current period
            try:
                periodSettings_df = inventoryPolicyPeriods_df.get_group(periods_df.loc[day, 'period_number'])
            except KeyError:
                # no site product period changes
                continue
            except AttributeError:
                # no site product period changes
                continue

            except AttributeError:
                # no site product period changes
                continue

            for i, row in periodSettings_df.iterrows():
                populate_inventory_policy_period_lists(row['site_name'], row['product_name'],
                                                       row['property'], float(row['value']), row['line_number'])

            scan_site_product_period_lists(anySite_dc, anyProduct_dc, siteProduct_dc, byGroups_dc)
        return

    inputFileName = get_os_file_name('InventoryPolicyPeriod.txt', False)
    with open(inputFileName) as fp:
        line = fp.readline()
        lineNumber = 1
        if line:
            aliasColumn = read_header('InventoryPolicyPeriod',
                                      [strip_string(x) for x in line.strip().split("\t")])
            if headerPresent_dc['InventoryPolicyPeriod']:
                # read next line after header
                line = fp.readline()
                lineNumber += 1

        for day in periodDates:
            time = get_time_from_begin(day) - env_.now
            yield env_.timeout(max(0, time))
            if not prep_site_product_period_lists(env_, time):
                continue

            while line:
                row = line.strip().split("\t")
                if len(row) > 0 and row[0] != '':
                    if len(periods_df) > 0:
                        if row[aliasColumn['period_number'] - 1] < periods_df.loc[day, 'period_number']:
                            write_exception_invalid(row[aliasColumn['period_number'] - 1],
                                                    inventoryPoliciesPeriodAliasHeader_dc['period_number'],
                                                    'Period is past current period.  Site product period is ignored.',
                                                    '', 'InventoryPolicyPeriod.txt', lineNumber)

                        if row[aliasColumn['period_number'] - 1] > periods_df.loc[day, 'period_number']:
                            break
                    elif row[aliasColumn['period_number'] - 1] != 1:
                        line = fp.readline()
                        lineNumber += 1
                        continue

                    while len(row) < len(inventoryPoliciesPeriodAliasHeader_dc):
                        row.append('')

                    populate_inventory_policy_period_lists(row[aliasColumn['site_name'] - 1],
                                                           row[aliasColumn['product_name'] - 1],
                                                           row[aliasColumn['property'] - 1],
                                                           float(row[aliasColumn['value'] - 1]), lineNumber)
                line = fp.readline()
                lineNumber += 1

            scan_site_product_period_lists(anySite_dc, anyProduct_dc, siteProduct_dc, byGroups_dc)

    fp.close()


def get_alias_from_property(property_name):
    try:
        return inventoryPolicyHeaderAlias_dc[property_name]
    except KeyError:
        # check if property name is contained within the key
        for key in inventoryPolicyHeaderAlias_dc.keys():
            if property_name in key:
                return inventoryPolicyHeaderAlias_dc[key]
    return ''


def prep_site_product_period_lists(env_, time):
    if time < 0 or (0 < runLength <= env_.now):
        return False

    global anySite_dc, anyProduct_dc, siteProduct_dc, byGroups_dc

    for key in inventoryPolicies_dc:
        inventoryPolicy = inventoryPolicies_dc[key]
        if not site_product_report(inventoryPolicy, REPORT_PLOT):
            continue
        inventoryPolicy.plotXaxisList1.append(env_.now)
        inventoryPolicy.plotXaxisList2.append('{}'.format(format_time(runBeginDate + get_time_to_add(env_.now))))

    if not consoleEvents and (env_.now < runLength or runLength == 0):
        print('-- Day {:.0f} ({})'.format(env_.now + 1, format_time(runBeginDate + get_time_to_add(env_.now))))

    anySite_dc = []
    anyProduct_dc = []
    siteProduct_dc = []
    byGroups_dc = []
    return True


def populate_inventory_policy_period_lists(site_name, product_name, property_name, value_, line_number):
    try:
        math.isnan(float(site_name))
        sites = []
    except ValueError:
        if site_name in sitesExclude_ls:
            return
        if len(sites_df) > 0 and site_name in sites_df.index:
            sites = [site_name]
        elif site_name in siteGroups_ls:
            sites = groupSites_df.get_group(site_name)['site_name'].values.tolist()
        elif masterDataDynamic:
            if not network.site_exists(site_name):
                write_exception_undefined(site_name, inventoryPoliciesPeriodAliasHeader_dc['site_name'],
                                          'New site is defined.', site_name, 'InventoryPolicyPeriod.txt', line_number)
            sites = [site_name]
        else:
            write_exception_undefined(site_name, inventoryPoliciesPeriodAliasHeader_dc['site_name'],
                                      'Site Product Period is ignored.', '', 'InventoryPolicyPeriod.txt', line_number)
            return

    try:
        math.isnan(float(product_name))
        products = []

    except ValueError:
        # check if product is defined in master
        if len(products_df) > 0 and product_name in products_df.index:
            products = [product_name]
        elif product_name in productGroups_ls:
            products = groupProducts_df.get_group(product_name)['product_name'].values.tolist()
        elif masterDataDynamic:
            if product_name not in products_dc.keys():
                create_product(product_name)
                write_exception_undefined(product_name, inventoryPoliciesPeriodAliasHeader_dc['product_name'],
                                          'New product is defined.', product_name, 'InventoryPolicyPeriod.txt',
                                          line_number)
            products = [product_name]
        else:
            write_exception_undefined(product_name, inventoryPoliciesPeriodAliasHeader_dc['product_name'],
                                      'Site Product Period is ignored.', '', 'InventoryPolicyPeriod.txt',
                                      line_number)
            return

    alias = get_alias_from_property(property_name)
    if alias == '':
        write_exception_invalid(property_name, inventoryPoliciesPeriodAliasHeader_dc['property'],
                                'Site product period is ignored.', '', 'InventoryPolicyPeriod.txt', line_number)
        return

    try:
        value = value_
        try:
            if math.isnan(value):
                write_exception_missing(inventoryPoliciesPeriodAliasHeader_dc['value'],
                                        'Site Product Period is ignored.', '', 'InventoryPolicyPeriod.txt', line_number)
                return

            if value < 0:
                write_exception_invalid(value, inventoryPoliciesPeriodAliasHeader_dc['value'], 'Set to positive value:',
                                        -value, 'InventoryPolicyPeriod.txt', line_number)
                value = -value

        except TypeError:
            write_exception_invalid(value, inventoryPoliciesPeriodAliasHeader_dc['value'],
                                    'Site Product Period is ignored.', '', 'InventoryPolicyPeriod.txt', line_number)
            return

    except ValueError:
        write_exception_invalid(value_, inventoryPoliciesPeriodAliasHeader_dc['value'],
                                'Site Product Period is ignored.', '', 'InventoryPolicyPeriod.txt', line_number)
        return

    if len(sites) > 0 and len(products) > 0:
        if site_name not in siteGroups_ls and product_name not in productGroups_ls:
            siteProduct_dc.append([site_name, product_name, alias, value])

        for site in sites:
            for product in products:
                byGroups_dc.append([site, product, alias, value])

    elif len(sites) == 0 and len(products) == 0:
        for key in network.sites_dc:
            for key2 in products_dc:
                get_inventory_policy(network.sites_dc[key].name, products_dc[key2].name). \
                    set_policy_property(alias, value)

    elif len(sites) == 0:
        for product in products:
            anySite_dc.append([product, alias, value])

    elif len(products) == 0:
        for site in sites:
            anyProduct_dc.append([site, alias, value])


def scan_site_product_period_lists(any_site_dc, any_product_dc, site_product_dc, by_groups_dc):
    for productName, alias, value in any_site_dc:
        for key in network.sites_dc:
            get_inventory_policy(network.sites_dc[key].name, productName).set_policy_property(alias, value)

    for siteName, alias, value in any_product_dc:
        for key in products_dc:
            get_inventory_policy(siteName, products_dc[key].name).set_policy_property(alias, value)

    for siteName, productName, alias, value in by_groups_dc:
        get_inventory_policy(siteName, productName).set_policy_property(alias, value)

    for siteName, productName, alias, value in site_product_dc:
        get_inventory_policy(siteName, productName).set_policy_property(alias, value)

    check_updates_completed()


def check_updates_completed():
    global updateCompleted_ls

    while len(updateCompleted_ls) > 0:
        inventoryPolicy = updateCompleted_ls[0]
        updateCompleted_ls.pop(0)
        inventoryPolicy.update_complete()


def find_network_supplier():
    pass


def set_destination_lanes(destination):
    # set all transportation lanes to destination site
    try:
        origin_df = transportLanesDest_df.get_group(destination.name)
        for i, row in origin_df.iterrows():
            set_sites_lanes(get_site(row['origin_node']), destination)

    except KeyError:
        return


def set_origin_lanes(origin):
    # set all transportation lanes from origin site
    try:
        destination_df = transportLanesOrigin_df.get_group(origin.name)
        for i, row in destination_df.iterrows():
            set_sites_lanes(origin, get_site(row['destination_node']))

    except KeyError:
        return


def set_sites_lanes(origin, destination):
    # create the lanes for origin/destination pair
    try:
        # Find all transportation lanes between sites
        lanes = transportLanesSites_df.loc[[(origin.name, destination.name)], :]

        if type(lanes) is pd.Series:
            if len(lanes) > 0:
                network.create_transport_lane(origin, destination, lanes['transport_name'], lanes['distance'],
                                              lanes['line_number'])
            else:
                network.create_blank_lanes_sites(origin, destination)

        elif type(lanes) is pd.DataFrame:
            if len(lanes.index) > 0:
                for i, row in lanes.iterrows():
                    network.create_transport_lane(origin, destination, row['transport_name'], row['distance'],
                                                  row['line_number'])
            else:
                network.create_blank_lanes_sites(origin, destination)
        return

    except KeyError:
        network.create_blank_lanes_sites(origin, destination)
        return

    except AttributeError:
        network.create_blank_lanes_sites(origin, destination)
        return


def assign_transport_lane(order_item, origin, destination):
    availableLanes = network.get_sites_lanes(origin, destination)
    # remove line below once order_item is referenced
    type(order_item)
    if len(availableLanes) == 0:
        return float('NaN')

    return availableLanes[0]


def get_site_type(site):
    try:
        return sites_df.loc[site, 'type']

    except KeyError:
        return ''

    except AttributeError:
        return ''


def get_site(site_name):
    try:
        return network.sites_dc[site_name]

    except KeyError:
        # create site

        try:
            reportLevel = sites_df.loc[site_name, 'report_level']

            if math.isnan(float(reportLevel)):
                reportLevel = reportLevelDefaults['site_level']

        except KeyError:
            reportLevel = reportLevelDefaults['site_level']

        except AttributeError:
            reportLevel = reportLevelDefaults['site_level']

        try:
            groups_ls = siteGroups_df.get_group(site_name)['group_name'].values.tolist()

        except KeyError:
            groups_ls = []

        except AttributeError:
            groups_ls = []

        site = Site(site_name, groups_ls, reportLevel)
        network.add_site(site)
        return site


def get_site_group_list(site_name):
    try:
        return siteGroups_df.get_group(site_name)['group_name'].values.tolist()

    except KeyError:
        return []

    except AttributeError:
        return []


def get_product_group_list(product_name):
    try:
        return productGroups_df.get_group(product_name)['group_name'].values.tolist()

    except KeyError:
        return []

    except AttributeError:
        return []


def create_product(product_name):
    if product_name not in products_dc:
        products_dc[product_name] = Product(product_name, get_product_group_list(product_name))

    return products_dc[product_name]


def get_customer_group_list(customer_name):
    try:
        return customerGroups_df.get_group(customer_name)['group_name'].values.tolist()

    except KeyError:
        return []

    except AttributeError:
        return []


def create_customer(customer_name):
    if customer_name not in customers_dc.keys():
        customers_dc[customer_name] = Customer(customer_name, get_customer_group_list(customer_name))

    return customer_name


def get_inventory_policy(site_name, product_name):
    inventoryPolicyName = site_name + '--' + product_name

    if inventoryPolicyName in inventoryPolicies_dc.keys():
        return inventoryPolicies_dc[inventoryPolicyName]

    site = get_site(site_name)

    if product_name in products_dc.keys():
        product = products_dc[product_name]
    else:
        # create product
        product = create_product(product_name)

    product.add_site(site_name, site)
    site.add_product(product_name, product)

    return set_inventory_sourcing_policies(site, product, inventoryPolicyName)


def create_transport(transport_name, line_number):
    if transports_df.index.isin([transport_name]).any():

        makeList = ['fixed_cost', 'unit_cost', 'speed']

        parameters = {}
        for alias in makeList:
            value = float(transports_df.loc[transport_name, alias])
            try:
                value = float(value)

                if math.isnan(value):
                    write_exception_missing(transportsAliasHeader_dc[alias], 'Set to default value:',
                                            transportDefaults[alias], 'Transports.txt',
                                            transports_df.loc[transport_name, 'line_number'])
                    value = transportDefaults[alias]

                elif value < 0:
                    write_exception_invalid(value, transportsAliasHeader_dc[alias], 'Change to positive value:',
                                            -value, 'Transports.txt', transports_df.loc[transport_name, 'line_number'])
                    value = -value

            except ValueError:
                write_exception_format(value, transportsAliasHeader_dc[alias], 'Set to default value:',
                                       transportDefaults[alias], 'Transports.txt',
                                       transports_df.loc[transport_name, 'line_number'])

                value = transportDefaults[alias]

            parameters[alias] = value

        return Transport(transport_name, parameters['fixed_cost'], parameters['unit_cost'], parameters['speed'])

    else:
        write_exception_undefined(transport_name, transportLanesAliasHeader_dc['transport_name'],
                                  'Transportation lane is ignored.', '', 'TransportLanes.txt', line_number)
        return float('NaN')


def review_policy_set(inventory_policy):
    if "s,S" in inventory_policy.policySettings['inventory_policy']:
        inventory_policy.policy = InventoryPolicy.POLICY_s

        if math.isnan(inventory_policy.policySettings['inventory_reorder_point']):
            inventory_policy.policySettings['inventory_reorder_point'] = \
                inventoryPolicyDefaults['inventory_reorder_point']
            try:
                temp_df = inventoryPolicies_df.loc[[(inventory_policy.site.name, inventory_policy.product.name)],
                                                   'line_number']
                write_exception_missing(inventoryPoliciesAliasHeader_dc['inventory_reorder_point'],
                                        'Set to default value:',
                                        inventory_policy.policySettings['inventory_reorder_point'],
                                        "InventoryPolicies.txt", temp_df[len(temp_df.index) - 1])

            except KeyError:
                write_exception_undefined('', inventoryPoliciesAliasHeader_dc['inventory_reorder_point'],
                                          'Set to default value:',
                                          inventory_policy.policySettings['inventory_reorder_point'],
                                          "InventoryPolicies.txt", '')

        if math.isnan(inventory_policy.policySettings['inventory_order_up_to']):
            inventory_policy.policySettings['inventory_order_up_to'] = inventoryPolicyDefaults['inventory_order_up_to']

            try:
                temp_df = inventoryPolicies_df.loc[[(inventory_policy.site.name, inventory_policy.product.name)],
                                                   'line_number']
                write_exception_missing(inventoryPoliciesAliasHeader_dc['inventory_order_up_to'],
                                        'Set to default value:',
                                        inventory_policy.policySettings['inventory_order_up_to'],
                                        "InventoryPolicies.txt", temp_df[len(temp_df.index) - 1])

            except KeyError:
                write_exception_undefined('', inventoryPoliciesAliasHeader_dc['inventory_order_up_to'],
                                          'Set to default value:',
                                          inventory_policy.policySettings['inventory_order_up_to'],
                                          "InventoryPolicies.txt", '')

        inventory_policy.reviewThreshold = inventory_policy.policySettings['inventory_reorder_point']
        return True

    elif "T,S" in inventory_policy.policySettings['inventory_policy']:
        inventory_policy.policy = InventoryPolicy.POLICY_T

        if math.isnan(inventory_policy.policySettings['inventory_order_up_to']):
            inventory_policy.policySettings['inventory_order_up_to'] = inventoryPolicyDefaults['inventory_order_up_to']

            try:
                temp_df = inventoryPolicies_df.loc[[(inventory_policy.site.name, inventory_policy.product.name)],
                                                   'line_number']
                write_exception_missing(inventoryPoliciesAliasHeader_dc['inventory_order_up_to'],
                                        'Set to default value:',
                                        inventory_policy.policySettings['inventory_order_up_to'],
                                        "InventoryPolicies.txt", temp_df[len(temp_df.index) - 1])

            except KeyError:
                write_exception_undefined('', inventoryPoliciesAliasHeader_dc['inventory_order_up_to'],
                                          'Set to default value:',
                                          inventory_policy.policySettings['inventory_order_up_to'],
                                          "InventoryPolicies.txt", '')

        inventory_policy.reviewThreshold = inventory_policy.policySettings['inventory_order_up_to']

        return True

    else:
        inventory_policy.policySettings['inventory_policy'] = inventoryPolicyDefaults['inventory_policy']
        try:
            temp_df = inventoryPolicies_df.loc[[(inventory_policy.site.name, inventory_policy.product.name)],
                                               'line_number']
            write_exception_missing(inventoryPoliciesAliasHeader_dc['inventory_policy'],
                                    'Set to default value:', inventory_policy.policySettings['inventory_policy'],
                                    "InventoryPolicies.txt", temp_df[len(temp_df.index) - 1])

        except KeyError:
            write_exception_undefined('', inventoryPoliciesAliasHeader_dc['inventory_policy'],
                                      'Set to default value:', inventory_policy.policySettings['inventory_policy'],
                                      "InventoryPolicies.txt", '')

    return False


def get_sourcing_site_name(inventory_policy):
    if len(inventory_policy.policySettings['sourcing_site']) == 0:
        return ''

    sourceSite = select_item_by_rule(inventory_policy.policySettings['sourcing_site'],
                                     inventory_policy.policySettings['sourcing_rule'])

    """
    if sourceSite not in sites_df.index:
        if not masterDataDynamic:
            temp_df = inventoryPolicies_df.loc[[(inventory_policy.site.name, inventory_policy.product.name)],
                                               'line_number']
            write_exception_undefined(sourceSite,
                                      inventoryPoliciesAliasHeader_dc['sourcing_site'], 'Virtual sourcing only.',
                                      '', 'InventoryPolicies.txt', temp_df[len(temp_df.index) - 1])
            return ''

        if sourceSite not in network.sites_dc.keys():
            temp_df = inventoryPolicies_df.loc[[(inventory_policy.site.name, inventory_policy.product.name)],
                                               'line_number']
            write_exception_undefined(sourceSite,
                                      inventoryPoliciesAliasHeader_dc['sourcing_site'],
                                      'New sourcing site created.',
                                      sourceSite, 'InventoryPolicies.txt',
                                      temp_df[len(temp_df.index) - 1])
    """

    return sourceSite


def get_inventory_policies(site_name, product_name):
    temp_df = inventoryPolicies_df.loc[[(site_name, product_name)], :]

    if type(temp_df) == pd.DataFrame:
        if len(temp_df.index) > 1:
            line = inventoryPolicies_df.loc[[(site_name, product_name)], 'line_number'][len(temp_df.index) - 1]
            fileExceptions.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.
                                 format('104', 'Duplicate records:', site_name, product_name,
                                        'Values in last record only will be used.', '', 'InventoryPolicies.txt',
                                        line))

        row = temp_df.iloc[-1, :]
    else:
        row = temp_df

    policies = {}
    for item in inventoryPoliciesAliases_ls:
        try:
            x = row[item]
            try:
                policies[item] = float(x)

            except ValueError:
                policies[item] = x

        except KeyError:
            continue

    policies['line_number'] = row['line_number']
    return policies


def get_sourcing_policies(site_name, product_name):
    temp_df = sourcingPolicies_df.loc[[(site_name, product_name)], :]

    if type(temp_df) == pd.DataFrame:
        if len(temp_df.index) > 1:
            line = sourcingPolicies_df.loc[[(site_name, product_name)], 'line_number'][len(temp_df.index) - 1]
            fileExceptions.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.
                                 format('104', 'Duplicate records:', site_name, product_name,
                                        'Values in last record only will be used.', '', 'SourcingPolicies.txt',
                                        line))

        row = temp_df.iloc[-1, :]
    else:
        row = temp_df

    policies = {}
    for item in sourcingPoliciesAliases_ls:
        try:
            x = row[item]
            if item == 'sourcing_site':
                try:
                    if math.isnan(float(x)):
                        policies[item] = []

                except ValueError:
                    siteList = get_site_list(x)
                    if site_name in siteList:
                        siteList.remove(site_name)

                        if not masterDataDynamic:
                            for site in siteList:
                                if site not in sites_df.index:
                                    siteList.remove(site)

                    policies[item] = siteList
                continue
            try:
                policies[item] = float(x)
            except ValueError:
                policies[item] = x

        except KeyError:
            continue

    policies['line_number'] = row['line_number']
    return policies


def get_site_list(site_name):
    try:
        if math.isnan(float(site_name)):
            return []

    except ValueError:
        pass

    if site_name in sitesExclude_ls:
        return []

    if site_name in sites_ls:
        return [site_name]

    if site_name in siteGroups_ls:
        return groupSites_df.get_group(site_name)['site_name'].values.tolist()

    return [site_name]


def validate_inventory_policy(parameter, alias, line_number):
    try:
        if math.isnan(float(parameter)):
            # Field is missing in Site-Product record
            write_exception_missing(inventoryPoliciesAliasHeader_dc[alias], 'Set to default value:',
                                    inventoryPolicyDefaults[alias], 'InventoryPolicies.txt', line_number)
            return inventoryPolicyDefaults[alias]

        if float(parameter) < 0:
            write_exception_invalid(parameter, inventoryPoliciesAliasHeader_dc[alias], 'Change to positive value:',
                                    -parameter, 'InventoryPolicies.txt', line_number)
            return -float(parameter)

        return float(parameter)

    except ValueError:
        return parameter


def validate_sourcing_policy(parameter, alias, line_number):
    try:
        if math.isnan(float(parameter)):
            # Field is missing in Site-Product record
            write_exception_missing(sourcingPoliciesAliasHeader_dc[alias], 'Set to default value:',
                                    sourcingPolicyDefaults[alias], 'SourcingPolicies.txt', line_number)
            return sourcingPolicyDefaults[alias]

        if float(parameter) < 0:
            write_exception_invalid(parameter, sourcingPoliciesAliasHeader_dc[alias], 'Change to positive value:',
                                    -parameter, 'SourcingPolicies.txt', line_number)
            return -float(parameter)

        return float(parameter)

    except ValueError:
        return parameter


def set_inventory_sourcing_policies(site_, product_, site_product):
    site_name = site_.name
    product_name = product_.name

    if len(inventoryPolicies_df) > 0:
        if inventoryPolicies_df.index.isin([(site_name, product_name)]).any():
            # Site-Product record was found in InventoryPolicies file
            siteProductPolicies = get_inventory_policies(site_name, product_name)

        else:
            siteProductPolicies = {}

            try:
                productGroups = set(get_product_group_list(product_name)).intersection(
                    np.unique(inventoryPoliciesSites_df.loc[[site_name], 'product_name'].tolist()))
            except KeyError:
                productGroups = []

            for product in productGroups:
                if inventoryPolicies_df.index.isin([(site_name, product)]).any():
                    siteProductPolicies = get_inventory_policies(site_name, product)
                    break

            if len(siteProductPolicies) == 0:
                try:
                    siteGroups = set(get_site_group_list(site_name)).intersection(
                        np.unique(inventoryPoliciesProducts_df.loc[[product_name], 'site_name'].tolist()))
                except KeyError:
                    siteGroups = []

                for site in siteGroups:
                    if inventoryPolicies_df.index.isin([(site, product_name)]).any():
                        siteProductPolicies = get_inventory_policies(site, product_name)
                        break

                if len(siteProductPolicies) == 0:
                    for site in siteGroups:
                        for product in productGroups:
                            if inventoryPolicies_df.index.isin([(site, product)]).any():
                                siteProductPolicies = get_inventory_policies(site, product)
                                break

                        if len(siteProductPolicies) > 0:
                            break
    else:
        siteProductPolicies = {}

    if len(siteProductPolicies) > 0:
        makeList = ['inventory_policy', 'inventory_review_period']

        for alias in makeList:
            siteProductPolicies[alias] = validate_inventory_policy(siteProductPolicies[alias], alias,
                                                                   siteProductPolicies['line_number'])

        makeList = ['inventory_reorder_point', 'inventory_order_up_to', 'inventory_reorder_qty']

        for alias in makeList:
            if not math.isnan(siteProductPolicies[alias]) and siteProductPolicies[alias] < 0:
                write_exception_invalid(siteProductPolicies[alias], inventoryPoliciesAliasHeader_dc['alias'],
                                        'Change to positive value:', -siteProductPolicies[alias],
                                        'InventoryPolicies.txt', siteProductPolicies['line_number'])
                siteProductPolicies[alias] = -siteProductPolicies[alias]

        if math.isnan(float(siteProductPolicies['inventory_initial'])):
            if math.isnan(float(inventoryPolicyDefaults['inventory_initial'])):
                if math.isnan(float(siteProductPolicies['inventory_order_up_to'])):
                    if math.isnan(float(siteProductPolicies['inventory_reorder_qty'])):
                        siteProductPolicies['inventory_initial'] = siteProductPolicies['inventory_reorder_point']
                        write_exception_missing(inventoryPoliciesAliasHeader_dc['inventory_initial'],
                                                'Set to Reorder Point:', siteProductPolicies['inventory_initial'],
                                                'InventoryPolicies.txt', siteProductPolicies['line_number'])
                    else:
                        siteProductPolicies['inventory_initial'] = siteProductPolicies['inventory_reorder_point'] + \
                                                                   siteProductPolicies['inventory_reorder_qty']
                        write_exception_missing(inventoryPoliciesAliasHeader_dc['inventory_initial'],
                                                'Set to (ReorderPoint + ReorderQty):',
                                                siteProductPolicies['inventory_initial'], 'InventoryPolicies.txt',
                                                siteProductPolicies['line_number'])
                else:
                    siteProductPolicies['inventory_initial'] = siteProductPolicies['inventory_order_up_to']
                    write_exception_missing(inventoryPoliciesAliasHeader_dc['inventory_initial'],
                                            'Set to OrderUpToLevel:',
                                            siteProductPolicies['inventory_initial'], 'InventoryPolicies.txt',
                                            siteProductPolicies['line_number'])
            else:
                siteProductPolicies['inventory_initial'] = inventoryPolicyDefaults['inventory_initial']
                write_exception_missing(inventoryPoliciesAliasHeader_dc['inventory_initial'], 'Set to default value:',
                                        siteProductPolicies['inventory_initial'], 'InventoryPolicies.txt',
                                        siteProductPolicies['line_number'])
        reportLevel = siteProductPolicies['report_level']
        try:
            if math.isnan(float(reportLevel)):
                reportLevel = reportLevelDefaults['site_product_level']
        except ValueError:
            pass
    else:
        # Site-Product record not found in InventoryPolicies file
        for item in inventoryPoliciesAliases_ls:
            siteProductPolicies[item] = float('NaN')

        makeList = ['inventory_policy', 'inventory_review_period', 'inventory_reorder_point',
                    'inventory_reorder_qty', 'inventory_order_up_to']

        for alias in makeList:
            siteProductPolicies[alias] = inventoryPolicyDefaults[alias]
            write_exception_undefined('', inventoryPoliciesAliasHeader_dc[alias], 'Set to default value:',
                                      siteProductPolicies[alias], site_name, product_name)

        if math.isnan(float(inventoryPolicyDefaults['inventory_initial'])):
            if math.isnan(float(inventoryPolicyDefaults['inventory_order_up_to'])):
                if math.isnan(float(inventoryPolicyDefaults['inventory_reorder_qty'])):
                    siteProductPolicies['inventory_initial'] = inventoryPolicyDefaults['inventory_reorder_point']
                    write_exception_undefined('', inventoryPoliciesAliasHeader_dc['inventory_initial'],
                                              'Set to default ReorderPoint:',
                                              siteProductPolicies['inventory_initial'], site_name, product_name)
                else:
                    siteProductPolicies['inventory_initial'] = inventoryPolicyDefaults['inventory_reorder_point'] + \
                                                               inventoryPolicyDefaults['inventory_reorder_qty']
                    write_exception_undefined('', inventoryPoliciesAliasHeader_dc['inventory_initial'],
                                              'Set to default (ReorderPoint + ReorderQty):',
                                              siteProductPolicies['inventory_initial'], site_name, product_name)
            else:
                siteProductPolicies['inventory_initial'] = inventoryPolicyDefaults['inventory_order_up_to']
                write_exception_undefined('', inventoryPoliciesAliasHeader_dc['inventory_initial'],
                                          'Set to default OrderUpToLevel:', siteProductPolicies['inventory_initial'],
                                          site_name, product_name)
        else:
            siteProductPolicies['inventory_initial'] = inventoryPolicyDefaults['inventory_initial']
            write_exception_undefined('', inventoryPoliciesAliasHeader_dc['inventory_initial'], 'Set to default value:',
                                      siteProductPolicies['inventory_initial'], site_name, product_name)
        reportLevel = reportLevelDefaults['site_product_level']

    if len(sourcingPolicies_df) > 0:
        if sourcingPolicies_df.index.isin([(site_name, product_name)]).any():
            sourcingPolicies = get_sourcing_policies(site_name, product_name)
        else:
            sourcingPolicies = {}

            try:
                productGroups = set(get_product_group_list(product_name)).intersection(
                    np.unique(sourcingPoliciesSites_df.loc[[site_name], 'product_name'].tolist()))
            except KeyError:
                productGroups = []

            for product in productGroups:
                if sourcingPolicies_df.index.isin([(site_name, product)]).any():
                    sourcingPolicies = get_sourcing_policies(site_name, product)
                    break

            if len(sourcingPolicies) == 0:
                try:
                    siteGroups = set(get_site_group_list(site_name)).intersection(
                        np.unique(sourcingPoliciesProducts_df.loc[[product_name], 'site_name'].tolist()))
                except KeyError:
                    siteGroups = []
                for site in siteGroups:
                    if sourcingPolicies_df.index.isin([(site, product_name)]).any():
                        sourcingPolicies = get_sourcing_policies(site, product_name)
                        break

                if len(sourcingPolicies) == 0:
                    for site in siteGroups:
                        for product in productGroups:
                            if sourcingPolicies_df.index.isin([(site, product)]).any():
                                sourcingPolicies = get_sourcing_policies(site, product)
                                break

                        if len(sourcingPolicies) > 0:
                            break
    else:
        sourcingPolicies = {}

    if len(sourcingPolicies) > 0:
        siteProductPolicies['sourcing_site'] = sourcingPolicies['sourcing_site']
        sourcingPolicies['sourcing_rule'] = strip_string(sourcingPolicies['sourcing_rule'])
        makeList = ['sourcing_order_time', 'sourcing_ship_time', 'sourcing_order_cost', 'sourcing_unit_cost',
                    'sourcing_rule']

        for alias in makeList:
            siteProductPolicies[alias] = validate_sourcing_policy(sourcingPolicies[alias], alias,
                                                                  sourcingPolicies['line_number'])
    else:

        # Set to default values
        for item in sourcingPoliciesAliases_ls:
            siteProductPolicies[item] = float('NaN')

        siteProductPolicies['sourcing_site'] = sourcingPolicyDefaults['sourcing_site']
        if len(sourcingPolicyDefaults['sourcing_site']) > 0:
            write_exception_undefined('', sourcingPoliciesAliasHeader_dc['sourcing_site'], 'Set to default value:',
                                      defaultSourcingSite, site_name, product_name)

        makeList = ['sourcing_order_time', 'sourcing_ship_time', 'sourcing_order_cost', 'sourcing_unit_cost',
                    'sourcing_rule']

        for alias in makeList:
            siteProductPolicies[alias] = sourcingPolicyDefaults[alias]
            write_exception_undefined('', sourcingPoliciesAliasHeader_dc[alias], 'Set to default value:',
                                      siteProductPolicies[alias], site_name, product_name)

    inventoryPolicy = InventoryPolicy(env, site_product, product_, site_, reportLevel)
    inventoryPolicies_dc[site_product] = inventoryPolicy
    site_.add_inventory_policy(site_product, inventoryPolicy)
    product_.add_inventory_policy(site_product, inventoryPolicy)

    inventoryPolicy.set_policies(siteProductPolicies)

    if site_product_report(inventoryPolicy, REPORT_LOG):
        fileLog.write('Day {:.0f} ({}): Create new Inventory Policy for Site = {}, Product = {}\n'.
                      format(env.now + 1, format_time(runBeginDate + get_time_to_add(env.now)), site_name,
                             product_name))

        if consoleEvents:
            print('Day {:.0f} ({}): Create new Inventory Policy for Site = {}, Product = {}'.
                  format(env.now + 1, format_time(runBeginDate + get_time_to_add(env.now)), site_name,
                         product_name))

    return inventoryPolicy


def site_product_report(inventory_policy, report_level):
    if report_level == REPORT_NONE and len(inventory_policy.reportMenu.list) == 0:
        return True

    return report_level in inventory_policy.reportMenu.list


def read_input_files():
    global products_df, inventoryPolicies_df, inventoryPoliciesSites_df, inventoryPoliciesProducts_df, fulfillOrders
    global sites_df, sitesExclude_df, sitesExclude_ls, sitesConsider_df, periods_df, defaultSourcingSite
    global transportLanesSites_df, transportLanesOrigin_df, transports_df, transportLanesDest_df
    global customers_df, customersExclude_df, customersExclude_ls, customersConsider_df, demand_df, demandArrivals_df
    global ordersDaily_df, inventoryPolicyPeriods_df, orderItemsRemain
    global policyLists_df, dataDictionary_df, inventoryPolicyDefaults, reportLevelDefaults, transportDefaults
    global dateRunBegin, dateStart, dateEnd, runBeginDate, warmedDate, endDate, runLength, warmLength
    global reviewAtReset, snapInterval, plotCreate, logEvents, consoleEvents
    global scenario, replicationId, globalReportingLevel, inputFilesList
    global masterDataDynamic, loadOrdersMemory, loadPeriodsMemory, loadDemandMemory
    global reportMenus_df, periodDates, numOrderTxt, numDemand
    global headerPresent_dc, inventoryPoliciesAliases_ls, sourcingPoliciesAliases_ls
    global keepOrdersMemory, keepShipmentsMemory, keepTimeSeriesMemory
    global orderQtyDefault, inventoryPolicyHeaderAlias_dc, orderNumberDefault
    global inventoryPoliciesAliasHeader_dc, ordersAliasHeader_dc, transportsAliasHeader_dc, transportLanesAliasHeader_dc
    global inventoryPoliciesPeriodAliasHeader_dc, orderDates, demandAliasHeader_dc, demandArrivalsAliasHeader_dc
    global demandDefaults, customers_ls, sites_ls, randomSeed, demandArrivalsDefaults, productGroups_ls
    global demandSeasonal_df, demandSeasonalDefaults, demandSeasonalAliasHeader_dc, datetimeStart
    global customerGroups_df, customerSourcingPolicies_df, productGroups_df, siteGroups_df, sourcingPolicies_df
    global sourcingPoliciesAliasHeader_dc, customerSourcingPoliciesAliasHeader_dc, sourcingPolicyDefaults
    global customerSourcingCustomer_ls, customerSourcingProduct_ls, siteGroups_ls, groupSites_df, groupProducts_df
    global customerSourcingCustomer_df, customerSourcingProduct_df, customerSourcingPolicies2_df
    global customerSourcingPolicyDefaults, sourcingPoliciesSites_df, sourcingPoliciesProducts_df
    global demandSeasonalGenerated, demandArrivalGenerated, orderGenerated, demandGenerated
    global stripFileName

    # Read Input Files

    print('Reading input data....')
    parameters_df = pd.read_csv(pathConfig / "ConfigModel.txt", sep='\t', dtype='str')
    parameters_df = parameters_df.set_index('SettingName')
    stripFileName = list(parameters_df.loc['CharactersStripFromFileName', 'SettingValue'])
    logEvents = to_bool(parameters_df.loc['LogEvents', 'SettingValue'])
    consoleEvents = to_bool(parameters_df.loc['PrintEventsToConsole', 'SettingValue'])
    masterDataDynamic = to_bool(parameters_df.loc['EnableMasterDataDynamicCreation', 'SettingValue'])
    loadOrdersMemory = to_bool(parameters_df.loc['LoadOrdersInMemory', 'SettingValue'])
    loadPeriodsMemory = to_bool(parameters_df.loc['LoadInventoryPolicyPeriodsInMemory', 'SettingValue'])
    loadDemandMemory = to_bool(parameters_df.loc['LoadDemandInMemory', 'SettingValue'])
    keepOrdersMemory = to_bool(parameters_df.loc['KeepOrdersInMemory', 'SettingValue'])
    keepShipmentsMemory = to_bool(parameters_df.loc['KeepShipmentsInMemory', 'SettingValue'])
    keepTimeSeriesMemory = to_bool(parameters_df.loc['KeepTimeSeriesInMemory', 'SettingValue'])
    snapInterval = float(parameters_df.loc['SnapshotInterval', 'SettingValue'])
    plotCreate = float(parameters_df.loc['InventoryPlots(0-none,1-create,2-display)', 'SettingValue'])
    globalReportingLevel = str(parameters_df.loc['GlobalReportingLevel', 'SettingValue'])

    reportMenus_df = pd.read_csv(pathConfig / "ConfigReports.txt", sep='\t', names=range(10), index_col=0, dtype='str')
    dataDictionary_df = pd.read_csv(pathConfig / "ConfigDataDictionary.txt", sep='\t')
    dataDictionary_df = dataDictionary_df.groupby(dataDictionary_df.columns[0])

    policyLists_df = pd.read_csv(pathConfig / "ConfigPolicyLists.txt", sep='\t')
    policyLists_df = policyLists_df.groupby(policyLists_df.columns[0])

    inputFilesList = os.listdir(pathInputs)

    if file_exists('Customers'):
        customers_df = read_file('Customers')
        add_line_number('Customers', customers_df)
        customersExclude_df = customers_df.loc[(customers_df['status'] == 'Exclude')]
        customersExclude_df = customersExclude_df.set_index(['customer_name'])
        customersExclude_ls = customersExclude_df.index.values.tolist()
        customersConsider_df = customers_df.loc[(customers_df['status'] == 'Consider')]
        customersConsider_df = customersConsider_df.set_index(['customer_name'])
        customers_df = customers_df.loc[(customers_df['status'] != 'Exclude')]
        customers_df = customers_df.set_index(['customer_name'])
        customers_ls = customers_df.index.values.tolist()

    if file_exists('CustomerGroups'):
        customerGroups_df = read_file('CustomerGroups')
        add_line_number('CustomerGroups', customerGroups_df)
        customerGroups_df = customerGroups_df.groupby('customer_name')

    if file_exists('Products'):
        products_df = read_file('Products')
        add_line_number('Products', products_df)
        products_df = products_df.set_index(['product_name'])

    if file_exists('ProductGroups'):
        productGroups_df = read_file('ProductGroups')
        add_line_number('ProductGroups', productGroups_df)
        productGroups_ls = np.unique(productGroups_df.set_index(['group_name']).index.values.tolist())
        groupProducts_df = productGroups_df.groupby('group_name')
        productGroups_df = productGroups_df.groupby('product_name')

    if file_exists('Sites'):
        sites_df = read_file('Sites')
        add_line_number('Sites', sites_df)
        sitesExclude_df = sites_df.loc[(sites_df['status'] == 'Exclude')]
        sitesExclude_df = sitesExclude_df.set_index(['site_name'])
        sitesExclude_ls = sitesExclude_df.index.values.tolist()
        sitesConsider_df = sites_df.loc[(sites_df['status'] == 'Consider')]
        sitesConsider_df = sitesConsider_df.set_index(['site_name'])
        sites_df = sites_df.loc[(sites_df['status'] != 'Exclude')]
        sites_df = sites_df.set_index(['site_name'])
        sites_ls = sites_df.index.values.tolist()

    if file_exists('SiteGroups'):
        siteGroups_df = read_file('SiteGroups')
        add_line_number('SiteGroups', siteGroups_df)
        siteGroups_df = siteGroups_df.loc[siteGroups_df['site_name'].isin(sites_ls)]
        groupSites_df = siteGroups_df.groupby('group_name')
        siteGroups_ls = np.unique(siteGroups_df.set_index(['group_name']).index.values.tolist())
        siteGroups_df = siteGroups_df.groupby('site_name')

    if file_exists('InventoryPolicies'):
        inventoryPolicies_df = read_file('InventoryPolicies')
        add_line_number('InventoryPolicies', inventoryPolicies_df)
        # inventoryPolicies_df = inventoryPolicies_df.drop_duplicates(subset=['site_name', 'product_name'],
        # keep='first')
        inventoryPoliciesProducts_df = inventoryPolicies_df.set_index(['product_name'])
        inventoryPoliciesSites_df = inventoryPolicies_df.set_index(['site_name'])
        inventoryPolicies_df = inventoryPolicies_df.set_index(['site_name', 'product_name'])

    if file_exists('SourcingPolicies'):
        sourcingPolicies_df = read_file('SourcingPolicies')
        add_line_number('SourcingPolicies', sourcingPolicies_df)
        sourcingPoliciesSites_df = sourcingPolicies_df.set_index(['site_name'])
        sourcingPoliciesProducts_df = sourcingPolicies_df.set_index(['product_name'])
        sourcingPolicies_df = sourcingPolicies_df.set_index(['site_name', 'product_name'])

    if file_exists('CustomerSourcingPolicies'):
        customerSourcingPolicies_df = read_file('CustomerSourcingPolicies')
        customerSourcingCustomer_ls = np.unique(customerSourcingPolicies_df.set_index(['customer_name']).
                                                index.values.tolist())
        customerSourcingProduct_ls = np.unique(customerSourcingPolicies_df.set_index(['product_name']).
                                               index.values.tolist())
        add_line_number('CustomerSourcingPolicies', customerSourcingPolicies_df)
        customerSourcingCustomer_df = customerSourcingPolicies_df
        customerSourcingCustomer_df['sourcing_rule'] = strip_df_column(customerSourcingCustomer_df['sourcing_rule'])
        customerSourcingCustomer_df = customerSourcingPolicies_df.set_index(['customer_name'])
        customerSourcingProduct_df = customerSourcingPolicies_df.set_index(['product_name'])
        customerSourcingPolicies2_df = customerSourcingPolicies_df.set_index(['customer_name', 'product_name',
                                                                              'source_site'])
        customerSourcingPolicies_df = customerSourcingPolicies_df.set_index(['customer_name', 'product_name'])

    if file_exists('Orders'):
        if loadOrdersMemory:
            ordersDaily_df = read_file('Orders')
            add_line_number('Orders', ordersDaily_df)
            numOrderTxt = len(ordersDaily_df.index)
            orderItemsRemain += numOrderTxt
            ordersDaily_df = ordersDaily_df.groupby('order_date')
    else:
        orderGenerated = True

    if file_exists('InventoryPolicyPeriod') and loadPeriodsMemory:
        inventoryPolicyPeriods_df = read_file('InventoryPolicyPeriod')
        add_line_number('InventoryPolicyPeriod', inventoryPolicyPeriods_df)
        inventoryPolicyPeriods_df = inventoryPolicyPeriods_df.groupby('period_number')

    if file_exists('Demand') and loadDemandMemory:
        demand_df = read_file('Demand')
        add_line_number('Demand', demand_df)
        numDemand = len(demand_df.index)
        demand_df = demand_df.groupby('period_number')
    else:
        demandGenerated = 0

    if file_exists('DemandArrivals'):
        demandArrivals_df = read_file('DemandArrivals')
        add_line_number('DemandArrivals', demandArrivals_df)
    else:
        demandArrivalGenerated = 0

    if file_exists('DemandSeasonal'):
        demandSeasonal_df = read_file('DemandSeasonal')
        add_line_number('DemandSeasonal', demandSeasonal_df)
    else:
        demandSeasonalGenerated = 0

    if file_exists('Transports'):
        transports_df = read_file('Transports')
        add_line_number('Transports', transports_df)
        transports_df = transports_df.set_index(['transport_name'])

    if file_exists('TransportLanes'):
        transportLanesSites_df = read_file('TransportLanes')
        add_line_number('TransportLanes', transportLanesSites_df)

        transportLanesOrigin_df = transportLanesSites_df.drop_duplicates(subset=['origin_node', 'destination_node'])
        transportLanesDest_df = transportLanesOrigin_df.groupby('destination_node')
        transportLanesOrigin_df = transportLanesOrigin_df.groupby('origin_node')
        transportLanesSites_df = transportLanesSites_df.set_index(['origin_node', 'destination_node'])

    # Create the alias header dictionaries
    inventoryPolicyHeaderAlias_dc = {}
    inventoryPoliciesAliasHeader_dc = {}
    temp_df = dataDictionary_df.get_group('InventoryPolicies.txt')
    for i, row in temp_df.iterrows():
        inventoryPolicyHeaderAlias_dc[row['Header']] = row['Alias']
        inventoryPoliciesAliasHeader_dc[row['Alias']] = row['Header']

    inventoryPoliciesAliases_ls = temp_df['Alias'].tolist()
    inventoryPoliciesAliases_ls.remove('site_name')
    inventoryPoliciesAliases_ls.remove('product_name')

    inventoryPoliciesPeriodAliasHeader_dc = {}
    temp_df = dataDictionary_df.get_group('InventoryPolicyPeriod.txt')
    for i, row in temp_df.iterrows():
        inventoryPoliciesPeriodAliasHeader_dc[row['Alias']] = row['Header']

    sourcingPoliciesAliasHeader_dc = {}
    temp_df = dataDictionary_df.get_group('SourcingPolicies.txt')
    for i, row in temp_df.iterrows():
        sourcingPoliciesAliasHeader_dc[row['Alias']] = row['Header']

    sourcingPoliciesAliases_ls = temp_df['Alias'].tolist()
    sourcingPoliciesAliases_ls.remove('site_name')
    sourcingPoliciesAliases_ls.remove('product_name')

    customerSourcingPoliciesAliasHeader_dc = {}
    temp_df = policyLists_df.get_group('CustomerSourcingPolicies.txt')
    for i, row in temp_df.iterrows():
        customerSourcingPoliciesAliasHeader_dc[row['Alias']] = row['PolicyName']

    ordersAliasHeader_dc = {}
    temp_df = dataDictionary_df.get_group('Orders.txt')
    for i, row in temp_df.iterrows():
        ordersAliasHeader_dc[row['Alias']] = row['Header']

    demandAliasHeader_dc = {}
    temp_df = dataDictionary_df.get_group('Demand.txt')
    for i, row in temp_df.iterrows():
        demandAliasHeader_dc[row['Alias']] = row['Header']

    demandArrivalsAliasHeader_dc = {}
    temp_df = dataDictionary_df.get_group('DemandArrivals.txt')
    for i, row in temp_df.iterrows():
        demandArrivalsAliasHeader_dc[row['Alias']] = row['Header']

    demandSeasonalAliasHeader_dc = {}
    temp_df = dataDictionary_df.get_group('DemandArrivals.txt')
    for i, row in temp_df.iterrows():
        demandSeasonalAliasHeader_dc[row['Alias']] = row['Header']

    transportsAliasHeader_dc = {}
    temp_df = dataDictionary_df.get_group('Transports.txt')
    for i, row in temp_df.iterrows():
        transportsAliasHeader_dc[row['Alias']] = row['Header']

    transportLanesAliasHeader_dc = {}
    temp_df = dataDictionary_df.get_group('TransportLanes.txt')
    for i, row in temp_df.iterrows():
        transportLanesAliasHeader_dc[row['Alias']] = row['Header']

    settings_df = read_file('Settings')
    add_line_number('Settings', settings_df)
    settings_df['name'] = strip_df_column(settings_df['name'])

    settings_df = settings_df.set_index(['name'])

    dateRunBegin = settings_df.loc['SimulationWarmupDateTime'.lower(), 'value']
    dateStart = settings_df.loc['SimulationStartDateTime'.lower(), 'value']

    try:
        math.isnan(float(dateStart))
        try:
            math.isnan(float(dateRunBegin))

        except ValueError:
            if not validate_date(dateRunBegin):

                dateRunBegin = 'NaN'
            else:
                write_exception_missing('SimulationStartDateTime', 'Set to SimulationWarmupDateTime:', dateRunBegin,
                                        'Settings.txt', settings_df.loc['SimulationStartDateTime'.lower(),
                                                                        'line_number'])
                dateStart = dateRunBegin

    except ValueError:
        pass

    if not validate_date(dateStart):
        if not validate_date(dateRunBegin):
            pass

        else:
            write_exception_format('', 'SimulationStartDateTime', 'Set to SimulationWarmupDateTime:', dateRunBegin,
                                   'Settings.txt', settings_df.loc['SimulationStartDateTime'.lower(), 'line_number'])
            dateStart = dateRunBegin

    try:
        math.isnan(float(dateRunBegin))

        try:
            if math.isnan(float(dateStart)):
                write_exception_missing('SimulationWarmupDateTime', 'No action possible.', '', 'Settings.txt',
                                        settings_df.loc['SimulationWarmupDateTime'.lower(), 'line_number'])
                write_exception_missing('SimulationStartDateTime', 'No action possible.', '', 'Settings.txt',
                                        settings_df.loc['SimulationStartDateTime'.lower(), 'line_number'])

                print('\n\nDATA ERROR: Both SimulationWarmupDateTime and SimulationStartDateTime cannot be blank.\n')
                terminate_simulation(True)

        except ValueError:
            write_exception_missing('SimulationWarmupDateTime', 'Set to SimulationStartDateTime:', dateStart,
                                    'Settings.txt', settings_df.loc['SimulationWarmupDateTime'.lower(), 'line_number'])
            dateRunBegin = dateStart

    except TypeError:
        if not validate_date(dateRunBegin):
            write_exception_format(dateRunBegin, 'SimulationWarmupDateTime', 'Set to SimulationStartDateTime:',
                                   dateStart, 'Settings.txt', settings_df.loc['SimulationWarmupDateTime'.lower(),
                                                                              'line_number'])
            dateRunBegin = dateStart

    except ValueError:
        if not validate_date(dateRunBegin):
            write_exception_format(dateRunBegin, 'SimulationWarmupDateTime', 'Set to SimulationStartDateTime:',
                                   dateStart, 'Settings.txt', settings_df.loc['SimulationWarmupDateTime'.lower(),
                                                                              'line_number'])
            dateRunBegin = dateStart

    dateEnd = settings_df.loc['SimulationEndDateTime'.lower(), 'value']

    try:
        math.isnan(float(dateEnd))

    except ValueError:
        if not validate_date(dateEnd):
            write_exception_format(dateStart, 'SimulationEndDateTime', 'Fatal Error.', '',
                                   'Settings.txt', settings_df.loc['SimulationEndDateTime'.lower(), 'line_number'])
            print('\n\nDATA ERROR: SimulationEndDateTime {} is not valid. Enter a valid date.\n'.format(dateEnd))
            terminate_simulation(True)

    try:
        if math.isnan(float(dateRunBegin)):
            if not loadOrdersMemory:
                ordersDaily_df = read_file('Orders')
                ordersDaily_df = ordersDaily_df.groupby('order_date')

            runBeginDate = get_strp_time('1/1/1900')
            orderDates = sorted([key for key, _ in ordersDaily_df], key=get_time_from_begin)
            dateRunBegin = orderDates[0]
            dateStart = dateRunBegin
    except ValueError:
        pass

    try:
        if math.isnan(float(dateEnd)):
            dateEnd = ''

    except ValueError:
        pass

    runBeginDate = get_strp_time(dateRunBegin)
    warmedDate = get_strp_time(dateStart)

    if dateEnd != '':
        endDate = get_strp_time(dateEnd)

        runLength = (endDate - runBeginDate).days + 1

        if runLength < 0:
            write_exception_invalid(dateEnd, 'SimulationEndDateTime', 'Fatal Error.', '',
                                    'Settings.txt', settings_df.loc['SimulationEndDateTime'.lower(), 'line_number'])
            print('\n\nDATA ERROR: SimulationEndDateTime {} is not valid. Enter a valid date.\n'.format(dateEnd))
            terminate_simulation(True)

    warmLength = (warmedDate - runBeginDate).days
    datetimeStart = dt.date(runBeginDate.year, runBeginDate.month, runBeginDate.day)

    if file_exists('Periods'):
        periods_df = read_file('Periods')
        periods_df = periods_df.set_index('start_date')
        periodDates = [key for key, _ in periods_df.groupby('start_date')]
        periodDates = sorted(periodDates, key=get_time_from_begin)
    else:
        periodDates = [dateRunBegin]

    scenario = settings_df.loc['ScenarioName'.lower(), 'value']
    randomSeed = int(settings_df.loc['SimulationGlobalSeed'.lower(), 'value'])
    reviewAtReset = to_bool(settings_df.loc['ReviewImmediatelyAfterPolicyReset'.lower(), 'value'])
    fulfillOrders = to_bool(settings_df.loc['FulfillOrders'.lower(), 'value'])

    temp_df = read_file('Defaults')
    add_line_number('Defaults', temp_df)
    temp_df['name'] = strip_df_column(temp_df['name'])
    temp_df = temp_df.set_index(['name'])

    makeList = [('inventory_policy', 'DefaultInventoryPolicy'),
                ('inventory_review_period', 'DefaultInventoryReviewPeriod'),
                ('inventory_initial', 'DefaultInventoryInitialInventory'),
                ('inventory_reorder_point', 'DefaultInventoryReorderPoint'),
                ('inventory_order_up_to', 'DefaultInventoryOrderUpToLevel'),
                ('inventory_reorder_qty', 'DefaultInventoryReorderQty')]
    inventoryPolicyDefaults = get_from_defaults(makeList, temp_df)

    makeList = [('sourcing_ship_time', 'DefaultSourcingShipTime'),
                ('sourcing_order_cost', 'DefaultSourcingOrderCost'),
                ('sourcing_unit_cost', 'DefaultSourcingOrderUnitCost'),
                ('sourcing_order_time', 'DefaultSourcingOrderTime'),
                ('sourcing_rule', 'DefaultSourcingPolicy')]
    sourcingPolicyDefaults = get_from_defaults(makeList, temp_df)
    defaultSourcingSite = temp_df.loc['DefaultSourcingSourceName'.lower(), 'value']
    sourcingPolicyDefaults['sourcing_site'] = get_site_list(defaultSourcingSite)
    sourcingPolicyDefaults['sourcing_rule'] = strip_string(sourcingPolicyDefaults['sourcing_rule'])

    makeList = [('sourcing_rule', 'DefaultCustomerSourcingPolicy')]
    customerSourcingPolicyDefaults = get_from_defaults(makeList, temp_df)
    customerSourcingPolicyDefaults['sourcing_rule'] = strip_string(customerSourcingPolicyDefaults['sourcing_rule'])

    makeList = [('speed', 'DefaultTransportSpeed'), ('fixed_cost', 'DefaultTransportFixedCost'),
                ('unit_cost', 'DefaultTransportUnitCost')]
    transportDefaults = get_from_defaults(makeList, temp_df)

    makeList = [('site_level', 'DefaultSiteReportingLevel'), ('site_product_level', 'DefaultSiteProductReportingLevel')]
    reportLevelDefaults = get_from_defaults(makeList, temp_df)

    orderQtyDefault = float(temp_df.loc['DefaultOrderQty'.lower(), 'value'])
    if orderQtyDefault < 0:
        write_exception_invalid(orderQtyDefault, 'Default_OrderQty',
                                'Change to positive value:', -orderQtyDefault, '_defaults.txt',
                                temp_df.loc['DefaultOrderQty'.lower(), 'line_number'])
        orderQtyDefault = -orderQtyDefault

    makeList = [('period', 'DefaultDemandPeriod'), ('delivery_due_date', 'DefaultDemandDeliveryDueDate'),
                ('order_qty', 'DefaultDemandOrderQty'), ('force_sum', 'DefaultDemandForceSum'),
                ('random_each_order', 'DefaultDemandRandomEachOrder'),
                ('order_qty_average', 'DefaultDemandOrderQtyAvg'), ('order_qty_cv', 'DefaultDemandOrderQtyCV'),
                ('order_qty_dist', 'DefaultDemandOrderQtyDist')]

    demandDefaults = get_from_defaults(makeList, temp_df)
    demandDefaults['period'] = demandDefaults['period'].lower()

    orderNumberDefault = float(temp_df.loc['DefaultOrderNumStart'.lower(), 'value'])

    makeList = [('demand_qty_dist', 'DefaultDemandArrivalsDemandQtyDist'),
                ('demand_qty_avg', 'DefaultDemandArrivalsDemandQtyAvg'),
                ('demand_qty_cv', 'DefaultDemandArrivalsDemandQtyCV'),
                ('order_qty_dist', 'DefaultDemandArrivalsOrderQtyDist'),
                ('order_qty_avg', 'DefaultDemandArrivalsOrderQtyAvg'),
                ('order_qty_cv', 'DefaultDemandArrivalsOrderQtyCV'),
                ('first_order_time_cv', 'DefaultDemandArrivalsFirstOrderTimeCV'),
                ('first_order_time_dist', 'DefaultDemandArrivalsFirstOrderTimeDist'),
                ('time_bet_orders_avg', 'DefaultDemandArrivalsTimeBetweenOrdersAvg'),
                ('time_bet_orders_cv', 'DefaultDemandArrivalsTimeBetweenOrdersCV'),
                ('time_bet_orders_dist', 'DefaultDemandArrivalsTimeBetweenOrdersDist'),
                ('number_of_orders', 'DefaultDemandArrivalsNumberOfOrders'),
                ('delivery_due_date', 'DefaultDemandArrivalsDeliveryDueDate'),
                ('force_sum_demand', 'DefaultDemandArrivalsForceSum'),
                ('random_each_order', 'DefaultDemandArrivalsRandomEachOrder')]

    demandArrivalsDefaults = get_from_defaults(makeList, temp_df)

    makeList = [('demand_qty_dist', 'DefaultDemandSeasonalDemandQtyDist'),
                ('demand_qty_avg', 'DefaultDemandSeasonalDemandQtyAvg'),
                ('demand_qty_cv', 'DefaultDemandSeasonalDemandQtyCV'),
                ('demand_qty_period', 'DefaultDemandSeasonalDemandPeriod'),
                ('demand_seasonality_index', 'DefaultDemandSeasonalityIndex'),
                ('demand_seasonality_index_period', 'DefaultDemandSeasonalityIndexPeriod'),
                ('demand_growth', 'DefaultDemandSeasonalGrowth'),
                ('demand_growth_period', 'DefaultDemandSeasonalGrowthPeriod'),
                ('order_qty_dist', 'DefaultDemandSeasonalOrderQtyDist'),
                ('order_qty_avg', 'DefaultDemandSeasonalOrderQtyAvg'),
                ('order_qty_cv', 'DefaultDemandSeasonalOrderQtyCV'),
                ('number_of_orders', 'DefaultDemandSeasonalNumberOfOrders'),
                ('delivery_due_date', 'DefaultDemandSeasonalDeliveryDueDate'),
                ('force_sum_demand', 'DefaultDemandSeasonalForceSum'),
                ('random_each_order', 'DefaultDemandSeasonalRandomEachOrder')
                ]

    demandSeasonalDefaults = get_from_defaults(makeList, temp_df)

    replicationId = 0
    if not runAborted:
        print('Input data successfully read.')

    print('Scenario: {}'.format(scenario))

    if logEvents:
        fileLog.write('Model Version: {}\n'.format(MODEL_VERSION))
        fileLog.write('Scenario: {}\n'.format(scenario))


def get_from_defaults(make_list, temp_df):
    defaults_dc = {}
    for alias, defaultParameter in make_list:
        try:
            defaults_dc[alias] = float(temp_df.loc[defaultParameter.lower(), 'value'])
            if defaults_dc[alias] < 0:
                write_exception_invalid(defaults_dc[alias], defaultParameter,
                                        'Change to positive value:', -defaults_dc[alias], '_defaults.txt',
                                        temp_df.loc[defaultParameter.lower(), 'line_number'])
                defaults_dc[alias] = -defaults_dc[alias]

        except ValueError:
            defaults_dc[alias] = temp_df.loc[defaultParameter.lower(), 'value']

        except KeyError:
            continue

    return defaults_dc


def add_line_number(data_type, pandas_df):
    if len(pandas_df.index) > 0:
        if headerPresent_dc[data_type]:
            pandas_df['line_number'] = pd.Series(list(range(2, len(pandas_df) + 2)), index=pandas_df.index)
            return

        pandas_df['line_number'] = pd.Series(list(range(1, len(pandas_df) + 1)), index=pandas_df.index)


def get_header_from_dict_hdr(data_type):
    global headerDictionary_df, headerIndexed_df, alias_dc, header_dc, hdrFileName

    headerDictionary_df = dataDictionary_df.get_group(data_type + '.txt').copy()
    headerDictionary_df['Header'] = headerDictionary_df['Header'].str.lower()

    headerIndexed_df = headerDictionary_df
    headerIndexed_df = headerIndexed_df.set_index('Header')

    hdrFileName = get_os_file_name('{}_HDR.txt'.format(data_type), True)

    header_dc = {}
    alias_dc = {}
    if os.path.exists(hdrFileName):
        tempPanda_df = pd.read_csv(hdrFileName, header=None, sep='\t')
        headerHdr_ls = [strip_string(x) for x in tempPanda_df.values[0]]

        i = 0
        for item in headerHdr_ls:
            i += 1
            header_dc[item] = i
            alias_dc[headerIndexed_df.loc[item, 'Alias']] = i
    else:
        hdrFileName = ''


def process_alias_header(input_file_name):
    global header_dc, alias_dc

    if len(alias_dc) == 0:
        # No HDR file and no header
        for i, row in headerDictionary_df.iterrows():
            header_dc[row[1]] = row[2]
            alias_dc[row[3]] = row[2]

    else:

        for i, row in headerDictionary_df.iterrows():

            try:
                header_dc[row[1]]
            except KeyError:

                if hdrFileName != '':
                    fileName = hdrFileName
                else:
                    fileName = input_file_name

                print('\n\nDATA ERROR: Header {} is undefined in file {}.\n'.format(row[1], fileName))
                terminate_simulation(True)


def file_exists(data_type):
    if os.path.exists(pathInputs / '{}.txt'.format(data_type)):
        return True

    fileName = get_alternative_name(data_type + '.txt', True)
    if fileName == '':
        return False

    return os.path.exists(pathInputs / '{}'.format(fileName))


def get_alternative_name(root_name, optional):
    fileName = root_name.lower()
    alternateFileName = [x.lower() for x in inputFilesList]
    i = 0
    for name in alternateFileName:
        if fileName == name:
            return inputFilesList[i]
        i += 1

    for char in stripFileName:
        alternateFileName = [x.replace(char, '') for x in alternateFileName]
        i = 0
        for name in alternateFileName:
            if fileName == name:
                return inputFilesList[i]
            i += 1

    if not optional:
        print('\n\nDATA ERROR: No file with matching name {} was found in the Inputs folder.\n'.format(root_name))
        terminate_simulation(True)

    return root_name


def get_os_file_name(file_name, optional):
    fileName = pathInputs / file_name

    if os.path.exists(fileName):
        return fileName

    fileName = pathInputs / '{}'.format(get_alternative_name(file_name, optional))

    if os.path.exists(fileName):
        return fileName

    return file_name


def strip_df_column(df_column):
    df_column = df_column.str.replace(' ', '')
    return df_column.str.lower()


def strip_string(string_value):
    try:
        if math.isnan(float(string_value)):
            return float('Nan')
    except ValueError:
        pass
    except TypeError:
        pass

    return string_value.replace(' ', '').lower()


def read_file(data_type):
    global headerPresent_dc

    get_header_from_dict_hdr(data_type)
    inputFileName = get_os_file_name('{}.txt'.format(data_type), False)

    tempPanda_df = pd.read_csv(inputFileName, header=None, sep='\t', dtype='str',
                               names=range(len(headerIndexed_df.index)))

    headerPresent_dc[data_type] = header_exists(headerDictionary_df,
                                                [strip_string(x) for x in tempPanda_df.values[0]])
    if headerPresent_dc[data_type]:
        # Input file has a header

        if len(alias_dc) == 0:
            # no HDR file was found or read
            headerInput_ls = [strip_string(x) for x in tempPanda_df.values[0]]
            i = 0
            for header in headerInput_ls:
                i += 1
                header_dc[header] = i
                alias_dc[headerIndexed_df.loc[header, 'Alias']] = i

        tempPanda_df = tempPanda_df.drop(tempPanda_df.index[0])

    process_alias_header(inputFileName)
    tempPanda_df.columns = alias_dc.keys()
    return tempPanda_df


def read_header(data_type, row):
    global headerPresent_dc

    get_header_from_dict_hdr(data_type)

    inputFileName = get_os_file_name('{}.txt'.format(data_type), False)

    headerPresent_dc[data_type] = header_exists(headerDictionary_df, row)
    if headerPresent_dc[data_type] and len(alias_dc) == 0:
        # Input file has a header no HDR file was found or read
        i = 0
        for header in row:
            i += 1
            header_dc[header] = i
            alias_dc[headerIndexed_df.loc[header, 'Alias']] = i

    process_alias_header(inputFileName)
    return alias_dc


def header_exists(header_dictionary_df, temp_df):
    for i, row in header_dictionary_df.iterrows():
        if row[1] in str(temp_df):
            return True
    return False


def open_report_files():
    global fileOrder, fileShip, fileInv, fileCoq
    global fileBoq, fileExceptions
    global fileLog
    global keepOrdersMemory, keepShipmentsMemory, keepTimeSeriesMemory

    if not os.path.exists('Outputs'):
        os.makedirs('Outputs')
    else:
        # delete all png files
        for file in os.listdir('Outputs'):
            if file.endswith('png'):
                os.remove(os.path.join('Outputs', file))

    if not keepOrdersMemory:
        fileOrder = open(pathOutputs / "Output_ORDERS.txt", "w")
        fileOrder.write('Date\tOrderNumber\tOrderLineNumber\tSite\tCustomer\tProduct\tQty Ordered\tDue Date\n')

    if not keepShipmentsMemory:
        fileShip = open(pathOutputs / "Output_SHIPMENTS.txt", "w")
        fileShip.write('Date\tOrderNumber\tOrderLineNumber\tSite\tCustomer\tProduct\tQty Shipped\tDue Date\n')

    if not keepTimeSeriesMemory:
        fileInv = open(pathOutputs / 'Output_INV.txt', "w")
        fileInv.write('Date\tSite\tProduct\tQty On Hand\n')

        fileCoq = open(pathOutputs / "Output_COQ.txt", "w")
        fileCoq.write('Date\tSite\tProduct\tQty On Order\n')

        fileBoq = open(pathOutputs / "Output_BOQ.txt", "w")
        fileBoq.write('Date\tSite\tProduct\tQty Backorder\n')

    fileLog = open(pathOutputs / "Output_LOG.txt", "w")

    fileExceptions = open(pathOutputs / "Output_EXCEPTIONS.txt", "w")
    fileExceptions.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.
                         format('ErrorNumber', 'Error', 'Field Name', 'Field Value', 'Action',
                                'New Field Value', 'File', 'Line'))


def close_report_files():
    global keepOrdersMemory, keepShipmentsMemory, keepTimeSeriesMemory

    # close all snapshot files
    if not keepOrdersMemory:
        fileOrder.close()

    if not keepShipmentsMemory:
        fileShip.close()

    if not keepTimeSeriesMemory:
        fileInv.close()
        fileCoq.close()
        fileBoq.close()

    fileExceptions.close()
    fileLog.close()


def write_reports(env_):
    # write out summary report

    global keepOrdersMemory, keepShipmentsMemory, keepTimeSeriesMemory

    if keepOrdersMemory:
        global ordersMemory
        file = open(pathOutputs / "Output_ORDERS.txt", "w")
        file.write('Date\tSite\tOrderNumber\tOrderLineNumber\tCustomer\tProduct\tQty Ordered\tDue Date\n')
        for item in ordersMemory:
            file.write(item)
        file.close()

    if keepShipmentsMemory:
        global shipmentsMemory_ls
        file = open(pathOutputs / "Output_SHIPMENTS.txt", "w")
        file.write('Date\tSite\tOrderNumber\tOrderLineNumber\tCustomer\tProduct\tQty Shipped\tDue Date\n')
        for item in shipmentsMemory_ls:
            file.write(item)
        file.close()

    if keepTimeSeriesMemory:
        global timeSeriesMemoryInv_ls, timeSeriesMemoryBoq_ls, timeSeriesMemoryCoq_ls

        file = open(pathOutputs / 'Output_INV.txt', "w")
        file.write('Date\tSite\tProduct\tQty On Hand\n')
        for item in timeSeriesMemoryInv_ls:
            file.write(item)
        file.close()

        file = open(pathOutputs / "Output_COQ.txt", "w")
        file.write('Date\tSite\tProduct\tQty On Order\n')
        for item in timeSeriesMemoryCoq_ls:
            file.write(item)
        file.close()

        file = open(pathOutputs / "Output_BOQ.txt", "w")
        file.write('Date\tSite\tProduct\tQty Backorder\n')
        for item in timeSeriesMemoryBoq_ls:
            file.write(item)
        file.close()

    fileReport = open(pathOutputs / "Output_SUMMARY.txt", "w")

    fileReport.write('Model Version:\t{}\n'.format(MODEL_VERSION))
    fileReport.write('Scenario:\t{}\n'.format(scenario))
    fileReport.write('Replication:\t{}\n'.format(replicationId))
    fileReport.write('Random seed:\t{}\n'.format(randomSeed))
    fileReport.write('Warm up date:\t{}\n'.format(dateRunBegin))
    fileReport.write('Start date:\t{}\n'.format(dateStart))

    if dateEnd != '':
        fileReport.write('End date:\t{}\n'.format(dateEnd))
    else:
        fileReport.write('End date:\t{}\n'.format(format_time(runBeginDate + get_time_to_add(env_.now))))

    fileReport.write('Simulation run length (days):\t{:.0f}\n'.format(runLength))
    fileReport.write('Simulation warm up length (days):\t{}\n'.format(warmLength))
    fileReport.write('Date and time:\t{}\n'.format(dt.datetime.now().strftime("%m/%d/%Y %H:%M:%S")))

    time = ((dt.datetime.now() - timeAtStart).total_seconds())
    fileReport.write('Computer time (sec):\t{:.6f}\n'.format(time))
    fileReport.write('Total customer Order-Items generated:\t{:.0f}\t{:.0f}\n'.
                     format(totalOrdersGenerated, totalOrdersGenerated_))
    fileReport.write('Total Order-Items  received:\t{:.0f}\t{:.0f}\n'.format(totalOrdersReceived, totalOrdersReceived_))
    fileReport.write('Total Order-Items shipped:\t{:.0f}\t{:.0f}\n'.format(totalOrdersShipped, totalOrdersShipped_))

    if totalOrdersShipped > 0:
        x = time / totalOrdersShipped
    else:
        x = 0

    if totalOrdersShipped_ > 0:
        y = (time - warmUpTime) / totalOrdersShipped_
    else:
        y = 0

    fileReport.write('Computer time per order shipped (sec):\t{:.4f}\t{:.4f}\n'.format(x, y))

    fileReport.close()

    fileReport = open(pathOutputs / "Output_REPORT.txt", "w")
    fileReport.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.
                     format('Scenario', 'Replication', 'ProductName', 'Site', 'OrdersReceived', 'OrdersShipped',
                            'UnitsShipped', 'OnTimeOrders', 'LateOrders', 'OrderFillRate', 'OrderQtyFillRate',
                            'AvgDaysLateForLateOrdersOnly', 'TransportationCost'))

    i = 0

    if fulfillOrders:
        for key in inventoryPolicies_dc:

            inventoryPolicy = inventoryPolicies_dc[key]

            plotFilename = 'Outputs/OutputPlots_{}_{}_{}.png'.format(replicationId, inventoryPolicy.site.name,
                                                                     inventoryPolicy.product.name)

            if os.path.exists(plotFilename):
                os.remove(plotFilename)

            if site_product_report(inventoryPolicy, REPORT_SUMMARY):

                if inventoryPolicy.orderTotal_ > 0:
                    x = inventoryPolicy.orderOnTime_ / inventoryPolicy.orderTotal_
                else:
                    x = 0

                if inventoryPolicy.totalOrdered_ > 0:
                    y = inventoryPolicy.qtyFilled_ / inventoryPolicy.totalOrdered_
                else:
                    y = 0

                if inventoryPolicy.orderLate_ > 0:
                    z = inventoryPolicy.cumLate_ / inventoryPolicy.orderLate_
                else:
                    z = 0

                fileReport.write('{}\t{}\t{}\t{}\t{:.0f}\t{:.0f}\t{:.0f}\t{:.0f}\t{:.0f}\t{:.3f}\t{:.3f}\t{:.3f}\t'
                                 '{:.2f}\n'.format(scenario, replicationId, inventoryPolicy.product.name,
                                                   inventoryPolicy.site.name, inventoryPolicy.orderTotal_,
                                                   inventoryPolicy.orderFilled_, inventoryPolicy.totalShipped_,
                                                   inventoryPolicy.orderOnTime_, inventoryPolicy.orderLate_, x, y, z,
                                                   inventoryPolicy.transportationCost))

            # plt.rc('figure', max_open_warning=0)

            if site_product_report(inventoryPolicy, REPORT_PLOT) and plotCreate > 0:
                plt.figure(figsize=(12, 8))

                plt.plot(inventoryPolicy.obsTime, inventoryPolicy.obsInventoryOnHand, label='On-Hand')
                plt.plot(inventoryPolicy.obsTime, inventoryPolicy.obsInventoryPosition, label='Position')
                plt.plot(inventoryPolicy.obsTime, inventoryPolicy.obsCoq, label='COQ')
                plt.plot(inventoryPolicy.obsTime, inventoryPolicy.obsBoq, label='BOQ')
                plt.plot(inventoryPolicy.obsTime, inventoryPolicy.obsReorderAt, label='ReorderPoint')
                plt.plot(inventoryPolicy.obsTime, inventoryPolicy.obsReorderTo, label='OrderUpToLevel')

                plt.xlabel('Date')
                plt.xticks(inventoryPolicy.plotXaxisList1, inventoryPolicy.plotXaxisList2, rotation=45, fontsize=8)
                plt.ylabel('Quantity')

                plt.title('Scenario: {}, Replication: {}, Site: {}, Product: {}'.
                          format(scenario, replicationId, inventoryPolicy.site.name, inventoryPolicy.product.name))
                plt.legend()

                plt.savefig(plotFilename)

                i += 1
                if plotCreate == 1 or i >= 20:
                    plt.close()

    fileReport.close()

    if fulfillOrders and plotCreate > 1 and i < 20:
        plt.show()


def to_bool(value):
    # Converts 'something' to boolean. Raises exception if it gets a string it doesn't handle.
    # Case is ignored for strings. These string values are handled:
    #  True: 'True', "1", "TRue", "yes", "y", "t"
    #  False: "", "0", "faLse", "no", "n", "f"
    # Non-string values are passed to bool.

    if isinstance(value, str) == isinstance('', str):
        if value.lower() in ("yes", "y", "true", "t", "1"):
            return True
        if value.lower() in ("no", "n", "false", "f", "0", ""):
            return False
        return false
        # raise Exception('DATA ERROR: Invalid value for boolean conversion: ' + value)
    return bool(value)


def validate_date(text):
    try:
        get_strp_time(text)
        return True

    except ValueError:
        return False

    except TypeError:
        return False


def validate_interval(from_date_text, to_date_text):
    deltaDate = get_strp_time(to_date_text) - get_strp_time(from_date_text)
    if deltaDate.days < 0:
        return False

    return True


def validate_float(text, negative_not_valid, header, action, new_value, file_name, line_number):
    try:
        num = float(text)

        if math.isnan(num):
            write_exception_missing(header, action, new_value, file_name, line_number)

            return float(new_value)

        if negative_not_valid and num < 0:
            write_exception_invalid(text, header, 'Change to positive value:', -num, file_name, line_number)

            return float(new_value)

        return num

    except ValueError:
        write_exception_format(text, header, action, new_value, file_name, line_number)

        return float(new_value)


def write_exception_missing(header, action, new_value, file_name, line_number):
    fileExceptions.write('{}\t{}\t{}\t\t{}\t{}\t{}\t{}\n'.
                         format('100', 'Missing value:', header, action,
                                new_value, file_name, line_number))


def write_exception_invalid(text, header, action, new_value, file_name, line_number):
    fileExceptions.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.
                         format('103', 'Invalid value:', header, text, action,
                                new_value, file_name, line_number))


def write_exception_format(text, header, action, new_value, file_name, line_number):
    fileExceptions.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.
                         format('102', 'Invalid format:', header, text, action,
                                new_value, file_name, line_number))


def write_exception_undefined(text, header, action, new_value, file_name, line_number):
    fileExceptions.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.
                         format('101', 'Undefined value:', header, text, action,
                                new_value, file_name, line_number))


def write_exception_amend(text, description, action, new_value, file_name, line_number):
    fileExceptions.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.
                         format('201', 'Invalid value:', description, text, action,
                                new_value, file_name, line_number))


def format_time(time):
    return time.strftime('X%m/X%d/%Y').replace('X0', 'X').replace('X', '')


def get_time_from_begin(time):
    return (get_strp_time(time) - runBeginDate).days


def get_interval_time(time1, time2):
    return (get_strp_time(time2) - get_strp_time(time1)).days


def get_time_interval(datetime1, datetime2):
    return (datetime2 - datetime1).days


def month_delta(date1, date2):
    d1 = get_strp_time(date1)
    d2 = get_strp_time(date2)
    delta = 0
    while True:
        mdays = cl.monthrange(d1.year, d1.month)[1]
        d1 += dt.timedelta(days=mdays)
        if d1 <= d2:
            delta += 1
        else:
            break
    return delta


def delta_month(date_time1, date_time2):
    delta = 0
    while True:
        mdays = cl.monthrange(date_time1.year, date_time1.month)[1]
        date_time1 += dt.timedelta(days=mdays)
        if date_time1 <= date_time2:
            delta += 1
        else:
            break
    return delta


def get_strp_time(time):
    return dt.datetime.strptime(time, '%m/%d/%Y')


def get_time_to_add(time):
    return dt.timedelta(days=time)


def sample_from_distribution(dist, mean, cv):
    distName = dist.lower()

    if distName == 'uniform':
        var = mean * cv * 0.01
        return np.random.uniform(mean - var, mean + var)

    if distName == 'normal':
        return np.random.normal(mean, mean * cv * 0.01)

    if distName == 'triangular':
        var = mean * cv * 0.01
        return np.random.triangular(mean - var, mean, mean + var)

    if distName == 'poisson':
        return np.random.poisson(mean)

    if distName == 'binomial':
        return np.random.binomial(mean, mean * cv * 0.01)

    if distName == 'exponential':
        return np.random.exponential(mean)

    if distName == 'gamma':
        return np.random.gamma(mean, mean * cv * 0.01)

    if distName == 'weibull':
        return np.random.weibull(mean)

    if distName == 'lognormal':
        return np.random.lognormal(mean, mean * cv * 0.01)

    if distName == 'negative binomial':
        return np.random.negative_binomial(mean, mean * cv * 0.01)

    if distName == 'geometric':
        return np.random.geometric(mean)

    if distName == 'constant':
        return mean

    return float('NaN')


def simulation_terminate(time):
    global runLength
    if orderItemsRemain > 0 or runLength > 0:
        return False

    if demandGenerated == 0 and orderGenerated and demandArrivalGenerated == 0 and demandSeasonalGenerated == 0:
        runLength = time + 1
        terminate_simulation(False)
        return True

    return False


def terminate_simulation(abort_flag):
    global runAborted

    endSimulation.succeed()
    runAborted = abort_flag


# create simulation environment
env = simpy.Environment()
endSimulation = env.event()

timeAtStart = dt.datetime.now()
print('Model Version: {}'.format(MODEL_VERSION))

open_report_files()
read_input_files()

inventoryPolicies_dc = {}

if not simulation_terminate(env.now):

    np.random.seed(randomSeed)

    network = Network()

    products_dc = {}
    customers_dc = {}
    orders_dc = {}

    env.process(run_warm_up(env))

    if runLength > 0:
        env.process(run_simulation(runLength))

    if file_exists('Orders'):
        env.process(order_generate(env))

    if file_exists('Demand'):
        env.process(demand_generate(env))

    if file_exists('DemandSeasonal'):
        env.process(demand_seasonal_generate(env))

    if file_exists('DemandArrivals'):
        env.process(demand_arrivals_generate(env))

    if file_exists('InventoryPolicyPeriod'):
        env.process(period_policy_generate(env))
    else:
        env.process(period_print_generate(env))

    # run simulation

    if not runAborted:
        if runLength > 0:
            print('Run Length: {:.0f} days starting on {} including {:.0f} warm up days'.
                  format(runLength, dateRunBegin, warmLength))
        else:
            print('Run Length: undefined starting on {} including {:.0f} warm up days'.
                  format(dateRunBegin, warmLength))

        if logEvents:
            if runLength > 0:
                fileLog.write('Run Length: {:.0f} days starting on {} including {:.0f} warm up days\n'.
                              format(runLength, dateRunBegin, warmLength))
            else:
                fileLog.write('Run Length: undefined starting on {} including {:.0f} warm up days\n'.
                              format(dateRunBegin, warmLength))

            fileLog.write('Day {:.0f} ({}): Begin simulation run.\n'.
                          format(env.now + 1, format_time(runBeginDate + get_time_to_add(env.now))))

        if consoleEvents:
            print('Day {:.0f} ({}): Begin simulation run.'.
                  format(env.now + 1, format_time(runBeginDate + get_time_to_add(env.now))))

    env.run(until=endSimulation)

    write_reports(env)

    if dateEnd:
        dayAdjust = -1
    else:
        dayAdjust = 0

    if runAborted:
        print('Day {:.0f} ({}): Abort simulation run'.
              format(env.now + 1, format_time(runBeginDate + get_time_to_add(env.now + dayAdjust))))
        if logEvents:
            fileLog.write('Day {:.0f} ({}): Abort simulation run'.
                          format(env.now + 1, format_time(runBeginDate + get_time_to_add(env.now + dayAdjust))))
    else:

        print('Day {:.0f} ({}): End simulation run with success in {:.3f} seconds'.
              format(runLength, format_time(runBeginDate + get_time_to_add(env.now + dayAdjust)),
                     ((dt.datetime.now() - timeAtStart).total_seconds())))
        if logEvents:
            fileLog.write('Day {:.0f} ({}): End simulation run with success in {:.3f} seconds\n'.
                          format(runLength, format_time(runBeginDate + get_time_to_add(env.now + dayAdjust)),
                                 ((dt.datetime.now() - timeAtStart).total_seconds())))
else:
    print('End simulation run with no orders generated.')
    if logEvents:
        fileLog.write('End simulation run with no orders generated.')

close_report_files()
