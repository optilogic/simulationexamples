Model Version:	1.0.0
Scenario:	Scenario 1 - Single Site, Single Product
Replication:	0
Random seed:	1
Warm up date:	1/1/2018
Start date:	1/1/2018
End date:	12/30/2018
Simulation run length (days):	364
Simulation warm up length (days):	0
Date and time:	11/02/2020 06:14:37
Computer time (sec):	0.319366
Total customer Order-Items generated:	150	150
Total Order-Items  received:	150	150
Total Order-Items shipped:	150	150
Computer time per order shipped (sec):	0.0021	0.0012
