Model Version:	1.0.0
Scenario:	Scenario 5a - Single Product With Seasonality
Replication:	0
Random seed:	1
Warm up date:	1/1/2018
Start date:	1/1/2018
End date:	12/31/2018
Simulation run length (days):	365
Simulation warm up length (days):	0
Date and time:	12/13/2020 13:15:02
Computer time (sec):	0.134640
Total customer Order-Items generated:	365	365
Total Order-Items  received:	365	365
Total Order-Items shipped:	364	364
Computer time per order shipped (sec):	0.0004	0.0002
