SettingName	SettingValue
DefaultOrderQty	100
DefaultOrderNumStart	10000

DefaultInventoryPolicy	"T,S"
DefaultInventoryReviewPeriod	1
DefaultInventoryReorderPoint	1000
DefaultInventoryReorderQty	500
DefaultInventoryOrderUpToLevel	2000
DefaultInventoryInitialInventory	2000

DefaultSourcingPolicy	By Probability
DefaultSourcingSourceName	
DefaultSourcingOrderCost	1000
DefaultSourcingOrderUnitCost	2
DefaultSourcingOrderTime	0
DefaultSourcingShipTime	10

DefaultCustomerSourcingPolicy	Order of Preference

DefaultDemandPeriod	Day
DefaultDemandOrderQty	100

DefaultDemandOrderQtyAvg	100
DefaultDemandOrderQtyCV	10
DefaultDemandOrderQtyDist	Constant
DefaultDemandDeliveryDueDate	1
DefaultDemandForceSum	True
DefaultDemandRandomEachOrder	True

DefaultDemandArrivalsDemandQtyAvg	100
DefaultDemandArrivalsDemandQtyCV	
DefaultDemandArrivalsDemandQtyDist	Constant
DefaultDemandArrivalsOrderQtyAvg	10
DefaultDemandArrivalsOrderQtyCV	0
DefaultDemandArrivalsOrderQtyDist	Constant
DefaultDemandArrivalsFirstOrderTimeCV	0
DefaultDemandArrivalsFirstOrderTimeDist	Constant
DefaultDemandArrivalsTimeBetweenOrdersAvg	100
DefaultDemandArrivalsTimeBetweenOrdersCV	1
DefaultDemandArrivalsTimeBetweenOrdersDist	Constant
DefaultDemandArrivalsNumberOfOrders	1
DefaultDemandArrivalsDeliveryDueDate	1
DefaultDemandArrivalsForceSum	True
DefaultDemandArrivalsRandomEachOrder	True

DefaultDemandSeasonalDemandQtyAvg	100
DefaultDemandSeasonalDemandQtyCV	
DefaultDemandSeasonalDemandQtyDist	Constant
DefaultDemandSeasonalDemandPeriod	month
DefaultDemandSeasonalityIndex	1,1,1,1,1,1,1,1,1,1,1,1	
DefaultDemandSeasonalityIndexPeriod	Month
DefaultDemandSeasonalGrowth	0	
DefaultDemandSeasonalGrowthPeriod	year
DefaultDemandSeasonalOrderQtyAvg	10
DefaultDemandSeasonalOrderQtyCV	0
DefaultDemandSeasonalOrderQtyDist	Constant
DefaultDemandSeasonalNumberOfOrders	
DefaultDemandSeasonalDeliveryDueDate	1
DefaultDemandSeasonalForceSum	True
DefaultDemandSeasonalRandomEachOrder	True

DefaultTransportFixedCost	100
DefaultTransportUnitCost	2
DefaultTransportSpeed	20

DefaultSiteReportingLevel	3
DefaultSiteProductReportingLevel	