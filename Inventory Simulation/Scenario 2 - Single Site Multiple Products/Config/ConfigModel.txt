SettingName	SettingValue
CharactersStripFromFileName	_ %&!
LogEvents	TRUE
PrintEventsToConsole	FALSE
EnableMasterDataDynamicCreation	TRUE
LoadOrdersInMemory	TRUE
LoadInventoryPolicyPeriodsInMemory	TRUE
LoadDemandInMemory	TRUE
KeepOrdersInMemory	TRUE
KeepShipmentsInMemory	TRUE
KeepTimeSeriesInMemory	TRUE
SnapshotInterval	1
"InventoryPlots(0-none,1-create,2-display)"	1
GlobalReportingLevel	4
