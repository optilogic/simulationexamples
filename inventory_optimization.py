"""

This file contains the Simpy (Python simulation package)
implementation of a simulation model analyzing the safety stock levels
in a multi-echelon (two stage) supply chain. The supply chain consists
of a supplier, a bulk container (BU), a drum (DR) and external
customers placing orders to BU and DR. BU node is supplied by the
supplier, whereas the DR node is supplied by the BU node. Customers
place orders at BU and DR nodes.

The exponential distribution is used to generate customer
inter-arrival times. Moreover, the arrivals follow a Poisson
distribution. The exponential distribution is equivalent to the
Poisson distribution if the exponential distribution parameter is
equal to the inverse of the Poisson distribution parameter.

Reference: https://dc.etsu.edu/cgi/viewcontent.cgi?article=4651&context=etd

"""



# Python packages needed for the implementation of this model

import random
import simpy
import numpy as np

# Class defining the supply chain parameters

class P:
    # External orders arrive to DR according to a Poisson process with
    # daily mean of (150/10)/30=1/2 orders/day
    externalToDRMean = 1/2

    # External orders placed to DR have a lot size of 10 units/order
    DRorderLotSize = 10

    # External orders arrive to BU according to a Poisson process with
    # daily mean of (50/20)/30=5/60 orders/day
    externalToBUMean = 5/60

    # External orders placed to BU have a lot size of 20 units/order
    BUorderLotSize = 20

    # BU places replenishment orders in lot sizes of 100 to upstream
    # supplier
    Q_1 = 100

    # DR places replenishment orders in lot sizes of 20 to BU
    Q_2 = 20

    # Re-order point for BU is 20+(200/30)*7 units
    ROP_BU = 20+(200/30)*7

    # Re-order point for DR is 10+(150/30)*2 units
    ROP_DR = 10+(150/30)*2

    # Replenishment lead time from supplier to BU is 7 days
    LT_1 = 7

    # Replenishment lead time from BU to DR is 2 days
    LT_2 = 2
    
    # Max time to run the simulation (1 year)
    simulationTimeMax = 12*30


# Class defining the elements of the simulation used for monitoring.
# These will reset for each simulation run.
    
class S:
    # Initializing the Inventory
    Inv = None

    # The amount of time DR customers wait for the order to be ready
    DRwaits = [ ]

    # The amount of time BU customers wait for the order to be ready
    BUwaits = [ ]

    # Number of BU customers
    nBUCustomers = 0

    # Number of DR customers
    nDRCustomers = 0

    # Creates a list of simulationTimeMax zeros for the daily demand at BU
    BU_Dem_day = list(np.repeat(0,P.simulationTimeMax))

    # Creates a list of simulationTimeMax zeros for the daily demand at DR
    DR_Dem_day = list(np.repeat(0,P.simulationTimeMax))    

    
class Inventory:
    def __init__(self, env):

        # Binds the attribute with the argument, the simpy environment
        self.env = env

        # Models Inventory on-hand at BU node. A Container helps model
        # the production and consumption of a homogenous,
        # undifferentiated bulk. It may be continuous (gas) or
        # discrete (oranges). Containers allow you to retrieve their
        # current level and capacity.
        self.BU_inv = simpy.Container(env, init = P.ROP_BU)

        # Models Inventory on-hand at DR node.
        self.DR_inv = simpy.Container(env, init = P.ROP_DR)
        
        self.mon_procBU = env.process(self.monitor_BU_inv(env))
        self.mon_procDR = env.process(self.monitor_DR_inv(env))    

# Monitors inventory level at BU node daily and triggers replenishment
# orders to supplier when designated inventory level is reached
        
    def monitor_BU_inv(self, env):
        while True:

            # Check whether the Inventory level at BU node is less
            # than or equal to the re-order point for BU
            if self.BU_inv.level <= P.ROP_BU:
                
                print("Time {0}: BU inventory reached ROP: BU places replenishment order".format(self.env.now))
                # Timeout the process at replenishment lead time from
                # the supplier to BU
                yield self.env.timeout(P.LT_1)
                
                print("Time {0}: BU replenishment inventory arrives".format(self.env.now))

                # BU node requests inventory (replenishment order) in
                # lot size of 100 to upstream supplier
                yield self.BUinv.put(P.Q_1)
                
                print("Time {0}: BU replenishment  order is added to inventory".format(self.env.now))
                # Timeout the process
                yield self.env.timeout(1)

# Monitors inventory level at DR node daily and triggers replenishment
# orders to BU node when designated inventory level is reached
                
    def monitor_DR_inv(self, env):
        while True:

            # Check whether the Inventory level at DR node is less
            # than or equal to the re-order point for DR
            if self.DR_inv.level <= P.ROP_DR:
                
                print("Time {0}: DR inventory reached ROP: DR places replenishment order to BU".format(self.env.now))

                # BU node releases inventory (replenishment order) in
                # lot size of 20 to DR node          
                yield self.BU_inv.get(P.Q_2)
                
                print("Time {0}: BU fills DR replenishment request".format(self.env.now))

                # Timeout the process at replenishment lead time from
                # BU to DR
                yield self.env.timeout(P.LT_2)
                
                print("Time {0}: DR replenishment inventory arrives from BU".format(self.env.now))

                # DR node requests inventory (replenishment order) in
                # lot size of 20 to BU
                yield self.DR_inv.put(P.Q_2)
                
                print("Time {0}: DR replenishment order is added to inventory".format(self.env.now))

                # Timeout the process
                yield self.env.timeout(1)


# External customers place order at DR node and this can be filled by
# the the inventory at DR plus requesting inventory from BU
                
class DRCustomer(object):
    def __init__(self, env, name = ''):
        self.env = env
        self.action = self.env.process(self.ordertoDR( ))
        if(name ==  ''):
            self.name = 'RandomDRCustomer' + str(randint(100))
        else:
            self.name = name                
                
    def DRorderToBU(self):
        print("Time {1}: DR places order to BU to fill order for {0}".format(self.name, self.env.now))
        yield S.Inv.BU_inv.get(P.DRorderLotSize)
        yield self.env.timeout(P.LT_2)
        yield S.Inv.DR_inv.put(P.DRorderLotSize)\

    def ordertoDR(self):
        startTime_DR = self.env.now
        j = int(self.env.now)
        S.DR_Dem_day[j] += 1
        print("Time {1}: {0} places order to DR".format(self.name, self.env.now))
        if S.Inv.DR_inv.level < P.DRorderLotSize:
            self.env.process(self.DRorderToBU())
            yield S.Inv.DR_inv.get(P.DRorderLotSize)
            print("Time {1}: {0} receives order from DR".format(self.name, self.env.now))
            waitTime_DR = self.env.now - startTime_DR
            print("{0} had to wait {1} days".format(self.name, waitTime_DR))
            S.DRwaits.append(waitTime_DR)

# External customers place orders at BU node and this can be filled by
# Inventory at BU node plus requesting Inventory from the supplier
            
class BUCustomer(object):
    def __init__(self, env, name = ''):
        self.env = env
        self.action = self.env.process(self.ordertoBU())
        if (name == ''):
            self.name = 'RandomBUCustomer' + str(randint(100))
        else:
            self.name = name
                
    def ordertoBU(self):
        startTime_BU = self.env.now
        i = int(self.env.now)
        S.BU_Dem_day[i] += 1
        print("Time {1}: {0} places order to BU".format(self.name, self.env.now))
        yield S.Inv.BU_inv.get(P.BUorderLotSize)
        print("Time {1}: {0} receives order".format(self.name, self.env.now))
        waitTime_BU = self.env.now - startTime_BU
        print("{0} had to wait {1} days".format(self.name, waitTime_BU))
        S.BUwaits.append(waitTime_BU)

# Processing the order to DR node
        
class DROrderProcessor(object):
    def __init__(self, env, DRlambda):
        self.env = env
        self.action = env.process(self.DREntrance())
        
    def DREntrance(self):
        while True:

            # Using the exponential distribution with parameter
            # 1/P.externalToDRMean for the inter-arrival of
            # customers/orders to DR node
            interarrivalTime_DR = random.expovariate(1/P.externalToDRMean)

            # Timeout the process at interarrivalTime_DR
            yield self.env.timeout(interarrivalTime_DR)

            # Creating an instance of the class DRCustomer using as
            # the name the current number of DR customers
            c = DRCustomer(self.env, name = "DRCustomer {0}".format(S.nDRCustomers))

             # incrementing the number of DR customers
            S.nDRCustomers += 1        

# Processing the order to BU node 
            
class BUOrderProcessor(object):
    def __init__(self, env, BUlambda):
        self.env = env
        self.action = env.process(self.BUEntrance())

    def BUEntrance(self):
        while True:

            # Using the exponential distribution with parameter
            # 1/P.externalToBUMean for the inter-arrival of
            # customers/orders to BU node
            interarrivalTime_BU = random.expovariate(1/P.externalToBUMean)

            # Timeout the process at interarrivalTime_BU 
            yield self.env.timeout(interarrivalTime_BU)

            # Creating an instance of the class BUCustomer using as
            # the name the current number of BU customers
            c = BUCustomer(self.env, name = "BUCustomer {0}".format(S.nBUCustomers))

            # incrementing the number of BU customers
            S.nBUCustomers += 1


            
# random number generator seeded by a natural number to allow the
# simulation to be reproduced
random.seed(12)

# Creating the Simpy environment to process and run the simulation model
env = simpy.Environment()

# Creating an instance for the BUOrderProcessor class with the Poisson
# parameter externalToBUMean
BU = BUOrderProcessor(env, BUlambda = P.externalToBUMean)

# Creating an instance for the DROrderProcessor class with the Poisson
# parameter externalToDRMean
DR = DROrderProcessor(env, DRlambda = P.externalToDRMean)

# Creating an instance for the Inventory class
S.Inv = Inventory(env)

# running the simulation model on simpy environment for the time give
# by the parameter simulationTimeMax
env.run(until = P.simulationTimeMax)

print(S.BUwaits, S.DRwaits, S.nBUCustomers, S.nDRCustomers, S.BU_Dem_day, S.DR_Dem_day)
